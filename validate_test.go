package main

import (
	"errors"
	"fmt"
	"testing"
)

type test struct {
	name          string
	target        interface{}
	expectedValid bool
	expectedError error
}

type testStruct struct {
	Required           string `json:"required" valid:"Required"`
	RequiredWithNoJSON string `valid:"Required"`
	Optional           string
}

var tests = []test{
	test{
		name: "Valid",
		target: &testStruct{
			Required:           "one",
			RequiredWithNoJSON: "two",
		},
		expectedValid: true,
		expectedError: nil,
	},
	test{
		name: "Missing required field",
		target: &testStruct{
			Required:           "",
			RequiredWithNoJSON: "",
		},
		expectedValid: false,
		expectedError: errors.New("required: Required, RequiredWithNoJSON: Required"),
	},
	test{
		name: "Missing just one field",
		target: &testStruct{
			Required:           "",
			RequiredWithNoJSON: "filled",
		},
		expectedValid: false,
		expectedError: errors.New("required: Required"),
	},
	test{
		name: "Pass value not pointer",
		target: testStruct{
			Required:           "",
			RequiredWithNoJSON: "",
		},
		expectedValid: false,
		expectedError: errors.New("required: Required, RequiredWithNoJSON: Required"),
	},
}

func TestValidate(t *testing.T) {
	for _, test := range tests {
		actualValid, actualError := Validate(test.target)
		if actualValid != test.expectedValid {
			t.Errorf(
				"%s valid:\nExpected: %v\nGot: %v\n",
				test.name,
				test.expectedValid,
				actualValid,
			)
		}
		if fmt.Sprint(actualError) != fmt.Sprint(test.expectedError) {
			t.Errorf(
				"%s required:\nExpected: %#v\nReceived: %#v\n",
				test.name,
				test.expectedError,
				actualError,
			)
		}
	}
}
