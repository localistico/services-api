package main

import (
	"crypto/tls"
	"errors"
	_ "expvar"
	"flag"
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/ernesto-jimenez/httplogger"
	"github.com/localistico/services-api/extract"
	"github.com/localistico/services-api/facebook"
	"github.com/localistico/services-api/foursquare"
	"github.com/localistico/services-api/google"
	"github.com/localistico/services-api/service"
	"github.com/localistico/services-api/yelp"
	"github.com/fzzy/radix/extra/pool"
	"github.com/fzzy/radix/redis"
	"github.com/gocraft/web"
	"github.com/lonelycode/go-uuid/uuid"
	"github.com/stvp/rollbar"
	"labix.org/v2/mgo"
)

// Context for websites
type Context struct {
	// RequestID contains generated uuid for this request
	RequestID string
	// Logger contains a logger for the requests
	Logger *log.Logger
	// Database
	DB *mgo.Database
	// Store for resources
	Store ResourceStore
}

func (c *Context) panicIfErr(err error) {
	if err != nil {
		c.Logger.Panic(err)
	}
}

func main() {
	currentEnv := os.Getenv("ENV")
	if currentEnv == "" {
		currentEnv = "development"
	}

	currentMgoDebug := os.Getenv("MGO_DEBUG") == "true"

	var (
		port                   = flag.String("port", os.Getenv("PORT"), "Server port")
		rollbarToken           = flag.String("rollbar-token", os.Getenv("ROLLBAR_TOKEN"), "Access token for Rollbar error notifications")
		mongoURL               = flag.String("mongo-url", os.Getenv("MONGO_URL"), "URL for the mongo database")
		redisURL               = flag.String("redis-url", os.Getenv("REDIS_URL"), "URL for the redis database")
		apiKey                 = flag.String("api-key", os.Getenv("API_KEY"), "API Key for http basic auth")
		googleAPIKey           = flag.String("google-api-key", os.Getenv("GOOGLE_API_KEY"), "API Key for Google Places API")
		yelpConsumerKey        = flag.String("yelp-consumer-key", os.Getenv("YELP_CONSUMER_KEY"), "Yelp's Consumer Key")
		yelpConsumerSecret     = flag.String("yelp-consumer-secret", os.Getenv("YELP_CONSUMER_SECRET"), "Yelp's Consumer Secret")
		yelpTokenKey           = flag.String("yelp-token-key", os.Getenv("YELP_TOKEN_KEY"), "Yelp's Token Key")
		yelpTokenSecret        = flag.String("yelp-token-secret", os.Getenv("YELP_TOKEN_SECRET"), "Yelp's Token Secret")
		proxyURL               = flag.String("proxyURL", os.Getenv("SCRAPER_PROXY_URL"), "URL for the scraper's proxy")
		fbAccessToken          = flag.String("fb-access-token", os.Getenv("FB_ACCESS_TOKEN"), "Access Token to access the Facebook API")
		foursquareClientID     = flag.String("foursquare-client-id", os.Getenv("FOURSQUARE_CLIENT_ID"), "Client ID for the foursquare API")
		foursquareClientSecret = flag.String("foursquare-client-secret", os.Getenv("FOURSQUARE_CLIENT_SECRET"), "Client Secret for the foursquare API")
		env                    = flag.String("env", currentEnv, "What environment to run the app in")
		slackToken             = flag.String("slack-token", os.Getenv("SLACK_TOKEN"), "Token to publish messages to Slack")
		mgoDebug               = flag.Bool("mgo-debug", currentMgoDebug, "Print mgo debug info")
		mountServices          = flag.String("mount-services", os.Getenv("MOUNT_SERVICES"), "csv list of services to mount")
	)
	flag.Parse()
	os.Setenv("TZ", "UTC")

	rootRouter := web.New(Context{})

	rootRouter.Get("/", func(rw web.ResponseWriter, req *web.Request) {
		rw.WriteHeader(200)
		rw.Write([]byte("ok"))
	})

	// Endpoint to test Rollbar integration
	rootRouter.Get("/rollbar", func(rw web.ResponseWriter, req *web.Request) {
		panic("testing rollbar")
	})

	apiRouter := rootRouter.Subrouter(Context{}, "")
	debugRouter := apiRouter.Subrouter(Context{}, "/debug")

	switch *env {
	case "development":
		rootRouter.Middleware(web.ShowErrorsMiddleware)
	case "staging", "production":
		apiRouter.Middleware(apiAuthMiddleware(apiKey))
		debugRouter.Middleware(apiAuthMiddleware(apiKey))
	default:
		log.Fatalf("Unknown environment %s", *env)
	}

	rootRouter.Middleware((*Context).RequestIDMiddleware)
	rootRouter.Middleware((*Context).LoggerMiddleware)
	rootRouter.Middleware((*Context).corsMiddleware)
	rootRouter.Middleware(connectDBMiddleware(mongoURL))

	var proxy func(*http.Request) (*url.URL, error)
	if *proxyURL == "" {
		proxy = http.ProxyFromEnvironment
	} else {
		p, err := url.Parse(*proxyURL)
		if err != nil {
			panic(err)
		}
		proxy = http.ProxyURL(p)
	}
	scraperTransport := httplogger.NewLoggedTransport(
		&http.Transport{Proxy: proxy, TLSClientConfig: &tls.Config{InsecureSkipVerify: true}},
		httplogger.DefaultLogger{},
	)

	if *rollbarToken != "" {
		log.Print("Rollbar setup")
		rollbar.Token = *rollbarToken
		rollbar.Environment = *env
		rootRouter.Middleware((*Context).rollbarMiddleware)
	} else {
		log.Print("Rollbar not setup")
	}

	google := func() *google.Service {
		svc, err := google.NewService(*googleAPIKey, scraperTransport)
		if err != nil {
			log.Fatal(err)
		}
		if *redisURL != "" {
			r, err := url.Parse(*redisURL)
			var password string
			if r.User != nil {
				password, _ = r.User.Password()
			}
			df := func(network, addr string) (*redis.Client, error) {
				client, err := redis.Dial(network, addr)
				if err != nil {
					return nil, err
				}
				if password == "" {
					return client, nil
				}
				if err = client.Cmd("AUTH", password).Err; err != nil {
					client.Close()
					return nil, err
				}
				return client, nil
			}
			pool, err := pool.NewCustomPool("tcp", r.Host, 3, df)
			if err != nil {
				log.Fatal(err)
			}
			svc.ReferenceStore = google.NewRedisReferenceCache(
				pool,
				1*time.Hour,
			)
		}
		return svc
	}
	googleService := func() service.ProfileService { return google() }
	routeServiceEndpoints(apiRouter, "/google", googleService)

	yelp := func() *yelp.Service {
		svc, err := yelp.NewService(*yelpConsumerKey, *yelpConsumerSecret, *yelpTokenKey, *yelpTokenSecret, scraperTransport)
		if err != nil {
			log.Fatal(err)
		}
		return svc
	}
	yelpService := func() service.ProfileService { return yelp() }
	routeServiceEndpoints(apiRouter, "/yelp", yelpService)

	facebook := func() *facebook.Service {
		svc, err := facebook.NewService(*fbAccessToken)
		if err != nil {
			log.Fatal(err)
		}
		return svc
	}
	facebookService := func() service.ProfileService { return facebook() }
	routeServiceEndpoints(apiRouter, "/facebook", facebookService)

	foursquare := func() *foursquare.Service {
		svc, err := foursquare.NewService(*foursquareClientID, *foursquareClientSecret)
		if err != nil {
			log.Fatal(err)
		}
		return svc
	}
	foursquareService := func() service.ProfileService { return foursquare() }
	routeServiceEndpoints(apiRouter, "/foursquare", foursquareService)

	if *mgoDebug {
		mgo.SetDebug(true)
		var aLogger *log.Logger
		aLogger = log.New(os.Stderr, "mgo=debug", log.LstdFlags)
		mgo.SetLogger(aLogger)
	}

	session, err := mgo.Dial(*mongoURL)
	defer session.Close()
	session.SetSafe(&mgo.Safe{W: 1})
	if err != nil {
		log.Fatal(err)
	}
	database := session.DB("")

	backend := extract.NewMongoBackend(database)
	extractor := extract.NewExtractor(&backend)

	var profileServices = []func() service.ProfileService{
		yelpService,
		googleService,
		facebookService,
		foursquareService,
	}

	extractor.SetTransport(httplogger.DefaultLoggedTransport)

	for _, svcFn := range profileServices {
		svc := svcFn()
		if *mountServices != "" && !strings.Contains(*mountServices, svc.Service()) {
			log.Printf("skip %s", svc.Service())
			continue
		}
		log.Printf("mount %s", svc.Service())
		svc.SetTransport(httplogger.NewLoggedTransport(
			&http.Transport{DisableKeepAlives: true},
			&httpLoggerExtraction{coll: database.C("http_log")},
		))
		searchCache := NewCollectionStore(database, svc.Service()+"_search_cache")
		if svc.Service() == "google" {
			searchCache = nil
		}
		profileCache := NewCollectionStore(database, svc.Service()+"_profile_cache")
		profileStatusCache := NewCollectionStore(database, svc.Service()+"_profile_status_cache")
		serviceCache := newServiceCache(svc, searchCache, profileCache, profileStatusCache)
		serviceCache.TTL = 24 * time.Hour
		extractor.MountService(serviceCache)
	}

	go func() {
		backend.EachPendingExtraction(func(extraction extract.Extraction) {
			log.Printf("onboot %s", extraction.Log())
			end, err := extractor.Extract(&extraction)
			if err != nil {
				log.Printf("onboot %s error=%q", extraction.Log(), err.Error())
				return
			}
			<-end
		})
	}()

	extractionEndpoint(apiRouter, extractor, &backend)

	placesRouter := rootRouter.Subrouter(Context{}, "/places")
	placesEndpoint(placesRouter, "", *slackToken, extractor, &backend)

	debugRouter.Get("/pprof/:type", func(rw web.ResponseWriter, r *web.Request) {
		http.DefaultServeMux.ServeHTTP(rw, r.Request)
	})

	log.Println("Start server in port", *port)
	log.Fatal(http.ListenAndServe(":"+*port, rootRouter))
}

// RequestIDMiddleware sets the context's RequestID wit a random uuid
func (c *Context) RequestIDMiddleware(rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	c.RequestID = uuid.New()
	rw.Header().Set("X-Request-Id", c.RequestID)
	next(rw, req)
}

// rollbarMiddleware configures error reporting with rollbar
func (c *Context) rollbarMiddleware(rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	defer capturePanic(req.Request)
	next(rw, req)
}

func capturePanic(req *http.Request) {
	if rec := recover(); rec != nil {
		if err, ok := rec.(error); ok {
			log.Printf("Recording err %s", err)
			rollbar.RequestError(rollbar.ERR, req, err)
		} else if err, ok := rec.(string); ok {
			log.Printf("Recording string %s", err)
			rollbar.RequestError(rollbar.ERR, req, errors.New(err))
		}

		panic(rec)
	}
}

// corsMiddleware sets cross-origin headers
func (c *Context) corsMiddleware(rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Access-Control-Allow-Methods", "*")
	rw.Header().Set("Access-Control-Allow-Headers", "Authorization")
	if req.Method == "OPTIONS" {
		c.Logger.Print("Pre-flight CORS request")
		rw.WriteHeader(http.StatusOK)
	} else {
		next(rw, req)
	}
}

// LoggerMiddleware logs requests
func (c *Context) LoggerMiddleware(rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	startTime := time.Now()
	prefix := fmt.Sprintf("request=%s - ", c.RequestID)
	c.Logger = log.New(os.Stderr, prefix, log.LstdFlags)

	next(rw, req)

	duration := time.Since(startTime).Nanoseconds()
	durationMs := duration / 1000000
	var durationUnits string
	switch {
	case duration > 2000000:
		durationUnits = "ms"
		duration /= 1000000
	case duration > 1000:
		durationUnits = "μs"
		duration /= 1000
	default:
		durationUnits = "ns"
	}

	c.Logger.Printf(
		"[%d %s] durationMs=%d status=%d method=%s path=%s\n",
		duration,
		durationUnits,
		durationMs,
		rw.StatusCode(),
		req.Method,
		req.URL.Path,
	)
}
