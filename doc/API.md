# API

Create extraction:

```bash
curl -iX POST 'http://services-api.localistico.com/extractions' -d '{
  "names": ["pimlico fresh"],
  "locations": ["london"],
  "expand_search": true,
  "check_profiles": [
    {"service": "facebook", "id": "103600506478365"}
  ]
}'
```

```json
HTTP/1.1 200 OK
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:25:13 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 771
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
X-Request-Id: 203461a1-4cc1-4812-851f-6634b0e099d9

{
  "extraction": {
    "correlation_uuid": "e6dbc71b-6137-4124-b8cb-91decc4c3cfe",
    "uuid": "e6dbc71b-6137-4124-b8cb-91decc4c3cfe",
    "status": "pending",
    "last_modified_at": "2015-05-20T16:25:13.066768331Z",
    "extracted_at": "2015-05-20T16:25:13.066767568Z",
    "requested_at": "2015-05-20T16:25:13.066756741Z",
    "names": [
      "pimlico fresh"
    ],
    "ignored_names": null,
    "locations": [
      "london"
    ],
    "radius": 0,
    "services": [
      "yelp",
      "google",
      "facebook",
      "foursquare"
    ],
    "expand_search": true,
    "check_profiles": [
      {
        "service": "facebook",
        "id": "103600506478365"
      }
    ],
    "profile_ids": null,
    "search_ids": null,
    "profile_status_ids": null
  }
}
```

Get pending extractions:

```bash
curl -iX GET 'http://services-api.localistico.com/extractions'
```

```json
HTTP/1.1 200 OK
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:12:45 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 26
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
X-Request-Id: e9951f70-941e-46e4-bbe8-c78cfa6a759e

{
  "extractions": null
}
```

Get extraction

```bash
curl -iX GET 'http://services-api.localistico.com/extractions/0ec012e7-e867-487d-98e0-5359874cce65'
```

```json
HTTP/1.1 200 OK
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:14:29 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 1622
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
X-Request-Id: e21b120d-e370-4d9e-ae01-939c544e4099

{
  "extraction": {
    "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
    "uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
    "status": "completed",
    "last_modified_at": "2015-05-15T09:56:48.187Z",
    "extracted_at": "2015-05-15T09:56:48.187Z",
    "requested_at": "2015-05-15T09:51:52.541Z",
    "webhook":
"https://api.localistico.com/api/reports/2d0cbc2a-89e3-4354-a63c-9f9d5cd3905e/webhook?access_token=Wgte7TtWRPaTd2H85mCgb_Tm0D0-B2tPyCbNaRfmcDo",
    "names": [
      "Vibe Web Design"
    ],
    "ignored_names": [],
    "locations": [
      "Ormskirk, United Kingdom"
    ],
    "radius": 500,
    "services": [
      "google",
      "facebook",
      "foursquare",
      "yelp"
    ],
    "expand_search": true,
    "profile_ids": [
      "1d2c981f-b64a-47cd-acda-e78ce9fcf1ce"
    ],
    "search_ids": [
      "1d288cc6-1f85-48ad-8b54-04b85227283b",
      "0e9efda5-68d1-4ded-bdb5-f5727c6e82f4",
      "4cef2f51-f0bb-4e53-8acf-6f83bb1d96af",
      "696d7323-56f0-438b-9a8e-792c1feaec33",
      "c27367cc-0313-4382-b9b8-36a550d95307",
      "d8a99ad5-f87c-480f-9e9d-adc913cc8bc4",
      "516f2932-8296-4319-b133-6e2979271a60",
      "5f7c33aa-2676-459b-a2bc-d45072691e2e",
      "abd13273-189b-4b51-8b54-f9b9446fa549",
      "8284bbe6-4fa7-4429-a477-c4c88badaca4",
      "c94d5f84-acc3-4c40-85d5-93c4166f8ef0",
      "c419ca44-7177-437d-a660-652dd5743e41",
      "954acf42-08c8-4738-91b5-06f01b404442",
      "7a306078-4d5e-449a-9760-9422c2859e91",
      "77aee78e-9138-4482-bd55-35c68a94934a",
      "30e8e199-f28b-4c01-8a54-81d511e142da"
    ],
    "profile_status_ids": []
  }
}
```

Notify webhook of extraction progress:

```bash
curl -iX GET 'http://services-api.localistico.com/extractions/0ec012e7-e867-487d-98e0-5359874cce65/notify'
```

```json
HTTP/1.1 302 Found
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:15:39 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 72
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
Location: /extractions/0ec012e7-e867-487d-98e0-5359874cce65
X-Request-Id: 21cea0e3-491e-4f89-9610-15c61ee41588
```

Cancel extraction:

```bash
curl -iX GET 'http://services-api.localistico.com/extractions/0ec012e7-e867-487d-98e0-5359874cce65/cancel'
```

```json
HTTP/1.1 302 Found
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:15:39 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 72
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
Location: /extractions/0ec012e7-e867-487d-98e0-5359874cce65
X-Request-Id: 21cea0e3-491e-4f89-9610-15c61ee41588
```

Get extracted profiles with profile details:

```bash
curl -iX GET 'http://services-api.localistico.com/extractions/0ec012e7-e867-487d-98e0-5359874cce65/profiles?include=profile'
```

```json
HTTP/1.1 200 OK
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:16:20 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 1399
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
X-Request-Id: fff664f6-6746-4b5c-a500-9f9954a92715

{
  "profiles": [
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "1d2c981f-b64a-47cd-acda-e78ce9fcf1ce",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:11.603Z",
      "extracted_at": "2015-05-15T09:52:11.603Z",
      "requested_at": "2015-05-15T09:52:09.048Z",
      "id": "vibe-web-design-ormskirk",
      "url": "http://www.yelp.co.uk/biz/vibe-web-design-ormskirk",
      "name": "Vibe Web Design",
      "verified": false,
      "lat": 53.5685877,
      "lng": -2.8787929,
      "address": {
        "street": "50 Bath Springs",
        "postcode": "L39 2YG",
        "locality": "Ormskirk",
        "region": "",
        "country": "GB"
      },
      "phone": "+44 7425 161960",
      "categories": [
        "Web Design"
      ],
      "website": "http://www.vibeweb.co.uk",
      "rating": 0,
      "max_rating": 5,
      "total_reviews": 0,
      "summary": "",
      "hours": {
        "mon": [
          "08:00 - 17:30"
        ],
        "tue": [
          "08:00 - 17:30"
        ],
        "wed": [
          "08:00 - 17:30"
        ],
        "thu": [
          "08:00 - 17:30"
        ],
        "fri": [
          "08:00 - 17:30"
        ],
        "sat": [],
        "sun": []
      },
      "closed": false,
      "service": "yelp",
      "requested_id": "vibe-web-design-ormskirk"
    }
  ]
}
```

Get searches with results

```bash
curl -iX GET 'http://services-api.localistico.com/extractions/0ec012e7-e867-487d-98e0-5359874cce65/searches?include=results'
```

```json
HTTP/1.1 200 OK
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:17:41 GMT
Content-Type: application/json; charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
X-Request-Id: 89fa0624-ca44-4a1e-9d72-043b7a16032f

{
  "searches": [
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "0e9efda5-68d1-4ded-bdb5-f5727c6e82f4",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:08.492Z",
      "extracted_at": "2015-05-15T09:52:08.492Z",
      "requested_at": "2015-05-15T09:51:52.718Z",
      "service": "google",
      "name": "vibe web design",
      "ignored_names": [],
      "location": "Ormskirk, United Kingdom",
      "results": [
        {
          "service": "google",
          "id": "6d745e39f9f9ce2c5204b9b5977f62c8812fa86c",
          "name": "Visual Vibe Creative",
          "discarded": true
        },
        {
          "service": "google",
          "id": "b4e1fcdd805ebdddb61763f5828134895dfcc42f",
          "name": "Visual Vibe Creative",
          "discarded": true
        }
      ]
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "1d288cc6-1f85-48ad-8b54-04b85227283b",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:08.346Z",
      "extracted_at": "2015-05-15T09:52:08.346Z",
      "requested_at": "2015-05-15T09:51:52.62Z",
      "service": "google",
      "name": "Vibe Web Design",
      "ignored_names": [],
      "location": "Ormskirk, United Kingdom",
      "results": [
        {
          "service": "google",
          "id": "6d745e39f9f9ce2c5204b9b5977f62c8812fa86c",
          "name": "Visual Vibe Creative",
          "discarded": true
        },
        {
          "service": "google",
          "id": "b4e1fcdd805ebdddb61763f5828134895dfcc42f",
          "name": "Visual Vibe Creative",
          "discarded": true
        }
      ]
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "30e8e199-f28b-4c01-8a54-81d511e142da",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:56:37.054Z",
      "extracted_at": "2015-05-15T09:56:37.054Z",
      "requested_at": "2015-05-15T09:52:11.642Z",
      "service": "yelp",
      "name": "vibe web design",
      "ignored_names": [],
      "lat": 53.5685877,
      "lng": -2.8787929,
      "radius": 500,
      "results": [
        {
          "service": "yelp",
          "id": "vibe-web-design-ormskirk",
          "name": "Vibe Web Design",
          "discarded": false
        }
      ]
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "4cef2f51-f0bb-4e53-8acf-6f83bb1d96af",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:08.285Z",
      "extracted_at": "2015-05-15T09:52:08.285Z",
      "requested_at": "2015-05-15T09:51:52.829Z",
      "service": "facebook",
      "name": "Vibe Web Design",
      "ignored_names": [],
      "location": "Ormskirk, United Kingdom",
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "516f2932-8296-4319-b133-6e2979271a60",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:09.04Z",
      "extracted_at": "2015-05-15T09:52:09.04Z",
      "requested_at": "2015-05-15T09:51:53.237Z",
      "service": "yelp",
      "name": "Vibe Web Design",
      "ignored_names": [],
      "location": "Ormskirk, United Kingdom",
      "results": [
        {
          "service": "yelp",
          "id": "vibe-web-design-ormskirk",
          "name": "Vibe Web Design",
          "discarded": false
        }
      ]
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "5f7c33aa-2676-459b-a2bc-d45072691e2e",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:09.612Z",
      "extracted_at": "2015-05-15T09:52:09.612Z",
      "requested_at": "2015-05-15T09:51:53.336Z",
      "service": "yelp",
      "name": "vibe web design",
      "ignored_names": [],
      "location": "Ormskirk, United Kingdom",
      "results": [
        {
          "service": "yelp",
          "id": "vibe-web-design-ormskirk",
          "name": "Vibe Web Design",
          "discarded": false
        }
      ]
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "696d7323-56f0-438b-9a8e-792c1feaec33",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:08.29Z",
      "extracted_at": "2015-05-15T09:52:08.29Z",
      "requested_at": "2015-05-15T09:51:52.914Z",
      "service": "facebook",
      "name": "vibe web design",
      "ignored_names": [],
      "location": "Ormskirk, United Kingdom",
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "77aee78e-9138-4482-bd55-35c68a94934a",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:56:36.508Z",
      "extracted_at": "2015-05-15T09:56:36.508Z",
      "requested_at": "2015-05-15T09:52:11.637Z",
      "service": "yelp",
      "name": "Vibe Web Design",
      "ignored_names": [],
      "lat": 53.5685877,
      "lng": -2.8787929,
      "radius": 500,
      "results": [
        {
          "service": "yelp",
          "id": "vibe-web-design-ormskirk",
          "name": "Vibe Web Design",
          "discarded": false
        }
      ]
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "7a306078-4d5e-449a-9760-9422c2859e91",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:56:42.632Z",
      "extracted_at": "2015-05-15T09:56:42.632Z",
      "requested_at": "2015-05-15T09:52:11.63Z",
      "service": "foursquare",
      "name": "vibe web design",
      "ignored_names": [],
      "lat": 53.5685877,
      "lng": -2.8787929,
      "radius": 500,
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "8284bbe6-4fa7-4429-a477-c4c88badaca4",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:56:47.867Z",
      "extracted_at": "2015-05-15T09:56:47.867Z",
      "requested_at": "2015-05-15T09:52:11.612Z",
      "service": "google",
      "name": "vibe web design",
      "ignored_names": [],
      "lat": 53.5685877,
      "lng": -2.8787929,
      "radius": 500,
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "954acf42-08c8-4738-91b5-06f01b404442",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:56:42.514Z",
      "extracted_at": "2015-05-15T09:56:42.514Z",
      "requested_at": "2015-05-15T09:52:11.625Z",
      "service": "foursquare",
      "name": "Vibe Web Design",
      "ignored_names": [],
      "lat": 53.5685877,
      "lng": -2.8787929,
      "radius": 500,
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "abd13273-189b-4b51-8b54-f9b9446fa549",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:56:47.772Z",
      "extracted_at": "2015-05-15T09:56:47.772Z",
      "requested_at": "2015-05-15T09:52:11.607Z",
      "service": "google",
      "name": "Vibe Web Design",
      "ignored_names": [],
      "lat": 53.5685877,
      "lng": -2.8787929,
      "radius": 500,
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "c27367cc-0313-4382-b9b8-36a550d95307",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:08.453Z",
      "extracted_at": "2015-05-15T09:52:08.453Z",
      "requested_at": "2015-05-15T09:51:53.025Z",
      "service": "foursquare",
      "name": "Vibe Web Design",
      "ignored_names": [],
      "location": "Ormskirk, United Kingdom",
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "c419ca44-7177-437d-a660-652dd5743e41",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:56:48.185Z",
      "extracted_at": "2015-05-15T09:56:48.185Z",
      "requested_at": "2015-05-15T09:52:11.62Z",
      "service": "facebook",
      "name": "vibe web design",
      "ignored_names": [],
      "lat": 53.5685877,
      "lng": -2.8787929,
      "radius": 500,
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "c94d5f84-acc3-4c40-85d5-93c4166f8ef0",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:56:47.785Z",
      "extracted_at": "2015-05-15T09:56:47.785Z",
      "requested_at": "2015-05-15T09:52:11.616Z",
      "service": "facebook",
      "name": "Vibe Web Design",
      "ignored_names": [],
      "lat": 53.5685877,
      "lng": -2.8787929,
      "radius": 500,
      "results": []
    },
    {
      "correlation_uuid": "0ec012e7-e867-487d-98e0-5359874cce65",
      "uuid": "d8a99ad5-f87c-480f-9e9d-adc913cc8bc4",
      "status": "completed",
      "attempts": 1,
      "last_modified_at": "2015-05-15T09:52:08.962Z",
      "extracted_at": "2015-05-15T09:52:08.962Z",
      "requested_at": "2015-05-15T09:51:53.124Z",
      "service": "foursquare",
      "name": "vibe web design",
      "ignored_names": [],
      "location": "Ormskirk, United Kingdom",
      "results": []
    }
  ]
}
```

Search an specific platform:

```bash
curl -iX GET 'http://services-api.localistico.com/foursquare/search?name=pimlico%20fresh&location=london'
```

```json
HTTP/1.1 200 OK
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:22:14 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 535
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
X-Request-Id: bfa645a2-e13c-4293-8a72-584d638ffabf

{
  "results": [
    {
      "service": "foursquare",
      "id": "53afb2d9498e6967a50bc499",
      "name": "pimlico fresh",
      "coordinates": {
        "lat": 35.87719255232962,
        "lng": 128.59738896932774
      },
      "discarded": true
    },
    {
      "service": "foursquare",
      "id": "4b5303c1f964a5209f8c27e3",
      "name": "Pimlico Fresh",
      "coordinates": {
        "lat": 51.492438,
        "lng": -0.140163
      },
      "discarded": false
    }
  ],
  "searched_at": "2015-05-20T16:22:14.144346253Z"
}
```

Get details from an specific profile:

```bash
curl -iX GET 'http://services-api.localistico.com/foursquare/profile/:id'
```

```json
HTTP/1.1 200 OK
Server: nginx/1.4.6 (Ubuntu)
Date: Wed, 20 May 2015 16:23:46 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 1036
Connection: keep-alive
Access-Control-Allow-Headers: Authorization
Access-Control-Allow-Methods: *
Access-Control-Allow-Origin: *
X-Request-Id: 1aadead5-fe7d-43f6-8916-c6ea8d6a91bd

{
  "service": "foursquare",
  "id": "4b5303c1f964a5209f8c27e3",
  "url":
"https://foursquare.com/v/pimlico-fresh/4b5303c1f964a5209f8c27e3?ref=IJQ2CUGM0AMC052AE5T2XRUIJ42BG0KKCAVNRO43BSNF0NAT",
  "name": "Pimlico Fresh",
  "verified": false,
  "lat": 51.492438,
  "lng": -0.140163,
  "address": {
    "street": "86 Wilton Rd",
    "postcode": "SW1V 1DN",
    "locality": "London",
    "region": "Greater London",
    "country": "GB"
  },
  "phone": "+442079320030",
  "categories": [
    "Café",
    "Breakfast Spot"
  ],
  "website": "",
  "rating": 8.2,
  "max_rating": 10,
  "total_reviews": 75,
  "summary": "Stylish cafe with outside tables and daily changing
blackboard menu for soups and stews.",
  "hours": {
    "mon": [
      "07:30 - 20:00"
    ],
    "tue": [
      "07:30 - 20:00"
    ],
    "wed": [
      "07:30 - 20:00"
    ],
    "thu": [
      "07:30 - 20:00"
    ],
    "fri": [
      "07:30 - 20:00"
    ],
    "sat": [
      "09:00 - 18:00"
    ],
    "sun": [
      "09:00 - 18:00"
    ]
  },
  "closed": false
}
```
