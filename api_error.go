package main

// APIError is used to render errors
type APIError struct {
	// ID contains the error id
	ID string `json:"id"`
	// Msg contains the error details
	Msg string `json:"msg"`
}
