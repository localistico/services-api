package main

import (
	"encoding/json"
	"net/http"
	"reflect"

	"github.com/gocraft/web"
	"github.com/stvp/rollbar"
)

func (c *Context) renderJSONArray(res web.ResponseWriter, status int, s interface{}) {
	if v := reflect.ValueOf(s); v.IsNil() {
		res.Write([]byte("[]\n"))
		return
	}
	//if t := reflect.TypeOf(s); t.Kind() == reflect.Array && t.Len() == 0 {
	//c.Logger.Print("empty array")
	//res.Write([]byte("[]\n"))
	//return
	//}

	c.renderJSON(res, status, s)
}

func (c *Context) renderError(res web.ResponseWriter, status int, id string, err error) {
	if status == http.StatusInternalServerError {
		rollbar.Error("error", err)
	}
	c.renderJSON(res, status, APIError{
		ID:  id,
		Msg: err.Error(),
	})
}

func (c *Context) renderJSON(res web.ResponseWriter, status int, s interface{}) {
	res.Header().Set("Content-Type", "application/json; charset=utf-8")
	res.WriteHeader(status)
	result, err := json.MarshalIndent(s, "", "  ")
	c.panicIfErr(err)
	_, err = res.Write(result)
	c.panicIfErr(err)
	_, err = res.Write([]byte("\n"))
	c.panicIfErr(err)
}
