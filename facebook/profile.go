package facebook

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/bbirec/goauth2/oauth"
	"github.com/localistico/services-api/service"
	"github.com/kellydunn/golang-geo"
)

const (
	baseURL          = "https://graph.facebook.com"
	searchPath       = "/search"
	placeDetailsPath = "/%s"
)

var (
	placeDetailsFields = strings.Join([]string{
		"best_page",
		"link",
		"website",
		"name",
		"location",
		"category_list",
		"phone",
		"hours",
		"is_unclaimed",
		"description",
		"about",
	}, ",")
)

// Service implements service.ProfileService
type Service struct {
	Transport *oauth.Transport
}

// NewService creates a new Service object
func NewService(accessToken string) (s *Service, err error) {
	if accessToken == "" {
		return nil, errors.New("need to provide an API access token")
	}
	return &Service{
		Transport: &oauth.Transport{
			Token: &oauth.Token{
				AccessToken: accessToken,
			},
		},
	}, nil
}

// SetTransport sets the http transport used for the API requests
func (s *Service) SetTransport(t http.RoundTripper) {
	s.Transport.Transport = t
}

// GetTransport returns the http transport used for the API requests
func (s *Service) GetTransport() http.RoundTripper {
	t := s.Transport.Transport
	if t == nil {
		return http.DefaultTransport
	}
	return t
}

func (s *Service) defaultParams() url.Values {
	var v = url.Values{}
	//v.Set("access_token", s.AccessToken)
	return v
}

// Service returns the name of the service
func (s *Service) Service() string {
	return "facebook"
}

// SearchLocation geocode the loc location and
// search for profiles around that location
func (s *Service) SearchLocation(name string, loc string) (found []service.ProfileFound, err error) {
	location, err := s.geocode(loc)
	if err != nil {
		if err.Error() == "ZERO_RESULTS" {
			return found, nil
		}
		return
	}
	found, err = s.SearchAround(name, location.Lat(), location.Lng(), 50000)
	return
}

type geocodeResult struct {
	location *geo.Point
	err      error
}

var geocodeCache = make(map[string]geocodeResult)

func (s *Service) geocode(loc string) (location *geo.Point, err error) {
	result, ok := geocodeCache[loc]
	if ok {
		return result.location, result.err
	}
	geocoderQuery := s.defaultParams()
	geocoderQuery.Set("address", loc)
	geocoder := geo.GoogleGeocoder{}
	location, err = geocoder.Geocode(geocoderQuery.Encode())
	geocodeCache[loc] = geocodeResult{location, err}
	if err != nil {
		return
	}
	return
}

// SearchBBox search for business profiles inside
// bounding box specified by sw (south wet) and
// ne (north east) coordinates
func (s *Service) SearchBBox(name string, sw *geo.Point, ne *geo.Point) (found []service.ProfileFound, err error) {
	panic("SearchBBox not implemented")
}

type apiSearchResult struct {
	ID       string      `json:"id"`
	Name     string      `json:"name"`
	Location apiLocation `json:"location"`
}

type apiLocation struct {
	Lat      float64 `json:"latitude"`
	Lng      float64 `json:"longitude"`
	City     string  `json:"city"`
	Postcode string  `json:"zip"`
	Street   string  `json:"street"`
	Country  string  `json:"country"`
}

type apiSearchResponse struct {
	Results []apiSearchResult `json:"data"`
	Error   *apiError         `json:"error"`
}

type apiError struct {
	Message string `json:"message"`
	Type    string `json:"type"`
	Code    int    `json:"code"`
}

func (err *apiError) Error() string {
	return fmt.Sprintf("%d %s: %s", err.Code, err.Type, err.Message)
}

func (s *Service) request(url string, response interface{}) error {
	res, err := s.Transport.Client().Get(url)
	if err != nil {
		return err
	}

	defer res.Body.Close()
	return json.NewDecoder(res.Body).Decode(&response)
}

func (s *Service) requestProfile(url string, response *apiProfileResponse) error {
	err := s.request(url, response)
	if err != nil {
		return err
	}

	if response.Error != nil {
		return response.Error
	}

	return nil
}

func (s *Service) requestSearch(url string, response *apiSearchResponse) error {
	err := s.request(url, response)
	if err != nil {
		return err
	}

	if response.Error != nil {
		return response.Error
	}
	return nil
}

// SearchByName searches profiles by name
func (s *Service) SearchByName(name string) (found []service.ProfileFound, err error) {
	query := s.defaultParams()
	query.Set("q", name)
	query.Set("type", "page")
	url := baseURL + searchPath + "?" + query.Encode()

	var response = &apiSearchResponse{}
	err = s.requestSearch(url, response)
	if err != nil {
		return found, err
	}

	filter := service.SimilarNameFilter(name)
	return s.filterResults(filter, response)
}

// SearchAround searches for venues around the
// specified coordinates. radius for search is
// specified in meters
func (s *Service) SearchAround(name string, lat, lng float64, radius int) (found []service.ProfileFound, err error) {
	query := s.defaultParams()
	query.Set("q", name)
	query.Set("type", "place")
	query.Set("center", fmt.Sprintf("%f,%f", lat, lng))
	query.Set("distance", fmt.Sprintf("%d", radius))
	url := baseURL + searchPath + "?" + query.Encode()

	var response = &apiSearchResponse{}
	err = s.requestSearch(url, response)
	if err != nil {
		return found, err
	}

	filter := service.SimilarNameFilter(name)
	return s.filterResults(filter, response)
}

func (s *Service) filterResults(filter service.FilterFunc, response *apiSearchResponse) ([]service.ProfileFound, error) {
	var (
		found []service.ProfileFound
		err   error
	)

	for _, result := range response.Results {
		profile := service.ProfileFound{
			Service:     s.Service(),
			ID:          result.ID,
			Name:        result.Name,
			Coordinates: geo.NewPoint(result.Location.Lat, result.Location.Lng),
		}
		var matched bool
		if matched, err = filter(profile); !matched {
			profile.Discarded = true
		}
		found = append(found, profile)
		if err != nil {
			return found, err
		}
	}

	return found, nil
}

// Profile extracts information for the profile with the specified id
func (s *Service) Profile(id string) (profile service.Profile, err error) {
	query := s.defaultParams()
	query.Set("fields", placeDetailsFields)

	url := baseURL + fmt.Sprintf(placeDetailsPath, id) + "?" + query.Encode()
	var response apiProfileResponse
	err = s.requestProfile(url, &response)
	if err != nil {
		return profile, err
	}

	if response.BestPage != nil {
		return s.Profile(response.BestPage.ID)
	}

	return adapt(&response)
}

var code21ErrorTemplate = regexp.MustCompile("^Page ID (\\d+) was migrated to page ID (\\d+).")

// CheckProfileStatus checks whether the passed profile has been merged or deleted and fills the appropriate field
func (s *Service) CheckProfileStatus(p *service.ProfileStatus) error {
	query := s.defaultParams()
	query.Set("fields", "best_page")

	url := baseURL + fmt.Sprintf(placeDetailsPath, p.ID) + "?" + query.Encode()
	var response apiProfileResponse
	err := s.requestProfile(url, &response)
	if err, ok := err.(*apiError); ok && (err.Code == 803 || err.Code == 100) {
		p.Deleted = true
		return nil
	}

	if err, ok := err.(*apiError); ok && err.Code == 21 {
		errors := code21ErrorTemplate.FindStringSubmatch(err.Message)
		from := errors[1]
		to := errors[2]
		if len(errors) != 3 || from != p.ID || to == "" {
			return err
		}
		p.BestID = to
		return nil
	}

	if err != nil {
		return err
	}

	if response.BestPage != nil {
		p.BestID = response.BestPage.ID
	}

	return nil
}
