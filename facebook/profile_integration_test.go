// +build integration

package facebook

import (
	"encoding/json"
	"flag"
	"os"
	"reflect"
	"testing"

	"github.com/localistico/services-api/service"
	"github.com/kellydunn/golang-geo"
)

var (
	fbAccessToken = flag.String("fb-access-token", os.Getenv("FB_ACCESS_TOKEN"), "Access Token to access the Facebook API")
	svc           service.ProfileService
)

func init() {
	var err error
	svc, err = NewService(*fbAccessToken)
	if err != nil {
		panic(err)
	}
}

func TestSearchAround(t *testing.T) {
	name := "Pimlico Fresh"
	center := geo.NewPoint(51.691544, 0.33409)
	radius := 50000
	found, rejected, err := service.SplitSearch(svc.SearchAround(name, center.Lat(), center.Lng(), radius))
	expectedFound := []service.ProfileFound{
		service.ProfileFound{
			Service:     "facebook",
			Name:        "Pimlico fresh",
			ID:          "279204625442028",
			Coordinates: geo.NewPoint(51.4924790225, -0.140232692367),
		},
	}
	expectedTotalFound := 1
	expectedTotalRejected := 0
	if err != nil {
		t.Error(err)
	}
	if actualTotalFound := len(found); actualTotalFound != expectedTotalFound {
		t.Errorf("Expected %d profiles searching for %s.\nGot: %d\n%#v", expectedTotalFound, name, actualTotalFound, found)
	} else if !reflect.DeepEqual(found, expectedFound) {
		t.Errorf("Fail %s\nExpected: %s\nReceived: %s", name, asJSON(expectedFound), asJSON(found))
	}
	if actualTotalRejected := len(rejected); actualTotalRejected != expectedTotalRejected {
		t.Errorf("Expected %d rejected profiles searching for %s.\nGot: %d\n%#v", expectedTotalRejected, name, actualTotalRejected, rejected)
	}
}

func TestSearchLocation(t *testing.T) {
	name := "Pimlico Fresh"
	location := "London"
	found, rejected, err := service.SplitSearch(svc.SearchLocation(name, location))
	expectedFound := []service.ProfileFound{
		service.ProfileFound{
			Service:     "facebook",
			Name:        "Pimlico fresh",
			ID:          "279204625442028",
			Coordinates: geo.NewPoint(51.4924790225, -0.140232692367),
		},
	}
	expectedTotalFound := 1
	expectedTotalRejected := 0
	if err != nil {
		t.Error(err)
	}
	if actualTotalFound := len(found); actualTotalFound != expectedTotalFound {
		t.Errorf("Expected %d profiles searching for %s.\nGot: %d\n%#v", expectedTotalFound, name, actualTotalFound, found)
	} else if !reflect.DeepEqual(found, expectedFound) {
		t.Errorf("Fail %s\nExpected: %s\nReceived: %s", name, asJSON(expectedFound), asJSON(found))
	}
	if actualTotalRejected := len(rejected); actualTotalRejected != expectedTotalRejected {
		t.Errorf("Expected %d rejected profiles searching for %s.\nGot: %d\n%#v", expectedTotalRejected, name, actualTotalRejected, rejected)
	}
}

type testProfile struct {
	name     string
	location string
	expected service.Profile
}

var testProfiles = []testProfile{

	testProfile{
		name:     "Pimlico Fresh",
		location: "London",
		expected: service.Profile{
			Service:  "facebook",
			ID:       "279204625442028",
			URL:      "https://www.facebook.com/pages/Pimlico-fresh/279204625442028",
			Name:     "Pimlico fresh",
			Verified: true,
			Lat:      51.4924790225,
			Lng:      -0.140232692367,
			Address: service.Address{
				StreetAddress: "86 wilton road",
				Postcode:      "sw1v1dn",
				Locality:      "London",
				Region:        "",
				Country:       "GB",
			},
			Phone:      "02079320030",
			Categories: []string{"Restaurant"},
			Website:    "",
			Summary:    "",
		},
	},

	testProfile{
		name:     "Gastronomica",
		location: "London",
		expected: service.Profile{
			Service:  "facebook",
			ID:       "153906154636819",
			URL:      "https://www.facebook.com/pages/Gastronomica/153906154636819",
			Name:     "Gastronomica",
			Verified: false,
			Lat:      51.491905008756,
			Lng:      -0.13835131656752,
			Address: service.Address{
				StreetAddress: "45 Tachbrook Street",
				Postcode:      "SW1V 2LZ",
				Locality:      "London",
				Region:        "",
				Country:       "GB",
			},
			Phone:      "+44 (0) 20 7233 6656",
			Categories: []string{"Restaurant"},
			Website:    "http://www.gastronomica.co.uk/",
			Summary:    "",
			Hours: service.ProfileHours{
				Mon: []string{"08:00 - 23:00"},
				Tue: []string(nil),
				Wed: []string(nil),
				Thu: []string(nil),
				Fri: []string(nil),
				Sat: []string{"08:00 - 23:00"},
				Sun: []string{"09:00 - 22:00"},
			},
		},
	},

	//testProfile{
	//name:     "Home Burger",
	//location: "Madrid",
	//expected: service.Profile{
	//Service:  "facebook",
	//ID:       "173746332647750",
	//URL:      "https://www.facebook.com/homeburgerbar",
	//Name:     "Home Burger Bar",
	//Verified: true,
	//Lat:      40.421363693806,
	//Lng:      -3.698267962968,
	//Address: service.Address{
	//StreetAddress: "Calle Cruz, 7",
	//Postcode:      "28012",
	//Locality:      "Madrid",
	//Region:        "",
	//Country:       "ES",
	//},
	//Phone: "+34.91.188.27.36",
	//Categories: []string{
	//"New American Restaurant",
	//"Vegetarian & Vegan Restaurant",
	//"Burger Restaurant",
	//},
	//Website: "",
	//Summary: "C/ Espíritu Santo, 12\nMadrid 28004\n915229728\nSe admiten reservas\n\nC/ Paseo de La Castellana, 210\nMadrid 28046\n912 19 56 58\nSe admiten reservas\n\nC/ San Marcos, 26\nMadrid 28004\n915 21 85 31\nSe admiten reservas\n\nC/ Silva, 25\nMadrid 28004\n911 15 12 79\nNo se admiten reservas\n\n",
	//Hours: service.ProfileHours{
	//Mon: []string{"13:30 - 16:00", "20:30 - 00:00"},
	//Tue: []string{"13:30 - 16:00", "20:30 - 00:00"},
	//Wed: []string{"13:30 - 16:00", "20:30 - 00:00"},
	//Thu: []string{"13:30 - 16:00", "20:30 - 00:00"},
	//Fri: []string{"13:30 - 17:00", "20:30 - 01:00"},
	//Sat: []string{"13:30 - 17:00", "20:30 - 01:00"},
	//Sun: []string{"13:30 - 17:00", "20:30 - 23:00"},
	//},
	//},
	//},

	testProfile{
		name:     "Home Burger",
		location: "Madrid",
		expected: service.Profile{
			Service:  "facebook",
			ID:       "156168281188455",
			URL:      "https://www.facebook.com/pages/Home-Burger-Bar/156168281188455",
			Name:     "Home Burger Bar",
			Verified: false,
			Lat:      40.450939125,
			Lng:      -3.704131865,
			Address: service.Address{
				StreetAddress: "",
				Postcode:      "",
				Locality:      "Madrid",
				Region:        "",
				Country:       "ES",
			},
			Phone: "",
			Categories: []string{
				"Local Business",
			},
		},
	},
}

func TestProfile(t *testing.T) {
	for _, test := range testProfiles {
		name := test.name
		location := test.location
		expectedProfile := test.expected
		found, _, err := svc.SearchLocation(name, location)
		if err != nil {
			t.Fatal(err)
		}
		if len(found) == 0 {
			t.Errorf("No results for %s in %s", name, location)
			continue
		}
		actualProfile, err := svc.Profile(found[0].ID)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(actualProfile, expectedProfile) {
			t.Errorf("Fail %s\nExpected: %#v\nReceived: %#v", name, expectedProfile, actualProfile)
		}
	}
}

func asJSON(x interface{}) []byte {
	result, err := json.Marshal(x)
	if err != nil {
		panic(err)
	}
	return result
}
