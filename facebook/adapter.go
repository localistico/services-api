package facebook

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/localistico/services-api/helpers"
	"github.com/localistico/services-api/service"
)

type apiProfileResponse struct {
	ID           string              `json:"id"`
	BestPage     *apiProfileResponse `json:"best_page"`
	URL          string              `json:"link"`
	Website      string              `json:"website"`
	Name         string              `json:"name"`
	Location     apiLocation         `json:"location"`
	CategoryList []apiCategory       `json:"category_list"`
	Phone        string              `json:"phone"`
	Hours        map[string]string   `json:"hours"`
	Unclaimed    bool                `json:"is_unclaimed"`
	Description  string              `json:"description"`
	About        string              `json:"about"`

	Error *apiError `json:"error"`
}

type apiCategory struct {
	Name string `json:"name"`
}

func (r *apiProfileResponse) ParseHours() (service.ProfileHours, error) {
	h := make(map[string]map[string]map[string]string)
	var hours service.ProfileHours
	for key, value := range r.Hours {
		parts := strings.Split(key, "_")
		if len(parts) != 3 {
			return hours, fmt.Errorf("cannot process key %s", key)
		}
		if h[parts[0]] == nil {
			h[parts[0]] = make(map[string]map[string]string)
		}
		if h[parts[0]][parts[1]] == nil {
			h[parts[0]][parts[1]] = make(map[string]string)
		}
		h[parts[0]][parts[1]][parts[2]] = value
	}
	for day, timeframes := range h {
		for _, times := range timeframes {
			if len(times) != 2 {
				// Malformed timeframe with only mon_1_open or mon_1_close, skipping
				continue
			}
			openTime, err := time.Parse("15:04", times["open"])
			if err != nil {
				return hours, err
			}
			closeTime, err := time.Parse("15:04", times["close"])
			if err != nil {
				return hours, err
			}

			times := fmt.Sprintf("%s - %s", openTime.Format("15:04"), closeTime.Format("15:04"))

			switch day {
			case "mon":
				hours.Mon = append(hours.Mon, times)
				sort.Strings(hours.Mon)
			case "tue":
				hours.Tue = append(hours.Tue, times)
				sort.Strings(hours.Tue)
			case "wed":
				hours.Wed = append(hours.Wed, times)
				sort.Strings(hours.Wed)
			case "thu":
				hours.Thu = append(hours.Thu, times)
				sort.Strings(hours.Thu)
			case "fri":
				hours.Fri = append(hours.Fri, times)
				sort.Strings(hours.Fri)
			case "sat":
				hours.Sat = append(hours.Sat, times)
				sort.Strings(hours.Sat)
			case "sun":
				hours.Sun = append(hours.Sun, times)
				sort.Strings(hours.Sun)
			default:
				return hours, fmt.Errorf("unknown day %#v (%#v)", day, r.Hours)
			}
		}
	}
	return hours, nil
}

func (r *apiProfileResponse) Categories() []string {
	categories := []string{}
	for _, category := range r.CategoryList {
		categories = append(categories, category.Name)
	}
	return categories
}

func (r *apiProfileResponse) Address() (service.Address, error) {
	var err error
	l := r.Location
	address := service.Address{}
	address.StreetAddress = l.Street
	address.Locality = l.City
	address.Postcode = l.Postcode
	if l.Country != "" {
		address.Country, err = helpers.CountryCodeFromName(l.Country)
	}
	return address, err
}

func adapt(response *apiProfileResponse) (service.Profile, error) {
	var err error
	profile := service.Profile{}
	profile.Service = "facebook"
	profile.ID = response.ID
	profile.URL = response.URL
	profile.Website = response.Website
	profile.Name = response.Name
	profile.SetPhone(response.Phone)
	profile.Lat = response.Location.Lat
	profile.Lng = response.Location.Lng
	profile.Categories = response.Categories()
	profile.Summary = response.Description
	if profile.Summary == "" {
		profile.Summary = response.About
	}
	profile.Verified = !response.Unclaimed
	if profile.Hours, err = response.ParseHours(); err != nil {
		return profile, err
	}
	if profile.Address, err = response.Address(); err != nil {
		return profile, err
	}

	return profile, nil
}
