package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/localistico/services-api/service"
	"github.com/jinzhu/copier"
)

type serviceCache struct {
	svc                service.ProfileService
	searchStore        ResourceStore
	profileStore       ResourceStore
	profileStatusStore ResourceStore
	TTL                time.Duration
}

type searchResult struct {
	CreatedAt time.Time
	ID        string
	Name      string
	Location  string
	Found     []service.ProfileFound
}

type profileResult struct {
	CreatedAt time.Time
	ID        string
	Profile   service.Profile
}

type profileStatusResult struct {
	CreatedAt     time.Time
	ID            string
	ProfileStatus service.ProfileStatus
}

func newSearchLocationResult(name, loc string) searchResult {
	return searchResult{
		ID:        fmt.Sprintf("name(%s)-location(%s)", name, loc),
		Name:      name,
		Location:  loc,
		CreatedAt: time.Now(),
	}
}

func newSearchAroundResult(name string, lat, lng float64, radius int) searchResult {
	coordinates := fmt.Sprintf("%f,%f", lat, lng)
	return searchResult{
		ID:        fmt.Sprintf("name(%s)-center(%s)-radius(%d)", name, coordinates, radius),
		Name:      name,
		Location:  coordinates,
		CreatedAt: time.Now(),
	}
}

func newProfileResult(id string) profileResult {
	return profileResult{
		ID:        id,
		CreatedAt: time.Now(),
	}
}

func newProfileStatusResult(id string) profileStatusResult {
	return profileStatusResult{
		ID:        id,
		CreatedAt: time.Now(),
	}
}

func newServiceCache(svc service.ProfileService, searchStore, profileStore, profileStatusStore ResourceStore) *serviceCache {
	return &serviceCache{
		svc:                svc,
		searchStore:        searchStore,
		profileStore:       profileStore,
		profileStatusStore: profileStatusStore,
		TTL:                24 * time.Hour * 365,
	}
}

func (s *serviceCache) Service() string {
	return s.svc.Service()
}

func (s *serviceCache) GetTransport() http.RoundTripper {
	return s.svc.GetTransport()
}

func (s *serviceCache) SetTransport(t http.RoundTripper) {
	s.svc.SetTransport(t)
}

func (s *serviceCache) CheckProfileStatus(p *service.ProfileStatus) error {
	var expired = false
	if p == nil {
		return errors.New("no profile provided")
	}
	result := newProfileStatusResult(p.ID)
	err := s.profileStatusStore.FindID(result.ID, &result)
	if err == nil {
		maxAge := result.CreatedAt.Add(s.TTL)
		if maxAge.Before(time.Now()) {
			expired = true
			err = ErrNotFound
		}
	}
	if err == ErrNotFound {
		err := s.svc.CheckProfileStatus(p)
		if err != nil {
			return err
		}
		result.ProfileStatus = *p
		if expired {
			log.Printf("cache=expired type=profile_status service=%s key=%s\n", s.Service(), result.ID)
			result.CreatedAt = time.Now()
			s.profileStatusStore.UpdateID(result.ID, result)
		} else {
			log.Printf("cache=miss type=profile_status service=%s key=%s\n", s.Service(), result.ID)
			s.profileStatusStore.Insert(&result)
		}
	} else if err == nil {
		log.Printf("cache=hit type=profile_status service=%s key=%s\n", s.Service(), result.ID)
		copier.Copy(p, &result.ProfileStatus)
	} else {
		return err
	}
	return nil
}

func (s *serviceCache) SearchLocation(name string, loc string) (found []service.ProfileFound, err error) {
	result := newSearchLocationResult(name, loc)
	return s.searchWithCache(result, func() (found []service.ProfileFound, err error) {
		return s.svc.SearchLocation(name, loc)
	})
}

func (s *serviceCache) SearchAround(name string, lat, lng float64, radius int) (found []service.ProfileFound, err error) {
	result := newSearchAroundResult(name, lat, lng, radius)
	return s.searchWithCache(result, func() (found []service.ProfileFound, err error) {
		return s.svc.SearchAround(name, lat, lng, radius)
	})
}

func (s *serviceCache) searchWithCache(result searchResult, search func() (found []service.ProfileFound, err error)) (found []service.ProfileFound, err error) {
	var expired = false
	if s.searchStore == nil {
		err = ErrNotFound
	} else {
		err = s.searchStore.FindID(result.ID, &result)
		if err == nil {
			maxAge := result.CreatedAt.Add(s.TTL)
			if maxAge.Before(time.Now()) {
				expired = true
				err = ErrNotFound
			}
		}
	}
	if err == ErrNotFound {
		found, err = search()
		if err != nil {
			return
		}
		result.Found = found
		result.CreatedAt = time.Now()
		if s.searchStore != nil {
			if expired {
				log.Printf("cache=expired type=search service=%s key=%s\n", s.Service(), result.ID)
				s.searchStore.UpdateID(result.ID, result)
			} else {
				log.Printf("cache=miss type=search service=%s key=%s\n", s.Service(), result.ID)
				s.searchStore.Insert(&result)
			}
		}
	} else {
		log.Printf("cache=hit type=search service=%s key=%s\n", s.Service(), result.ID)
		found = result.Found
	}
	return
}

func (s *serviceCache) Profile(id string) (profile service.Profile, err error) {
	var expired = false
	result := newProfileResult(id)
	err = s.profileStore.FindID(result.ID, &result)
	if err == nil {
		maxAge := result.CreatedAt.Add(s.TTL)
		if maxAge.Before(time.Now()) {
			expired = true
			err = ErrNotFound
		}
	}
	if err == ErrNotFound {
		profile, err = s.svc.Profile(id)
		if err != nil {
			return profile, err
		}
		result.Profile = profile
		if expired {
			log.Printf("cache=expired type=profile service=%s key=%s\n", s.Service(), result.ID)
			result.CreatedAt = time.Now()
			s.profileStore.UpdateID(result.ID, result)
		} else {
			log.Printf("cache=miss type=profile service=%s key=%s\n", s.Service(), result.ID)
			s.profileStore.Insert(&result)
		}
	} else {
		log.Printf("cache=hit type=profile service=%s key=%s\n", s.Service(), result.ID)
		profile = result.Profile
	}
	return profile, err
}
