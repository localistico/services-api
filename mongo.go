package main

import (
	"log"

	"github.com/gocraft/web"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

type mgoCollStore struct {
	db   *mgo.Database
	coll *mgo.Collection
}

// All fills the specified list with the contents from the mongodb collection
func (s *mgoCollStore) All(list interface{}) error {
	err := s.coll.Find(nil).All(list)
	if err == mgo.ErrNotFound {
		err = ErrNotFound
	}
	return err
}

// Insert stores the item in the collection
func (s *mgoCollStore) Insert(item interface{}) error {
	return s.coll.Insert(item)
}

// FindID search for an element with the specified ID in the collection and returns ErrNotFound if it's not in the collection
func (s *mgoCollStore) FindID(id string, item interface{}) error {
	err := s.coll.Find(bson.M{"id": id}).One(item)
	if err == mgo.ErrNotFound {
		err = ErrNotFound
	}
	return err
}

// UpdateID stores the new item in the collection record with the specified id
func (s *mgoCollStore) UpdateID(id string, item interface{}) error {
	err := s.coll.Update(bson.M{"id": id}, item)
	if err == mgo.ErrNotFound {
		err = ErrNotFound
	}
	return err
}

// RemoveID removes the collection element with the specified id
func (s *mgoCollStore) RemoveID(id string) error {
	err := s.coll.Remove(bson.M{"id": id})
	if err == mgo.ErrNotFound {
		err = ErrNotFound
	}
	return err
}

// NewCollectionStore returns a Store backed by a mongodb collection with the specified name
func NewCollectionStore(db *mgo.Database, name string) ResourceStore {
	return &mgoCollStore{
		db:   db,
		coll: db.C(name),
	}
}

// NewSetCollectionMiddleware returns a middleware that sets Context store to the mongodb collection with the specified name
func NewSetCollectionMiddleware(collection string) func(*Context, web.ResponseWriter, *web.Request, web.NextMiddlewareFunc) {
	return func(
		ctx *Context,
		rw web.ResponseWriter,
		req *web.Request,
		next web.NextMiddlewareFunc,
	) {
		ctx.Store = NewCollectionStore(ctx.DB, collection)
		next(rw, req)
	}
}

func connectDBMiddleware(mongoURL *string) func(*Context, web.ResponseWriter, *web.Request, web.NextMiddlewareFunc) {
	session, err := mgo.Dial(*mongoURL)
	if err != nil {
		log.Fatal(err)
	}
	session.SetSafe(&mgo.Safe{W: 1})
	database := session.DB("")
	return func(
		ctx *Context,
		rw web.ResponseWriter,
		req *web.Request,
		next web.NextMiddlewareFunc,
	) {
		ctx.DB = database
		next(rw, req)
	}
}
