package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"regexp"

	"github.com/gocraft/web"
	"github.com/localistico/services-api/extract"
	"github.com/stvp/rollbar"
)

// PlacesContext contains the services to extract places
type PlacesContext struct {
	*Context
	Extractor  *extract.Extractor
	Backend    extract.ExtractionsBackend
	SlackToken string
}

// PlacesTest is fetched by the slack channel
func (c *PlacesContext) PlacesTest(res web.ResponseWriter, req *web.Request) {
	locality, _ := url.QueryUnescape(req.PathParams["locality"])
	place, _ := url.QueryUnescape(req.PathParams["place"])

	res.Header().Set("Location", reportURL(locality, place))
	res.WriteHeader(http.StatusFound)
}

type iftttRequest struct {
	Title      string   `json:"title"`
	Categories []string `json:"categories"`
}

func reportURL(locality, place string) string {
	return fmt.Sprintf("http://report-staging.localistico.com/%s/%s", url.QueryEscape(locality), url.QueryEscape(place))
}

// IFTTT endpoint for ifttt webhook notifying of new reviews in yelp
func (c *PlacesContext) IFTTT(res web.ResponseWriter, req *web.Request) {
	defer req.Body.Close()
	var request iftttRequest
	json.NewDecoder(req.Body).Decode(&request)

	exp := regexp.MustCompile(`.+Review of (.+) - .+ \(`)
	locality := request.Categories[0]
	match := exp.FindStringSubmatch(request.Title)
	if len(match) != 2 {
		c.Logger.Panicf("Failed match for %#v results in %#v", request.Title, match)
		return
	}

	place := match[1]

	extraction := &extract.Extraction{
		Locations: []string{locality},
		Names:     []string{place},
	}
	err := extraction.VerifyValid()
	if err != nil {
		rollbar.RequestError("error", req.Request, err)
		return
	}
	err = c.Backend.FindOrCreateExtraction(extraction)
	if err != nil {
		rollbar.RequestError("error", req.Request, err)
		return
	}
	c.Extractor.Extract(extraction)

	reportURL := reportURL(locality, place)
	if c.SlackToken != "" {
		message := slackMessage{
			Text:     fmt.Sprintf("Review in %s (%s) - %s", place, locality, url.QueryEscape(reportURL)),
			Channel:  "#reports",
			Username: "api",
		}
		err := message.sendTo("espresso.slack.com", c.SlackToken)
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			c.Logger.Panic(err)
			return
		}
		res.WriteHeader(http.StatusOK)
		c.Logger.Printf("Sent to slack: %s", message.Text)
		return
	}
	// FIXME: create extraction here
	c.Logger.Printf("webhook=%s", reportURL)
}

func placesEndpoint(router *web.Router, path string, slackToken string, extractor *extract.Extractor, backend extract.ExtractionsBackend) {
	subrouter := router.Subrouter(PlacesContext{}, path)
	subrouter.Middleware(placesInitMiddleware(slackToken, extractor, backend))
	subrouter.Post("/ifttt", (*PlacesContext).IFTTT)
	subrouter.Get("/:locality/:place", (*PlacesContext).PlacesTest)
}

func placesInitMiddleware(slackToken string, extractor *extract.Extractor, backend extract.ExtractionsBackend) func(*PlacesContext, web.ResponseWriter, *web.Request, web.NextMiddlewareFunc) {
	return func(
		c *PlacesContext,
		rw web.ResponseWriter,
		req *web.Request,
		next web.NextMiddlewareFunc,
	) {
		c.SlackToken = slackToken
		c.Extractor = extractor
		c.Backend = backend
		next(rw, req)
	}
}
