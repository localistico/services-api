package foursquare

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"github.com/localistico/services-api/service"
	"github.com/kellydunn/golang-geo"
)

const (
	baseURL          = "https://api.foursquare.com/v2"
	searchPath       = "/venues/search"
	placeDetailsPath = "/venues/%s"
)

// Service implements service.ProfileService
type Service struct {
	ClientID     string
	ClientSecret string
	client       *http.Client
}

// NewService creates a new Service object
func NewService(clientID, clientSecret string) (s *Service, err error) {
	if clientID == "" || clientSecret == "" {
		return nil, errors.New("need to provide API credentials")
	}
	return &Service{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		client:       &http.Client{},
	}, nil
}

// SetTransport sets the http transport used for the API requests
func (s *Service) SetTransport(t http.RoundTripper) {
	s.client.Transport = t
}

// GetTransport returns the http transport used for the API requests
func (s *Service) GetTransport() http.RoundTripper {
	t := s.client.Transport
	if t == nil {
		return http.DefaultTransport
	}
	return t
}

func (s *Service) defaultParams() url.Values {
	var v = url.Values{}
	v.Set("v", "20140602")
	v.Set("client_id", s.ClientID)
	v.Set("client_secret", s.ClientSecret)
	v.Set("locale", "en")
	return v
}

func (s *Service) defaultSearchParams() url.Values {
	v := s.defaultParams()
	v.Set("intent", "global")
	v.Set("limit", "50")
	return v
}

// Service returns the name of the service
func (s *Service) Service() string {
	return "foursquare"
}

// SearchLocation geocode the loc location and
// search for profiles around that location
func (s *Service) SearchLocation(name string, loc string) (found []service.ProfileFound, err error) {
	query := s.defaultSearchParams()
	query.Set("query", name)
	query.Set("near", loc)
	url := baseURL + searchPath + "?" + query.Encode()

	response := &apiSearchResponse{}
	err = s.requestSearch(url, response)
	if err != nil {
		return found, err
	}

	bounds, err := response.Bounds()
	if err != nil {
		return found, err
	}

	filter := service.MatchesAllFilter(
		service.SimilarNameFilter(name),
		service.WithinBoundsFilter(bounds),
	)
	return s.filterResults(filter, response)
}

// SearchLocationCh geocode the loc location and
// search for profiles around that location
func (s *Service) SearchLocationCh(name string, loc string) (<-chan []service.ProfileFound, <-chan error) {
	found := make(chan []service.ProfileFound)
	errCh := make(chan error)
	go func() {
		defer close(found)
		defer close(errCh)
		var err error
		f, err := s.SearchLocation(name, loc)
		if err != nil {
			errCh <- err
			return
		}
		found <- f
	}()
	return found, errCh
}

// SearchBBox search for business profiles inside
// bounding box specified by sw (south wet) and
// ne (north east) coordinates
func (s *Service) SearchBBox(name string, sw *geo.Point, ne *geo.Point) (found []service.ProfileFound, err error) {
	panic("SearchBBox not implemented")
}

type apiSearchResult struct {
	ID       string      `json:"id"`
	Name     string      `json:"name"`
	Location apiLocation `json:"location"`
}

type apiLocation struct {
	Lat      float64 `json:"lat"`
	Lng      float64 `json:"lng"`
	City     string  `json:"city"`
	Postcode string  `json:"postalCode"`
	Street   string  `json:"address"`
	Country  string  `json:"cc"`
	Region   string  `json:"state"`
}

func (l *apiLocation) Point() *geo.Point {
	return geo.NewPoint(l.Lat, l.Lng)
}

type apiSearchResults struct {
	Venues  []apiSearchResult `json:"venues"`
	Geocode *apiGeocoding     `json:"geocode"`
}

type apiSearchResponse struct {
	Meta    apiMetaResponse  `json:"meta"`
	Results apiSearchResults `json:"response"`
	Error   string           `json:"error_message"`
}

func (r *apiSearchResponse) Bounds() (*geo.Polygon, error) {
	if r.Results.Geocode == nil {
		return nil, errors.New("no geocode in API response")
	}
	if r.Results.Geocode.Feature.Geometry.Bounds == nil {
		return nil, errors.New("no bounds in feature")
	}
	ne := r.Results.Geocode.Feature.Geometry.Bounds.NE.Point()
	sw := r.Results.Geocode.Feature.Geometry.Bounds.SW.Point()
	se := geo.NewPoint(sw.Lat(), ne.Lng())
	nw := geo.NewPoint(ne.Lat(), sw.Lng())
	bounds := geo.NewPolygon([]*geo.Point{
		ne,
		se,
		sw,
		nw,
		ne,
	})
	return bounds, nil
}

type apiGeocoding struct {
	Feature apiGeofeature `json:"feature"`
}

type apiGeofeature struct {
	Geometry apiGeometry `json:"geometry"`
}

type apiGeometry struct {
	Bounds *apiBounds `json:"bounds"`
}

type apiBounds struct {
	NE apiLocation `json:"ne"`
	SW apiLocation `json:"sw"`
}

type apiMetaResponse struct {
	Code        int    `json:"code"`
	ErrorType   string `json:"errorType"`
	ErrorDetail string `json:"errorDetail"`
}

func (s *Service) request(url string, response interface{}) error {
	res, err := s.client.Get(url)
	if err != nil {
		return err
	}

	defer res.Body.Close()
	return json.NewDecoder(res.Body).Decode(&response)
}

func (s *Service) requestProfile(url string, response *apiProfileResponse) error {
	err := s.request(url, response)
	if err != nil {
		return err
	}

	switch response.Meta.Code {
	case 200:
		break
	default:
		return fmt.Errorf("%d %s: %s", response.Meta.Code, response.Meta.ErrorType, response.Meta.ErrorDetail)
	}
	return nil
}

func (s *Service) requestSearch(url string, response *apiSearchResponse) error {
	err := s.request(url, response)
	if err != nil {
		return err
	}

	switch response.Meta.Code {
	case 200:
		break
	default:
		return fmt.Errorf("%d %s: %s", response.Meta.Code, response.Meta.ErrorType, response.Meta.ErrorDetail)
	}
	return nil
}

// SearchAround searches for venues around the
// specified coordinates. radius for search is
// specified in meters
func (s *Service) SearchAround(name string, lat, lng float64, radius int) (found []service.ProfileFound, err error) {
	query := s.defaultSearchParams()
	query.Set("intent", "browse")
	query.Set("query", name)
	query.Set("ll", fmt.Sprintf("%f,%f", lat, lng))
	query.Set("radius", fmt.Sprintf("%d", radius))
	url := baseURL + searchPath + "?" + query.Encode()

	var response = &apiSearchResponse{}
	err = s.requestSearch(url, response)
	if err != nil {
		return found, err
	}

	filter := service.SimilarNameFilter(name)
	return s.filterResults(filter, response)
}

func (s *Service) filterResults(filter service.FilterFunc, response *apiSearchResponse) ([]service.ProfileFound, error) {
	var (
		found []service.ProfileFound
		err   error
	)

	for _, result := range response.Results.Venues {
		profile := service.ProfileFound{
			Service:     "foursquare",
			ID:          result.ID,
			Name:        result.Name,
			Coordinates: geo.NewPoint(result.Location.Lat, result.Location.Lng),
		}
		var matched bool
		if matched, err = filter(profile); !matched {
			profile.Discarded = true
		}
		found = append(found, profile)
		if err != nil {
			return found, err
		}
	}

	return found, nil
}

// Profile extracts information for the profile with the specified id
func (s *Service) Profile(id string) (profile service.Profile, err error) {
	url := s.apiProfileURL(id)
	var response apiProfileResponse
	err = s.requestProfile(url, &response)
	if err != nil {
		return profile, err
	}

	return s.adapt(&response.Result.Venue)
}

func (s *Service) apiProfileURL(id string) string {
	query := s.defaultParams()
	return baseURL + fmt.Sprintf(placeDetailsPath, id) + "?" + query.Encode()
}

// CheckProfileStatus checks whether the passed profile has been merged or deleted and fills the appropriate field
func (s *Service) CheckProfileStatus(p *service.ProfileStatus) error {
	if p.ID == "" {
		return errors.New("no profile id")
	}
	if p.Service != s.Service() {
		return errors.New("profile from a different service")
	}

	url := s.apiProfileURL(p.ID)
	var response apiProfileResponse
	err := s.requestProfile(url, &response)
	if err != nil {
		return err
	}

	if p.ID != response.Result.Venue.ID {
		p.BestID = response.Result.Venue.ID
	}

	if response.Result.Venue.Deleted {
		p.Deleted = true
	}

	return nil
}
