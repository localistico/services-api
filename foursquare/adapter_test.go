package foursquare

import (
	"reflect"
	"testing"
)

type test struct {
	name     string
	src      string
	expected string
}

var tests = []test{
	test{
		name:     "missing pm",
		src:      "8:00 – 11:00 pm",
		expected: "8:00pm - 11:00pm",
	},
	test{
		name:     "missing pm with dots",
		src:      "8:00 – 11:00 p.m.",
		expected: "8:00p.m. - 11:00p.m.",
	},
	test{
		name:     "existing am",
		src:      "8:00 am – 11:00 pm",
		expected: "8:00am - 11:00pm",
	},
	test{
		name:     "existing am with dots",
		src:      "8:00 a.m. – 11:00 p.m.",
		expected: "8:00a.m. - 11:00p.m.",
	},
}

func TestNormalizedTimes(t *testing.T) {
	for _, test := range tests {
		time := apiTimeframe{
			Days: "Mon-Thu",
			Open: []apiTime{
				apiTime{
					Time: test.src,
				},
			},
		}
		actual := time.normalizedTimes()[0]
		expected := test.expected
		if actual != expected {
			t.Errorf("%s\nExpected: %#v\nReceived: %#v", test.name, expected, actual)
		}
	}
}

var testParseTimes = []testParseTime{
	testParseTime{
		name: "empty array",
		src:  []string{},
	},
	testParseTime{
		name: "am/pm names",
		src: []string{
			"8:11–11:00 pm",
			"8:20pm –11:00 pm",
			"9:30p.m. –11:00 pm",
			"09:40a.m. –11:00 pm",
			"9:50 - 21:00",
			"24 Hours",
			"7:30 AM-Midnight",
			"Midnight-7:30 AM",
			"7:30 AM-Noon",
			"Noon-7:30 PM",
		},
		expected: []string{
			"20:11 - 23:00",
			"20:20 - 23:00",
			"21:30 - 23:00",
			"09:40 - 23:00",
			"09:50 - 21:00",
			"00:00 - 00:00",
			"07:30 - 00:00",
			"00:00 - 07:30",
			"07:30 - 12:00",
			"12:00 - 19:30",
		},
	},
}

type testParseTime struct {
	name     string
	src      []string
	expected []string
}

func mapTimes(src []string) []apiTime {
	var result []apiTime
	for _, t := range src {
		result = append(result, apiTime{Time: t})
	}
	return result
}

func TestParseTimes(t *testing.T) {
	for _, test := range testParseTimes {
		name := test.name
		src := apiTimeframe{Open: mapTimes(test.src)}
		expected := test.expected
		actual, err := parseTimes(src.normalizedTimes())
		if err != nil {
			t.Fatalf("%s error: %s", name, err)
		}

		if !reflect.DeepEqual(expected, actual) {
			t.Errorf("%s wrong.\nExpected: %#v\nReceived: %#v", name, expected, actual)
		}
	}
}
