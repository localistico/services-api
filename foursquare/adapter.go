package foursquare

import (
	"fmt"
	"log"
	"net/url"
	"regexp"
	"sort"
	"strings"

	"github.com/localistico/services-api/helpers"
	"github.com/localistico/services-api/service"
)

type apiProfileResponse struct {
	Meta   apiMetaResponse  `json:"meta"`
	Result apiProfileResult `json:"response"`
}

type apiProfileResult struct {
	Venue apiProfileVenue `json:"venue"`
}

type apiProfileVenue struct {
	ID           string            `json:"id"`
	URL          string            `json:"canonicalUrl"`
	Website      string            `json:"url"`
	Name         string            `json:"name"`
	Location     apiLocation       `json:"location"`
	Contact      apiProfileContact `json:"contact"`
	CategoryList []apiCategory     `json:"categories"`
	Hours        apiHours          `json:"hours"`
	Verified     bool              `json:"verified"`
	Rating       *float32          `json:"rating"`
	RatingsCount float32           `json:"ratingSignals"`
	Description  string            `json:"description"`
	Tips         apiProfileTips    `json:"tips"`
	Deleted      bool              `json:"deleted"`
}

type apiHours struct {
	Timeframes []apiTimeframe `json:"timeframes"`
}

type apiTimeframe struct {
	Days string    `json:"days"`
	Open []apiTime `json:"open"`
}

type apiTime struct {
	Time string `json:"renderedTime"`
}

type apiProfileContact struct {
	Phone string `json:"phone"`
}

type apiProfileTips struct {
	Count int `json:"count"`
}

type apiCategory struct {
	Name string `json:"name"`
}

var weekDays = sort.StringSlice(strings.Split("Mon Tue Wed Thu Fri Sat Sun", " "))

var timeframeRegex = regexp.MustCompile(`(\d{1,2}:\d{2}|midnight|noon) *([ap]\.?m\.?)?[^\d]+(\d{1,2}:\d{2}|midnight|noon) *([ap]\.?m\.?)?`)

func (s apiTimeframe) normalizedTimes() []string {
	var result []string
	for _, t := range s.Open {
		time := strings.ToLower(t.Time)
		var match []string
		switch time {
		case "24 hours":
			match = []string{time, "00:00", "", "00:00", ""}
		default:
			match = timeframeRegex.FindStringSubmatch(time)
			if match == nil {
				log.Printf("Error for %#v with times: %#v", s.Days, s.Open)
				log.Panicf("cannot parse time frame: %#v", time)
			}
		}
		switch match[1] {
		case "midnight":
			match[1] = "12:00"
			match[2] = "am"
			break
		case "noon":
			match[1] = "12:00"
			match[2] = "pm"
			break
		default:
			if match[2] == "" {
				match[2] = match[4]
			}
		}
		switch match[3] {
		case "midnight":
			match[3] = "12:00"
			match[4] = "am"
			break
		case "noon":
			match[3] = "12:00"
			match[4] = "pm"
			break
		}
		result = append(result, fmt.Sprintf("%s%s - %s%s", match[1], match[2], match[3], match[4]))
	}
	return result
}

func parseTimes(times []string) ([]string, error) {
	var result []string
	for _, timeframe := range times {
		t := helpers.ParseTimes(timeframe)
		if len(t) != 2 {
			return result, fmt.Errorf("invalid timeframe: %s (returned times %s)", timeframe, t)
		}
		from := t[0].Format("15:04")
		to := t[1].Format("15:04")
		result = append(result, fmt.Sprintf("%s - %s", from, to))
	}
	return result, nil
}

func (r *apiProfileVenue) ParseHours() (service.ProfileHours, error) {
	var hours service.ProfileHours
	for _, tf := range r.Hours.Timeframes {
		days := strings.Split(tf.Days, "–")
		from := days[0]
		to := days[len(days)-1]
		times, err := parseTimes(tf.normalizedTimes())
		if err != nil {
			return hours, err
		}
		var matched = false
		for i, day := range weekDays {
			if day == from {
				matched = true
			}
			if matched == false {
				continue
			}
			switch day {
			case "Mon":
				hours.Mon = times
			case "Tue":
				hours.Tue = times
			case "Wed":
				hours.Wed = times
			case "Thu":
				hours.Thu = times
			case "Fri":
				hours.Fri = times
			case "Sat":
				hours.Sat = times
			case "Sun":
				hours.Sun = times
			default:
				return hours, fmt.Errorf("unknown day %#v (%#v)", weekDays[i], r.Hours)
			}
			if day == to {
				break
			}
		}
	}
	return hours, nil
}

func (r *apiProfileVenue) Categories() []string {
	categories := []string{}
	for _, category := range r.CategoryList {
		categories = append(categories, category.Name)
	}
	return categories
}

func (r *apiProfileVenue) Address() (service.Address, error) {
	var err error
	l := r.Location
	address := service.Address{}
	address.StreetAddress = l.Street
	address.Locality = l.City
	address.Postcode = l.Postcode
	address.Region = l.Region
	address.Country = l.Country
	return address, err
}

func (s *Service) adapt(response *apiProfileVenue) (service.Profile, error) {
	var err error
	profile := service.Profile{}
	profile.Service = "foursquare"
	profile.ID = response.ID
	profile.URL = response.URL
	if profile.URL != "" {
		uri, err := url.Parse(profile.URL)
		if err != nil {
			return profile, err
		}
		values := uri.Query()
		values.Set("ref", s.ClientID)
		uri.RawQuery = values.Encode()
		profile.URL = uri.String()
	}
	profile.Name = response.Name
	profile.SetPhone(response.Contact.Phone)
	profile.Lat = response.Location.Lat
	profile.Lng = response.Location.Lng
	profile.Categories = response.Categories()
	profile.Summary = response.Description
	profile.Website = response.Website
	profile.Verified = response.Verified
	if response.RatingsCount > 0 {
		profile.Rating = response.Rating
	}
	profile.MaxRating = service.NewInt(10)
	profile.TotalReviews = service.NewInt(response.Tips.Count)
	if profile.Hours, err = response.ParseHours(); err != nil {
		return profile, err
	}
	if profile.Address, err = response.Address(); err != nil {
		return profile, err
	}

	return profile, nil
}
