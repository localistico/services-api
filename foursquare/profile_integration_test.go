// +build integration

package foursquare

import (
	"encoding/json"
	"flag"
	"os"
	"reflect"
	"testing"

	"github.com/localistico/services-api/service"
	"github.com/kellydunn/golang-geo"
)

var (
	foursquareClientID     = flag.String("foursquare-client-id", os.Getenv("FOURSQUARE_CLIENT_ID"), "Client ID for the foursquare API")
	foursquareClientSecret = flag.String("foursquare-client-secret", os.Getenv("FOURSQUARE_CLIENT_SECRET"), "Client Secret for the foursquare API")
	svc                    service.ProfileService
)

func init() {
	var err error
	svc, err = NewService(*foursquareClientID, *foursquareClientSecret)
	if err != nil {
		panic(err)
	}
}

func TestSearchAround(t *testing.T) {
	name := "Pimlico Fresh"
	center := geo.NewPoint(51.691544, 0.33409)
	radius := 50000
	found, rejected, err := service.SplitSearch(svc.SearchAround(name, center.Lat(), center.Lng(), radius))
	expectedFound := []service.ProfileFound{
		service.ProfileFound{
			Service:     "foursquare",
			Name:        "Pimlico Fresh",
			ID:          "4b5303c1f964a5209f8c27e3",
			Coordinates: geo.NewPoint(51.492438, -0.140163),
		},
	}
	expectedTotalFound := 1
	expectedTotalRejected := 49
	if err != nil {
		t.Error(err)
	}
	if actualTotalFound := len(found); actualTotalFound != expectedTotalFound {
		t.Errorf("Expected %d profiles searching for %s.\nGot: %d\n%#v", expectedTotalFound, name, actualTotalFound, found)
	} else if !reflect.DeepEqual(found, expectedFound) {
		t.Errorf("Fail %s\nExpected: %s\nReceived: %s", name, asJSON(expectedFound), asJSON(found))
	}
	if actualTotalRejected := len(rejected); actualTotalRejected != expectedTotalRejected {
		t.Errorf("Expected %d rejected profiles searching for %s.\nGot: %d\n%#v", expectedTotalRejected, name, actualTotalRejected, rejected)
	}
}

func TestSearchLocation(t *testing.T) {
	name := "Pimlico Fresh"
	location := "London"
	found, rejected, err := service.SplitSearch(svc.SearchLocation(name, location))
	expectedFound := []service.ProfileFound{
		service.ProfileFound{
			Service:     "foursquare",
			Name:        "Pimlico Fresh",
			ID:          "4b5303c1f964a5209f8c27e3",
			Coordinates: geo.NewPoint(51.492438, -0.140163),
		},
	}
	expectedTotalFound := 1
	expectedTotalRejected := 49
	if err != nil {
		t.Error(err)
	}
	if actualTotalFound := len(found); actualTotalFound != expectedTotalFound {
		t.Errorf("Expected %d profiles searching for %s.\nGot: %d\n%#v", expectedTotalFound, name, actualTotalFound, found)
	} else if !reflect.DeepEqual(found, expectedFound) {
		t.Errorf("Fail %s\nExpected: %#v\nReceived: %#v", name, expectedFound, found)
	}
	if actualTotalRejected := len(rejected); actualTotalRejected != expectedTotalRejected {
		t.Errorf("Expected %d rejected profiles searching for %s.\nGot: %d\n%#v", expectedTotalRejected, name, actualTotalRejected, rejected)
	}
}

type testProfile struct {
	name     string
	location string
	expected service.Profile
}

var testProfiles = []testProfile{

	testProfile{
		name:     "Pimlico Fresh",
		location: "London",
		expected: service.Profile{
			Service:  "foursquare",
			ID:       "4b5303c1f964a5209f8c27e3",
			URL:      "https://foursquare.com/v/pimlico-fresh/4b5303c1f964a5209f8c27e3?ref=" + *foursquareClientID,
			Name:     "Pimlico Fresh",
			Verified: false,
			Lat:      51.492438,
			Lng:      -0.140163,
			Address: service.Address{
				StreetAddress: "86 Wilton Rd",
				Postcode:      "S W1V",
				Locality:      "London",
				Region:        "Greater London",
				Country:       "GB",
			},
			Phone: "+442079320030",
			Categories: []string{
				"Café",
				"Vegetarian / Vegan Restaurant",
				"Breakfast Spot",
			},
			Website:      "",
			Rating:       service.NewFloat32(8.63),
			MaxRating:    service.NewInt(10),
			TotalReviews: service.NewInt(50),
			Summary:      "Stylish cafe with outside tables and daily changing blackboard menu for soups and stews.",
			Hours: service.ProfileHours{
				Mon: []string{"07:30 - 20:00"},
				Tue: []string{"07:30 - 20:00"},
				Wed: []string{"07:30 - 20:00"},
				Thu: []string{"07:30 - 20:00"},
				Fri: []string{"07:30 - 20:00"},
				Sat: []string{"09:00 - 18:00"},
				Sun: []string{"09:00 - 18:00"},
			},
		},
	},

	testProfile{
		name:     "Gastronomica",
		location: "London",
		expected: service.Profile{
			Service:  "foursquare",
			ID:       "4ad07fa5f964a5203bd820e3",
			URL:      "https://foursquare.com/v/gastronomica/4ad07fa5f964a5203bd820e3?ref=" + *foursquareClientID,
			Name:     "Gastronomica",
			Verified: false,
			Lat:      51.491885,
			Lng:      -0.138381,
			Address: service.Address{
				StreetAddress: "45 Tachbrook St",
				Postcode:      "SW1V 2LZ",
				Locality:      "Pimlico",
				Region:        "Greater London",
				Country:       "GB",
			},
			Phone:        "+442072336656",
			Categories:   []string{"Café"},
			Website:      "",
			MaxRating:    service.NewInt(10),
			TotalReviews: service.NewInt(9),
			Summary:      "",
		},
	},

	testProfile{
		name:     "Home Burger",
		location: "Madrid",
		expected: service.Profile{
			Service:  "foursquare",
			ID:       "4adcda3bf964a520813d21e3",
			URL:      "https://foursquare.com/v/home-burger/4adcda3bf964a520813d21e3?ref=" + *foursquareClientID,
			Name:     "Home Burger",
			Verified: false,
			Lat:      40.42515576695866,
			Lng:      -3.70286300869635,
			Address: service.Address{
				StreetAddress: "C. Espíritu Santo, 12",
				Postcode:      "28004",
				Locality:      "Madrid",
				Region:        "Madrid",
				Country:       "ES",
			},
			Phone: "+34915229728",
			Categories: []string{
				"Burger Joint",
				"American Restaurant",
			},
			Website:      "",
			Rating:       service.NewFloat32(7.59),
			MaxRating:    service.NewInt(10),
			TotalReviews: service.NewInt(39),
			Summary:      "",
		},
	},
}

func TestProfile(t *testing.T) {
	for _, test := range testProfiles {
		name := test.name
		location := test.location
		expectedProfile := test.expected
		found, _, err := svc.SearchLocation(name, location)
		if err != nil {
			t.Fatal(err)
		}
		if len(found) == 0 {
			t.Errorf("No results for %s in %s", name, location)
			continue
		}
		actualProfile, err := svc.Profile(found[0].ID)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(actualProfile, expectedProfile) {
			t.Errorf("Fail %s\nExpected: %s\nReceived: %s", name, asJSON(expectedProfile), asJSON(actualProfile))
		}
	}
}

func asJSON(x interface{}) []byte {
	result, err := json.Marshal(x)
	if err != nil {
		panic(err)
	}
	return result
}
