package google

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"

	"google.golang.org/api/googleapi"
	"google.golang.org/api/googleapi/transport"
	"google.golang.org/api/plus/v1"

	"github.com/localistico/services-api/service"
	"github.com/kellydunn/golang-geo"
)

const (
	baseURL          = "https://maps.googleapis.com/maps/api"
	textSearchPath   = "/place/textsearch/json"
	nearbySearchPath = "/place/nearbysearch/json"
	placeDetailsPath = "/place/details/json"
)

// Service implements service.ProfileService
type Service struct {
	APIKey         string
	client         *http.Client
	ReferenceStore ReferenceCacheStore
	debugHTTP      bool
	plusTransport  *transport.APIKey
	scraperClient  *http.Client
}

// NewService creates a new Service object
func NewService(APIKey string, scraperTransport http.RoundTripper) (s *Service, err error) {
	if APIKey == "" {
		return nil, errors.New("need to provide an API key")
	}
	svc := &Service{
		APIKey: APIKey,
		client: &http.Client{
			Transport: &http.Transport{
				DisableKeepAlives: true,
			},
		},
		ReferenceStore: defaultReferenceCache,
		debugHTTP:      false,
		plusTransport: &transport.APIKey{
			Key:       APIKey,
			Transport: &http.Transport{},
		},
		scraperClient: &http.Client{
			Transport: scraperTransport,
		},
	}
	return svc, nil
}

// SetTransport sets the http transport used for the API requests
func (s *Service) SetTransport(t http.RoundTripper) {
	s.client.Transport = t
	s.plusTransport.Transport = t
}

// GetTransport returns the http transport used for the API requests
func (s *Service) GetTransport() http.RoundTripper {
	t := s.client.Transport
	if t == nil {
		return http.DefaultTransport
	}
	return t
}

func (s *Service) defaultParams() url.Values {
	var v = url.Values{}
	v.Set("sensor", "false")
	v.Set("key", s.APIKey)
	v.Set("language", "en")
	return v
}

// Service returns the name of the service
func (s *Service) Service() string {
	return "google"
}

// SearchLocation geocode the loc location and
// search for profiles around that location
func (s *Service) SearchLocation(name string, loc string) (found []service.ProfileFound, err error) {
	query := s.defaultParams()
	query.Set("query", fmt.Sprintf("%s, %s", name, loc))
	url := baseURL + textSearchPath + "?" + query.Encode()

	maxPages := 3
	filter := service.SimilarNameFilter(name)

	for page := 1; page <= maxPages; page++ {
		var f []service.ProfileFound
		var response = &apiSearchResponse{}
		err = s.requestSearch(url, response)
		if err != nil {
			return
		}
		f, err = s.filterResults(filter, response)
		if err != nil {
			return
		}
		found = append(found, f...)
		continueNextPage := true // len(d) == 0
		if !continueNextPage || response.NextPageToken == "" {
			break
		}
		query.Set("pagetoken", response.NextPageToken)
		url = baseURL + textSearchPath + "?" + query.Encode()
		time.Sleep(1900 * time.Millisecond)
	}
	return
}

func (s *Service) saveReference(id, reference string) {
	ref := ReferencePair{ID: id, Reference: reference}
	err := s.ReferenceStore.UpdateID(id, ref)
	if err != nil {
		s.ReferenceStore.Insert(ref)
	}
}

func (s *Service) getReference(id string) string {
	var ref ReferencePair
	err := s.ReferenceStore.FindID(id, &ref)
	if err != nil {
		return ""
	}
	return ref.Reference
}

// SearchBBox search for business profiles inside
// bounding box specified by sw (south wet) and
// ne (north east) coordinates
func (s *Service) SearchBBox(name string, sw *geo.Point, ne *geo.Point) (found []service.ProfileFound, err error) {
	panic("SearchBBox not implemented")
}

type apiSearchResult struct {
	ID        string `json:"id"`
	Reference string `json:"reference"`
	Name      string `json:"name"`
}

type apiSearchResponse struct {
	Status        string            `json:"status"`
	Results       []apiSearchResult `json:"results"`
	Error         string            `json:"error_message"`
	NextPageToken string            `json:"next_page_token"`
}

type apiProfileResponse struct {
	Status string           `json:"status"`
	Result apiProfileResult `json:"result"`
	Error  string           `json:"error_message"`
}

func (s *Service) request(url string, response interface{}) error {
	res, err := s.client.Get(url)
	if err != nil {
		return err
	}
	if s.debugHTTP {
		out, err := httputil.DumpResponse(res, true)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s\n", out)
	}

	defer res.Body.Close()
	return json.NewDecoder(res.Body).Decode(&response)
}

func (s *Service) requestProfile(url string, response *apiProfileResponse) error {
	err := s.request(url, response)
	if err != nil {
		return err
	}

	switch response.Status {
	case "OK":
		break
	default:
		return fmt.Errorf("%s: %s", response.Status, response.Error)
	}
	return nil
}

func (s *Service) requestSearch(url string, response *apiSearchResponse) error {
	err := s.request(url, response)
	if err != nil {
		return err
	}

	switch response.Status {
	case "ZERO_RESULTS":
	case "OK":
		break
	default:
		return fmt.Errorf("%s: %s", response.Status, response.Error)
	}
	return nil
}

// SearchAround searches for venues around the
// specified coordinates. radius for search is
// specified in meters
func (s *Service) SearchAround(name string, lat, lng float64, radius int) (found []service.ProfileFound, err error) {
	query := s.defaultParams()
	query.Set("name", name)
	query.Set("location", fmt.Sprintf("%f,%f", lat, lng))
	query.Set("radius", fmt.Sprintf("%d", radius))
	url := baseURL + nearbySearchPath + "?" + query.Encode()

	var response = &apiSearchResponse{}
	err = s.requestSearch(url, response)
	if err != nil {
		return found, err
	}

	filter := service.SimilarNameFilter(name)
	return s.filterResults(filter, response)
}

func (s *Service) filterResults(filter service.FilterFunc, response *apiSearchResponse) ([]service.ProfileFound, error) {
	var (
		found []service.ProfileFound
		err   error
	)

	for _, result := range response.Results {
		s.saveReference(result.ID, result.Reference)
		profile := service.ProfileFound{
			Service: "google",
			ID:      result.ID,
			Name:    result.Name,
		}
		var matched bool
		if matched, err = filter(profile); !matched {
			profile.Discarded = true
		}
		found = append(found, profile)
		if err != nil {
			return found, err
		}
	}

	return found, nil
}

// Profile extracts information for the profile with the specified id
func (s *Service) Profile(id string) (profile service.Profile, err error) {
	reference := s.getReference(id)
	if reference == "" {
		return profile, errors.New("cannot find a reference for the id")
	}
	query := s.defaultParams()
	query.Set("reference", reference)

	url := baseURL + placeDetailsPath + "?" + query.Encode()
	var response apiProfileResponse
	err = s.requestProfile(url, &response)
	if err != nil {
		return profile, err
	}

	return s.adapt(&response.Result)
}

// CheckProfileStatus checks whether the passed profile has been merged or deleted and fills the appropriate field
func (s *Service) CheckProfileStatus(p *service.ProfileStatus) error {
	api, err := plus.New(&http.Client{Transport: s.plusTransport})
	if err != nil {
		return err
	}

	res, err := api.People.Get(p.ID).Do()
	if err, ok := err.(*googleapi.Error); ok && err.Code == 404 {
		p.Deleted = true
		return nil
	}

	if err != nil {
		return err
	}

	if p.ID != res.Id {
		p.BestID = res.Id
	}

	response, err := s.scrapeProfile(res.Url)
	if err != nil {
		return err
	}

	p.Closed = response.Closed()

	return nil
}
