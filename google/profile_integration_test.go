// +build integration

package google

import (
	"encoding/json"
	"flag"
	"os"
	"reflect"
	"testing"

	"github.com/localistico/services-api/service"
	"github.com/kellydunn/golang-geo"
)

var (
	googleAPIKey = flag.String("google-api-key", os.Getenv("GOOGLE_API_KEY"), "API Key for Google Places API")
	svc          service.ProfileService
)

func init() {
	var err error
	svc, err = NewService(*googleAPIKey)
	if err != nil {
		panic(err)
	}
}

func TestSearchAround(t *testing.T) {
	name := "Pimlico Fresh"
	center := geo.NewPoint(51.691544, 0.33409)
	radius := 50000
	found, rejected, err := service.SplitSearch(svc.SearchAround(name, center.Lat(), center.Lng(), radius))
	expectedFound := []service.ProfileFound{
		service.ProfileFound{
			Service: "google",
			Name:    "Pimlico Fresh",
			ID:      "e753e187b1cca0ca056aa2c5c3909965de018330",
		},
	}
	expectedTotalFound := 1
	expectedTotalRejected := 19
	if err != nil {
		t.Error(err)
	}
	if actualTotalFound := len(found); actualTotalFound != expectedTotalFound {
		t.Errorf("Expected %d profiles searching for %s.\nGot: %d\n%#v", expectedTotalFound, name, actualTotalFound, found)
	} else if !reflect.DeepEqual(found, expectedFound) {
		t.Errorf("Fail %s\nExpected: %s\nReceived: %s", name, asJSON(expectedFound), asJSON(found))
	}
	if actualTotalRejected := len(rejected); actualTotalRejected != expectedTotalRejected {
		t.Errorf("Expected %d rejected profiles searching for %s.\nGot: %d\n%#v", expectedTotalRejected, name, actualTotalRejected, rejected)
	}
}

func TestSearchLocation(t *testing.T) {
	name := "Pimlico Fresh"
	location := "London"
	found, rejected, err := service.SplitSearch(svc.SearchLocation(name, location))
	expectedFound := []service.ProfileFound{
		service.ProfileFound{
			Service: "google",
			Name:    "Pimlico Fresh",
			ID:      "e753e187b1cca0ca056aa2c5c3909965de018330",
		},
	}
	expectedTotalFound := 1
	expectedTotalRejected := 19
	if err != nil {
		t.Error(err)
	}
	if actualTotalFound := len(found); actualTotalFound != expectedTotalFound {
		t.Errorf("Expected %d profiles searching for %s.\nGot: %d\n%#v", expectedTotalFound, name, actualTotalFound, found)
	} else if !reflect.DeepEqual(found, expectedFound) {
		t.Errorf("Fail %s\nExpected: %#v\nReceived: %#v", name, expectedFound, found)
	}
	if actualTotalRejected := len(rejected); actualTotalRejected != expectedTotalRejected {
		t.Errorf("Expected %d rejected profiles searching for %s.\nGot: %d\n%#v", expectedTotalRejected, name, actualTotalRejected, rejected)
	}
}

type testProfile struct {
	name     string
	location string
	expected service.Profile
}

var testProfiles = []testProfile{

	testProfile{
		name:     "Pimlico Fresh",
		location: "London",
		expected: service.Profile{
			Service:  "google",
			ID:       "102840583891114124064",
			URL:      "https://plus.google.com/102840583891114124064/about?hl=en",
			Name:     "Pimlico Fresh",
			Verified: false,
			Lat:      51.49238,
			Lng:      -0.140324,
			Address: service.Address{
				StreetAddress: "86-87 Wilton Rd",
				Postcode:      "SW1V 1DN",
				Locality:      "London",
				Region:        "",
				Country:       "GB",
			},
			Phone: "+44 20 7932 0030",
			Categories: []string{
				"Cafe",
				"Takeaway",
			},
			Website:      "",
			Rating:       service.NewFloat32(4.1),
			MaxRating:    service.NewInt(5),
			TotalReviews: service.NewInt(48),
			Summary:      "",
			Hours: service.ProfileHours{
				Mon: []string{"07:30 - 19:30"},
				Tue: []string{"07:30 - 19:30"},
				Wed: []string{"07:30 - 19:30"},
				Thu: []string{"07:30 - 19:30"},
				Fri: []string{"07:30 - 19:30"},
				Sat: []string{"09:00 - 18:00"},
				Sun: []string{"09:00 - 18:00"},
			},
		},
	},

	testProfile{
		name:     "Gastronomica",
		location: "London",
		expected: service.Profile{
			Service:  "google",
			ID:       "104344155196090670499",
			URL:      "https://plus.google.com/104344155196090670499/about?hl=en",
			Name:     "Gastronomica Pimlico",
			Verified: true,
			Lat:      51.491971,
			Lng:      -0.138161,
			Address: service.Address{
				StreetAddress: "45 Tachbrook St",
				Postcode:      "SW1V 2LZ",
				Locality:      "London",
				Region:        "",
				Country:       "GB",
			},
			Phone:        "+44 20 7233 6656",
			Categories:   []string{"Deli"},
			Website:      "http://www.gastronomica.co.uk/",
			Rating:       service.NewFloat32(3.8),
			MaxRating:    service.NewInt(5),
			TotalReviews: service.NewInt(28),
			Summary:      "A classic Italian delicatessen specialising in cheeses, charcuteries and wines. We serve to tables; also offering pastries, espressos and homemade cakes. Warm and friendly, with quality and passion.",
			Hours: service.ProfileHours{
				Mon: []string{"20:00 - 23:00"},
				Tue: []string{"20:00 - 23:00"},
				Wed: []string{"20:00 - 23:00"},
				Thu: []string{"20:00 - 23:00"},
				Fri: []string{"20:00 - 23:00"},
				Sat: []string{"20:00 - 23:00"},
				Sun: []string{"09:00 - 22:00"},
			},
		},
	},

	testProfile{
		name:     "Home Burger",
		location: "Madrid",
		expected: service.Profile{
			Service:  "google",
			ID:       "115205178192755262507",
			URL:      "https://plus.google.com/115205178192755262507/about?hl=en",
			Name:     "Home Burger Bar - San Marcos",
			Verified: false,
			Lat:      40.421303,
			Lng:      -3.698285,
			Address: service.Address{
				StreetAddress: "Calle San Marcos, 26",
				Postcode:      "28004",
				Locality:      "Madrid",
				Region:        "",
				Country:       "ES",
			},
			Phone: "+34 915 21 85 31",
			Categories: []string{
				"Hamburger Restaurant",
				"American Restaurant",
			},
			Website:      "http://www.homeburgerbar.com/",
			Rating:       service.NewFloat32(4.1),
			MaxRating:    service.NewInt(5),
			TotalReviews: service.NewInt(113),
			Summary:      "",
			Hours: service.ProfileHours{
				Mon: []string{
					"13:30 - 16:00",
					"20:30 - 00:00",
				},
				Tue: []string{
					"13:30 - 16:00",
					"20:30 - 00:00",
				},
				Wed: []string{
					"13:30 - 16:00",
					"20:30 - 00:00",
				},
				Thu: []string{
					"13:30 - 16:00",
					"20:30 - 00:00",
				},
				Fri: []string{
					"14:00 - 17:00",
					"20:30 - 12:00",
				},
				Sat: []string{
					"14:00 - 17:00",
					"20:30 - 12:00",
				},
				Sun: []string{
					"14:00 - 17:00",
					"20:30 - 23:00",
				},
			},
		},
	},
}

func TestProfile(t *testing.T) {
	for _, test := range testProfiles {
		name := test.name
		location := test.location
		expectedProfile := test.expected
		found, _, err := svc.SearchLocation(name, location)
		if err != nil {
			t.Fatal(err)
		}
		if len(found) == 0 {
			t.Errorf("No results for %s in %s", name, location)
			continue
		}
		actualProfile, err := svc.Profile(found[0].ID)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(actualProfile, expectedProfile) {
			t.Errorf("Fail %s\nExpected: %s\nReceived: %s", name, asJSON(expectedProfile), asJSON(actualProfile))
		}
	}
}

func asJSON(x interface{}) []byte {
	result, err := json.Marshal(x)
	if err != nil {
		panic(err)
	}
	return result
}
