package google

import (
	"reflect"
	"testing"

	"github.com/localistico/services-api/service"
)

type testHours struct {
	name          string
	srcHours      []resultTimePeriod
	expectedHours service.ProfileHours
}

var testsHours = []testHours{

	testHours{
		name: "always open",
		srcHours: []resultTimePeriod{
			resultTimePeriod{
				Open: resultTimeAndDay{
					Day:  0,
					Time: "0000",
				},
			},
		},
		expectedHours: service.ProfileHours{
			Mon: []string{"00:00 - 00:00"},
			Tue: []string{"00:00 - 00:00"},
			Wed: []string{"00:00 - 00:00"},
			Thu: []string{"00:00 - 00:00"},
			Fri: []string{"00:00 - 00:00"},
			Sat: []string{"00:00 - 00:00"},
			Sun: []string{"00:00 - 00:00"},
		},
	},

	testHours{
		name: "multiple days period",
		srcHours: []resultTimePeriod{
			resultTimePeriod{
				Open: resultTimeAndDay{
					Day:  2,
					Time: "2300",
				},
				Close: &resultTimeAndDay{
					Day:  5,
					Time: "2315",
				},
			},
		},
		expectedHours: service.ProfileHours{
			Tue: []string{"23:00 - 00:00"},
			Wed: []string{"00:00 - 00:00"},
			Thu: []string{"00:00 - 00:00"},
			Fri: []string{"00:00 - 23:15"},
		},
	},

	testHours{
		name: "full week with couple of 24h",
		srcHours: []resultTimePeriod{
			resultTimePeriod{
				Open: resultTimeAndDay{
					Day:  0,
					Time: "1100",
				},
				Close: &resultTimeAndDay{
					Day:  0,
					Time: "2230",
				},
			},
			resultTimePeriod{
				Open: resultTimeAndDay{
					Day:  1,
					Time: "2300",
				},
				Close: &resultTimeAndDay{
					Day:  2,
					Time: "1100",
				},
			},
			resultTimePeriod{
				Open: resultTimeAndDay{
					Day:  2,
					Time: "2300",
				},
				Close: &resultTimeAndDay{
					Day:  5,
					Time: "0000",
				},
			},
			resultTimePeriod{
				Open: resultTimeAndDay{
					Day:  5,
					Time: "2300",
				},
				Close: &resultTimeAndDay{
					Day:  6,
					Time: "0000",
				},
			},
			resultTimePeriod{
				Open: resultTimeAndDay{
					Day:  6,
					Time: "2300",
				},
				Close: &resultTimeAndDay{
					Day:  0,
					Time: "0000",
				},
			},
		},
		expectedHours: service.ProfileHours{
			Mon: []string{"23:00 - 11:00"},
			Tue: []string{"23:00 - 00:00"},
			Wed: []string{"00:00 - 00:00"},
			Thu: []string{"00:00 - 00:00"},
			Fri: []string{"23:00 - 00:00"},
			Sat: []string{"23:00 - 00:00"},
			Sun: []string{"11:00 - 22:30"},
		},
	},
}

func TestParseHours(t *testing.T) {
	for _, test := range testsHours {
		result := &apiProfileResult{
			Hours: resultHours{
				Periods: test.srcHours,
			},
		}
		expectedHours := test.expectedHours
		actualHours, err := result.ParseHours()
		if err != nil {
			t.Fatalf("%s: %s", test.name, err)
		}
		if !reflect.DeepEqual(actualHours, expectedHours) {
			t.Errorf("%s: unexpected result\nExpected: %v\nReceived: %v", test.name, test.expectedHours, actualHours)
		}
	}
}

func TestParseAddress(t *testing.T) {
	expected := service.Address{
		StreetAddress: "86-87 Wilton Rd",
		Postcode:      "SW1V 1DN",
		Locality:      "London",
		Region:        "",
		Country:       "GB",
	}

	actual, err := parseAddress(&apiProfileResult{
		AdrAddress: "<span class=\"street-address\">86-87 Wilton Rd</span>, <span class=\"extended-address\">Victoria</span>, <span class=\"locality\">London</span> <span class=\"postal-code\">SW1V 1DN</span>, <span class=\"country-name\">United Kingdom</span>",
		Address: []resultAdrComponent{
			resultAdrComponent{
				ShortName: "GB",
				Types:     []string{"country", "political"},
			},
		},
	})
	if err != nil {
		t.Errorf("Got error: %s", err.Error())
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Wrong.\nExpected: %#v\nGot: %#v", expected, actual)
	}
}

func TestParseAddressWithExtra(t *testing.T) {
	expected := service.Address{
		StreetAddress: "86-87 Wilton Rd",
		Postcode:      "SW1V 1DN",
		Locality:      "London",
		Region:        "",
		Country:       "GB",
	}

	actual, err := parseAddress(&apiProfileResult{
		AdrAddress: "86-87<span class=\"street-address\">Wilton Rd</span>, <span class=\"extended-address\">Victoria</span>, <span class=\"locality\">London</span> <span class=\"postal-code\">SW1V 1DN</span>, <span class=\"country-name\">United Kingdom</span>",
		Address: []resultAdrComponent{
			resultAdrComponent{
				ShortName: "GB",
				Types:     []string{"country", "political"},
			},
		},
	})
	if err != nil {
		t.Errorf("Got error: %s", err.Error())
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Wrong.\nExpected: %#v\nGot: %#v", expected, actual)
	}
}

func TestParseAddressWithExtraAndNoStreet(t *testing.T) {
	expected := service.Address{
		StreetAddress: "86-87 Wilton Rd",
		Postcode:      "SW1V 1DN",
		Locality:      "London",
		Region:        "",
		Country:       "GB",
	}

	actual, err := parseAddress(&apiProfileResult{
		AdrAddress: "86-87 Wilton Rd, <span class=\"extended-address\">Victoria</span>, <span class=\"locality\">London</span> <span class=\"postal-code\">SW1V 1DN</span>, <span class=\"country-name\">United Kingdom</span>",
		Address: []resultAdrComponent{
			resultAdrComponent{
				ShortName: "GB",
				Types:     []string{"country", "political"},
			},
		},
	})
	if err != nil {
		t.Errorf("Got error: %s", err.Error())
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Wrong.\nExpected: %#v\nGot: %#v", expected, actual)
	}
}

func TestParseAddressWithEntities(t *testing.T) {
	expected := service.Address{
		StreetAddress: "Bishop's & Ave's",
		Postcode:      "SW1V 1DN",
		Locality:      "London",
		Region:        "",
		Country:       "GB",
	}

	actual, err := parseAddress(&apiProfileResult{
		AdrAddress: "Bishop&#39;s &amp; <span class=\"street-address\">Ave&#39;s</span>, <span class=\"extended-address\">Victoria</span>, <span class=\"locality\">London</span> <span class=\"postal-code\">SW1V 1DN</span>, <span class=\"country-name\">United Kingdom</span>",
		Address: []resultAdrComponent{
			resultAdrComponent{
				ShortName: "GB",
				Types:     []string{"country", "political"},
			},
		},
	})
	if err != nil {
		t.Errorf("Got error: %s", err.Error())
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Wrong.\nExpected: %#v\nGot: %#v", expected, actual)
	}
}
