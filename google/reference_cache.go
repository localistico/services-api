package google

import (
	"errors"
	"github.com/fzzy/radix/extra/pool"
	"time"
)

// ReferenceCacheStore defines the interface to be able to store references
type ReferenceCacheStore interface {
	FindID(string, *ReferencePair) error
	UpdateID(string, ReferencePair) error
	Insert(ReferencePair) error
}

// ReferencePair specifies what's the reference for the found ID
type ReferencePair struct {
	ID        string
	Reference string
}

// RedisReferenceCache caches google references in redis... So misterious... :)
type RedisReferenceCache struct {
	pool *pool.Pool
	ttl  time.Duration
}

// NewRedisReferenceCache initializes and returns a new RedisReferenceCache
func NewRedisReferenceCache(p *pool.Pool, t time.Duration) *RedisReferenceCache {
	return &RedisReferenceCache{
		pool: p,
		ttl:  t,
	}
}

func (c *RedisReferenceCache) key(id string) string {
	return "google_references:" + id
}

// FindID fetches the reference for the given id from the cache
func (c *RedisReferenceCache) FindID(id string, ref *ReferencePair) error {
	if id == "" {
		return errors.New("missing id")
	}
	rc, err := c.pool.Get()
	if err != nil {
		return err
	}
	defer c.pool.Put(rc)
	ref.Reference, err = rc.Cmd("get", c.key(id)).Str()
	if err != nil {
		return err
	}
	ref.ID = id
	return nil
}

// UpdateID updates the reference in the cache
func (c *RedisReferenceCache) UpdateID(id string, ref ReferencePair) error {
	if id == "" {
		return errors.New("missing id")
	}
	rc, err := c.pool.Get()
	if err != nil {
		return err
	}
	defer c.pool.Put(rc)
	k := c.key(id)
	r := rc.Cmd("set", k, ref.Reference)
	if c.ttl > 0 {
		rc.Cmd("expire", k, c.ttl)
	}
	if r.Err != nil {
		return r.Err
	}
	return nil
}

// Insert saves a new reference in the cache
func (c *RedisReferenceCache) Insert(ref ReferencePair) error {
	return c.UpdateID(ref.ID, ref)
}

type memoryReferenceCache struct {
	references map[string]string
}

var defaultReferenceCache = &memoryReferenceCache{references: make(map[string]string)}

func (m *memoryReferenceCache) FindID(id string, ref *ReferencePair) error {
	ref.Reference = m.references[id]
	ref.ID = id
	return nil
}

func (m *memoryReferenceCache) UpdateID(id string, ref ReferencePair) error {
	m.references[ref.ID] = ref.Reference
	return nil
}

func (m *memoryReferenceCache) Insert(ref ReferencePair) error {
	m.references[ref.ID] = ref.Reference
	return nil
}
