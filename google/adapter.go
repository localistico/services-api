package google

import (
	"bytes"
	"fmt"
	"html"
	"log"
	"net/http"
	"net/textproto"
	"net/url"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/ernesto-jimenez/scraperboard"
	"github.com/localistico/services-api/service"
	"google.golang.org/api/plus/v1"
)

type apiProfileResult struct {
	ID           string               `json:"id"`
	URL          string               `json:"url"`
	Name         string               `json:"name"`
	Phone        string               `json:"international_phone_number"`
	Rating       float32              `json:"rating"`
	TotalReviews int                  `json:"user_ratings_total"`
	Geometry     resultGeometry       `json:"geometry"`
	Address      []resultAdrComponent `json:"address_components"`
	AdrAddress   string               `json:"adr_address"`
	Hours        resultHours          `json:"opening_hours"`
	Closed       bool                 `json:"permanently_closed"`
}

type resultHours struct {
	Periods []resultTimePeriod `json:"periods"`
}

func (r *resultHours) AlwaysOpen() bool {
	if len(r.Periods) != 1 {
		return false
	}
	period := r.Periods[0]
	if period.Close != nil {
		return false
	}
	if period.Open.Day != 0 || period.Open.Time != "0000" {
		return false
	}
	return true
}

type resultTimePeriod struct {
	Open  resultTimeAndDay  `json:"open"`
	Close *resultTimeAndDay `json:"close"`
}

type resultTimeAndDay struct {
	Day  int    `json:"day"`
	Time string `json:"time"`
}

func (td *resultTimeAndDay) normalizedTime() (string, error) {
	timeFormat := "1504"
	t, err := time.Parse(timeFormat, td.Time)
	if err != nil {
		return "", err
	}
	return t.Format("15:04"), nil
}

type resultAdrComponent struct {
	ShortName string   `json:"short_name"`
	Types     []string `json:"types"`
}

type resultGeometry struct {
	Location resultLocation `json:"location"`
}

type resultLocation struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

func (r *apiProfileResult) id() string {
	re := regexp.MustCompile("[0-9]+")
	return re.FindString(r.URL)
}

var addressRe = regexp.MustCompile("<[^>]+ class=\"([^\"]+)\">([^<]+)<")
var preAddr = regexp.MustCompile("^[^<]*")

func parseAddress(result *apiProfileResult) (service.Address, error) {
	var addr service.Address
	addr.StreetAddress = string(bytes.Trim([]byte(preAddr.FindString(result.AdrAddress)), " ,"))
	matches := addressRe.FindAllStringSubmatch(result.AdrAddress, -1)
	for _, component := range matches {
		switch component[1] {
		case "street-address":
			addr.StreetAddress = textproto.TrimString(addr.StreetAddress + " " + component[2])
		case "locality":
			addr.Locality = html.UnescapeString(textproto.TrimString(component[2]))
		case "postal-code":
			addr.Postcode = html.UnescapeString(textproto.TrimString(component[2]))
		}
	}
	addr.StreetAddress = html.UnescapeString(addr.StreetAddress)
	for _, component := range result.Address {
		if len(component.Types) == 0 {
			continue
		}
		switch component.Types[0] {
		case "country":
			addr.Country = component.ShortName
		}
	}
	return addr, nil
}

func (s *Service) plusAPI(profile *service.Profile) error {
	client := &http.Client{Transport: s.plusTransport}
	plusService, err := plus.New(client)
	if err != nil {
		return err
	}

	place, err := plusService.People.Get(profile.ID).Do()
	if err != nil {
		return err
	}

	profile.Summary, _ = scraperboard.MarkdownifyReader(strings.NewReader(place.AboutMe))

	switch len(place.Emails) {
	case 0:
	default:
		for i, email := range place.Emails {
			log.Printf("profile=%s emails=%d email=%d email.Type=%s email.Value=%s", profile.ID, len(place.Emails), i, email.Type, email.Value)
		}
	}

	switch len(place.Urls) {
	case 0:
	case 1:
		if len(place.Urls) > 0 {
			profile.Website = place.Urls[0].Value
		}
	default:
		for i, url := range place.Urls {
			log.Printf("profile=%s urls=%d url=%d url.Type=%s url.Value=%s url.Label=%s", profile.ID, len(place.Urls), i, url.Type, url.Value, url.Label)
		}
	}

	profile.Verified = place.Verified

	return nil
}

func (s *Service) scrape(profile *service.Profile) error {
	response, err := s.scrapeProfile(profile.URL)

	if err != nil {
		return err
	}

	profile.Summary = response.Description
	profile.Categories = response.Categories
	if !profile.Closed {
		profile.Closed = response.Closed()
	}

	return nil
}

func (s *Service) scrapeProfile(url string) (response scraperResponse, err error) {
	scraper, err := scraperboard.NewScraperFromString(scraperXML)
	if err != nil {
		return response, err
	}

	res, err := s.scraperClient.Get(url)
	if err != nil {
		return response, err
	}
	defer res.Body.Close()
	err = scraper.ExtractFromResponse(res, &response)
	if err != nil {
		return response, err
	}

	return response, nil
}

type scraperResponse struct {
	Categories  []string `json:"categories"`
	Description string   `json:"description"`
	Contact     string   `json:"contact"`
}

func (r *scraperResponse) Closed() bool {
	return strings.Contains(r.Contact, "This place has closed")
}

func newAlwaysOpenHours() service.ProfileHours {
	return service.ProfileHours{
		Mon: []string{"00:00 - 00:00"},
		Tue: []string{"00:00 - 00:00"},
		Wed: []string{"00:00 - 00:00"},
		Thu: []string{"00:00 - 00:00"},
		Fri: []string{"00:00 - 00:00"},
		Sat: []string{"00:00 - 00:00"},
		Sun: []string{"00:00 - 00:00"},
	}
}

func (r *apiProfileResult) ParseHours() (service.ProfileHours, error) {
	var hours service.ProfileHours
	if r.Hours.AlwaysOpen() {
		return newAlwaysOpenHours(), nil
	}
	for _, period := range r.Hours.Periods {
		if period.Close == nil {
			return hours, fmt.Errorf("no close period %#v", r.Hours)
		}
		openTime, err := period.Open.normalizedTime()
		if err != nil {
			return hours, err
		}
		closeTime, err := period.Close.normalizedTime()
		if err != nil {
			return hours, err
		}
		openDay := period.Open.Day
		closeDay := period.Close.Day
		if closeDay < openDay {
			closeDay += 7
		}
		days := closeDay - openDay
		switch days {
		case 0:
		case 1:
			closeDay = openDay
		default:
			if closeTime == "00:00" {
				closeDay--
			}
		}
		end := "00:00"
		for i := openDay; i <= closeDay; i++ {
			if i == closeDay {
				end = closeTime
			}
			times := fmt.Sprintf("%s - %s", openTime, end)
			day := i % 7
			switch day {
			case 0:
				hours.Sun = append(hours.Sun, times)
				sort.Strings(hours.Sun)
			case 1:
				hours.Mon = append(hours.Mon, times)
				sort.Strings(hours.Mon)
			case 2:
				hours.Tue = append(hours.Tue, times)
				sort.Strings(hours.Tue)
			case 3:
				hours.Wed = append(hours.Wed, times)
				sort.Strings(hours.Wed)
			case 4:
				hours.Thu = append(hours.Thu, times)
				sort.Strings(hours.Thu)
			case 5:
				hours.Fri = append(hours.Fri, times)
				sort.Strings(hours.Fri)
			case 6:
				hours.Sat = append(hours.Sat, times)
				sort.Strings(hours.Sat)
			default:
				return hours, fmt.Errorf("unknown day %d - %#v", period.Open.Day, r.Hours)
			}
			openTime = "00:00"
		}
	}

	return hours, nil
}

func (s *Service) adapt(response *apiProfileResult) (service.Profile, error) {
	var err error
	profile := service.Profile{}
	profile.Service = "google"
	profile.ID = response.id()
	profile.Name = response.Name
	profile.Closed = response.Closed
	uri, err := url.Parse(response.URL)
	if err != nil {
		return profile, err
	}
	profile.URL = response.URL
	if uri.Host != "plus.google.com" {
		return profile, &service.NotBusinessProfileError{
			ID:   profile.ID,
			Name: profile.Name,
			URL:  response.URL,
		}
	}
	profile.SetPhone(response.Phone)
	profile.Lat = response.Geometry.Location.Lat
	profile.Lng = response.Geometry.Location.Lng
	profile.Rating = &response.Rating
	maxRating := 5
	profile.MaxRating = &maxRating
	profile.TotalReviews = &response.TotalReviews
	if profile.Hours, err = response.ParseHours(); err != nil {
		return profile, err
	}
	address, err := parseAddress(response)
	profile.Address = address
	if err != nil {
		return profile, err
	}

	err = s.scrape(&profile)
	err = s.plusAPI(&profile)

	return profile, err
}

var scraperXML = `
<?xml version="1.0" encoding="UTF-8" ?>
<Scraper>
  <ArrayProperty name="categories" selector="[id='1'][role='article'] [role='button'][data-displaytext]">
    <Filter type="text"/>
  </ArrayProperty>
  <Property name="description" selector="[itemprop=description]">
    <Filter type="attr" argument="content"/>
  </Property>
  <Property name="contact" selector="[id='1'][role='article']">
    <Filter type="text"/>
  </Property>
</Scraper>
`
