package yelp

import (
	"reflect"
	"testing"
)

type test struct {
	name     string
	src      []string
	expected string
}

var tests = []test{
	test{
		name:     "missing pm",
		src:      []string{"8:00", "11:00 pm"},
		expected: "8:00 - 11:00 pm",
	},
	test{
		name:     "missing pm with dots",
		src:      []string{"8:00", "11:00 p.m."},
		expected: "8:00 - 11:00 p.m.",
	},
	test{
		name:     "existing am",
		src:      []string{"8:00 am", "11:00 p.m."},
		expected: "8:00 am - 11:00 p.m.",
	},
	test{
		name:     "existing am with dots",
		src:      []string{"8:00 a.m.", "11:00 p.m."},
		expected: "8:00 a.m. - 11:00 p.m.",
	},
	test{
		name:     "existing 24h",
		src:      []string{"8:00", "23:00"},
		expected: "8:00 - 23:00",
	},
}

func TestNormalizedTimes(t *testing.T) {
	for _, test := range tests {
		time := scraperTime{
			Day:   "Thursday",
			Times: test.src,
		}
		actual := time.NormalizedTimes()[0]
		expected := test.expected
		if actual != expected {
			t.Errorf("%s\nExpected: %#v\nReceived: %#v", test.name, expected, actual)
		}
	}
}

var testParseTimes = []testParseTime{
	testParseTime{
		name: "empty array",
		src:  scraperTime{Times: []string{}},
	},
	testParseTime{
		name: "am/pm names",
		src: scraperTime{
			Day: "Mon",
			Times: []string{
				"8:11",
				"11:00 pm",
				"8:20pm",
				"11:00 pm",
				"9:30p.m. ",
				"11:00 pm",
				"09:40a.m. ",
				"11:00 pm",
				"9:50 ",
				"21:00",
			},
		},
		expected: []string{
			"08:11 - 23:00",
			"20:20 - 23:00",
			"21:30 - 23:00",
			"09:40 - 23:00",
			"09:50 - 21:00",
		},
	},
}

type testParseTime struct {
	name     string
	src      scraperTime
	expected []string
}

func TestParseTimes(t *testing.T) {
	for _, test := range testParseTimes {
		name := test.name
		src := test.src
		expected := test.expected
		actual, err := parseTimes(src.NormalizedTimes())
		if err != nil {
			t.Fatalf("%s error: %s", name, err)
		}

		if !reflect.DeepEqual(expected, actual) {
			t.Errorf("%s wrong.\nExpected: %#v\nReceived: %#v", name, expected, actual)
		}
	}
}
