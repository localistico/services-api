// +build integration

package yelp

import (
	"encoding/json"
	"flag"
	"os"
	"reflect"
	"testing"

	"github.com/localistico/services-api/service"
	"github.com/kellydunn/golang-geo"
)

var (
	yelpConsumerKey    = flag.String("yelp-consumer-key", os.Getenv("YELP_CONSUMER_KEY"), "Yelp's Consumer Key")
	yelpConsumerSecret = flag.String("yelp-consumer-secret", os.Getenv("YELP_CONSUMER_SECRET"), "Yelp's Consumer Secret")
	yelpTokenKey       = flag.String("yelp-token-key", os.Getenv("YELP_TOKEN_KEY"), "Yelp's Token Key")
	yelpTokenSecret    = flag.String("yelp-token-secret", os.Getenv("YELP_TOKEN_SECRET"), "Yelp's Token Secret")
	svc                service.ProfileService
)

func init() {
	var err error
	svc, err = NewService(*yelpConsumerKey, *yelpConsumerSecret, *yelpTokenKey, *yelpTokenSecret)
	if err != nil {
		panic(err)
	}
}

func TestSearchAround(t *testing.T) {
	name := "Pimlico Fresh"
	center := geo.NewPoint(51.691544, -0.33409)
	radius := 50000
	found, rejected, err := service.SplitSearch(svc.SearchAround(name, center.Lat(), center.Lng(), radius))
	expectedFound := []service.ProfileFound{
		service.ProfileFound{
			Service: "yelp",
			Name:    "Pimlico Fresh",
			ID:      "pimlico-fresh-london",
		},
	}
	expectedTotalFound := 1
	expectedTotalRejected := 19
	if err != nil {
		t.Error(err)
	}
	if actualTotalFound := len(found); actualTotalFound != expectedTotalFound {
		t.Errorf("Expected %d profiles searching for %s.\nGot: %d\n%#v", expectedTotalFound, name, actualTotalFound, found)
	} else if !reflect.DeepEqual(found, expectedFound) {
		t.Errorf("Fail %s\nExpected: %#v\nReceived: %#v", name, expectedFound, found)
	}
	if actualTotalRejected := len(rejected); actualTotalRejected != expectedTotalRejected {
		t.Errorf("Expected %d rejected profiles searching for %s.\nGot: %d\n%#v", expectedTotalRejected, name, actualTotalRejected, rejected)
	}
}

func TestSearchLocation(t *testing.T) {
	name := "Pimlico Fresh"
	location := "London"
	found, rejected, err := service.SplitSearch(svc.SearchLocation(name, location))
	expectedFound := []service.ProfileFound{
		service.ProfileFound{
			Service: "yelp",
			ID:      "pimlico-fresh-london",
			Name:    "Pimlico Fresh",
		},
	}
	expectedTotalFound := 1
	expectedTotalRejected := 19
	if err != nil {
		t.Error(err)
	}
	if actualTotalFound := len(found); actualTotalFound != expectedTotalFound {
		t.Errorf("Expected %d profiles searching for %s.\nGot: %d\n%#v", expectedTotalFound, name, actualTotalFound, found)
	} else if !reflect.DeepEqual(found, expectedFound) {
		t.Errorf("Fail %s\nExpected: %#v\nReceived: %#v", name, expectedFound, found)
	}
	if actualTotalRejected := len(rejected); actualTotalRejected != expectedTotalRejected {
		t.Errorf("Expected %d rejected profiles searching for %s.\nGot: %d\n%#v", expectedTotalRejected, name, actualTotalRejected, rejected)
	}
}

type testProfile struct {
	name     string
	location string
	expected service.Profile
}

var testProfiles = []testProfile{

	testProfile{
		name:     "Pimlico Fresh",
		location: "London",
		expected: service.Profile{
			Service:  "yelp",
			ID:       "pimlico-fresh-london",
			URL:      "http://www.yelp.co.uk/biz/pimlico-fresh-london",
			Name:     "Pimlico Fresh",
			Verified: false,
			Lat:      51.4923913743,
			Lng:      -0.14021524722,
			Address: service.Address{
				StreetAddress: "85 Wilton Road",
				Postcode:      "SW1V 1DE",
				Locality:      "London",
				Region:        "",
				Country:       "GB",
			},
			Phone: "+44 20 7932 0030",
			Categories: []string{
				"Coffee & Tea",
				"Breakfast & Brunch",
			},
			Website:      "",
			Rating:       service.NewFloat32(4),
			MaxRating:    service.NewInt(5),
			TotalReviews: service.NewInt(35),
			Summary:      "",
			Hours: service.ProfileHours{
				Mon: []string{"09:00 - 18:00"},
				Tue: []string{"09:00 - 18:00"},
				Wed: []string{"09:00 - 18:00"},
				Thu: []string{"09:00 - 18:00"},
				Fri: []string{"09:00 - 18:00"},
				Sat: []string{"09:00 - 18:00"},
				Sun: []string{"09:00 - 18:00"},
			},
		},
	},

	testProfile{
		name:     "Gastronomica",
		location: "London",
		expected: service.Profile{
			Service:  "yelp",
			ID:       "gastronomica-london-2",
			URL:      "http://www.yelp.co.uk/biz/gastronomica-london-2",
			Name:     "Gastronomica",
			Verified: false,
			Lat:      51.4919465,
			Lng:      -0.1381519,
			Address: service.Address{StreetAddress: "45 Tachbrook Street, Pimlico",
				Postcode: "SW1V 2LZ",
				Locality: "London",
				Region:   "",
				Country:  "GB"},
			Phone: "+44 20 7233 6656",
			Categories: []string{
				"Delis",
				"Beer, Wine & Spirits",
				"Coffee & Tea",
			},
			Website:      "http://www.gastronomica.co.uk/",
			Rating:       service.NewFloat32(3.5),
			MaxRating:    service.NewInt(5),
			TotalReviews: service.NewInt(13),
			Summary:      "# Specialties\n\n Artisanal cheeses, cured meat, regional Italian food and wine.  Try our great Melanzane alla parmigiana, Lasagne and fresh pasta! Great selection of Piedmontese wines (Barolo, Barbaresco, Barbera and Nebbiolo from several quality producers).\n\n# History\n\n Established in 2008.\n\n Marco Vineis started importing high quality food from Italy more that 10 years ago, creating Gastronomica.  We at Gastronomica believe that poor rural economies have great potential, if supported by effective organisation, and that Italy's traditional foods are literally masterpieces. The know-how and skills of shepherds and artisans contribute greatly to the local culture and economy, maintaining important traditions and helping to regenerate their communities. Taste this philosophy at our shop in the central Victoria area, Gastronomica Pimlico!\n\n# Gastronomica also recommends\n\n         [Gastronomica Wapping](/biz/gastronomica-wapping-london)      5 reviews   [Italian](/search?find_loc=London\u0026cflt=italian), [Coffee \u0026 Tea](/search?find_loc=London\u0026cflt=coffee), [Cafes](/search?find_loc=London\u0026cflt=cafes) Gastronomica says, “Nearby the Wapping Project and just beside Wapping overground is our vibrant Italian café and deli, appealing to food lovers far and wide.”         [Gastronomica](/biz/gastronomica-london-4)      1 review   [Food Stands](/search?find_loc=London\u0026cflt=foodstands), [Italian](/search?find_loc=London\u0026cflt=italian) Gastronomica says, “Appealing for people in search for original foodstuffs. Great selection of finest Italian products and specially prepared sandwich or hot piadina.”",
			Hours: service.ProfileHours{
				Mon: []string{"08:00 - 23:00"},
				Tue: []string{"08:00 - 23:00"},
				Wed: []string{"08:00 - 23:00"},
				Thu: []string{"08:00 - 23:00"},
				Fri: []string{"08:00 - 23:00"},
				Sat: []string{"08:00 - 23:00"},
				Sun: []string{"10:00 - 22:00"},
			},
		},
	},

	testProfile{
		name:     "Home Burger",
		location: "Madrid",
		expected: service.Profile{
			Service:  "yelp",
			ID:       "home-burger-bar-madrid-3",
			URL:      "http://www.yelp.es/biz/home-burger-bar-madrid-3",
			Name:     "Home Burger Bar",
			Verified: false,
			Lat:      40.4218911,
			Lng:      -3.7056914,
			Address: service.Address{
				StreetAddress: "Calle de Silva, 25",
				Postcode:      "28004",
				Locality:      "Madrid",
				Region:        "",
				Country:       "ES",
			},
			Phone:        "+34 911 151 279",
			Categories:   []string{"Burgers"},
			Website:      "http://www.homeburgerbar.com",
			Rating:       service.NewFloat32(4),
			MaxRating:    service.NewInt(5),
			TotalReviews: service.NewInt(15),
			Summary:      "",
			Hours: service.ProfileHours{
				Mon: []string{"13:30 - 16:00", "20:30 - 00:00"},
				Tue: []string{"13:30 - 16:00", "20:30 - 00:00"},
				Wed: []string{"13:30 - 16:00", "20:30 - 00:00"},
				Thu: []string{"13:30 - 16:00", "20:30 - 00:00"},
				Fri: []string{"13:00 - 00:00"},
				Sat: []string{"13:00 - 00:00"},
				Sun: []string{"13:00 - 23:00"},
			},
		},
	},
}

func TestProfile(t *testing.T) {
	for _, test := range testProfiles {
		name := test.name
		location := test.location
		expectedProfile := test.expected
		found, _, err := svc.SearchLocation(name, location)
		if err != nil {
			t.Fatal(err)
		}
		if len(found) == 0 {
			t.Errorf("No results for %s in %s", name, location)
			continue
		}
		actualProfile, err := svc.Profile(found[0].ID)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(actualProfile, expectedProfile) {
			t.Errorf("Fail %s\nExpected: %s\nReceived: %s", name, asJSON(expectedProfile), asJSON(actualProfile))
		}
	}
}

func asJSON(x interface{}) []byte {
	result, err := json.Marshal(x)
	if err != nil {
		panic(err)
	}
	return result
}
