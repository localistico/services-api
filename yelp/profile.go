package yelp

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/localistico/services-api/service"
	"github.com/garyburd/go-oauth/oauth"
	"github.com/kellydunn/golang-geo"
	"gopkgs.com/unidecode.v1"
)

const (
	baseURL          = "http://api.yelp.com"
	searchPath       = "/v2/search"
	placeDetailsPath = "/v2/business/%s"
)

var references = map[string]string{}

// Service implements service.ProfileService
type Service struct {
	pageLimit        string
	oauthClient      *oauth.Client
	oauthCredentials *oauth.Credentials
	client           *http.Client
	scraperClient    *http.Client
}

// NewService creates a new Service object
func NewService(consumerKey, consumerSecret, tokenKey, tokenSecret string, scraperTransport http.RoundTripper) (s *Service, err error) {
	if consumerKey == "" || consumerSecret == "" || tokenKey == "" || tokenSecret == "" {
		return nil, errors.New("need to provide a complete set of API credentials")
	}
	return &Service{
		pageLimit: "20",
		client:    &http.Client{},
		scraperClient: &http.Client{
			Transport: scraperTransport,
		},
		oauthClient: &oauth.Client{
			Credentials: oauth.Credentials{
				Token:  consumerKey,
				Secret: consumerSecret,
			},
		},
		oauthCredentials: &oauth.Credentials{
			Token:  tokenKey,
			Secret: tokenSecret,
		},
	}, nil
}

// SetTransport sets the http transport used for the API requests
func (s *Service) SetTransport(t http.RoundTripper) {
	s.client = &http.Client{Transport: t}
}

// GetTransport returns the http transport used for the API requests
func (s *Service) GetTransport() http.RoundTripper {
	t := s.client.Transport
	if t == nil {
		return http.DefaultTransport
	}
	return t
}

// Service returns the name of the service
func (s *Service) Service() string {
	return "yelp"
}

// SearchLocation geocode the loc location and
// search for profiles around that location
func (s *Service) SearchLocation(name string, loc string) (found []service.ProfileFound, err error) {
	query := map[string]string{}
	query["limit"] = s.pageLimit
	query["location"] = loc
	query["term"] = name
	url := baseURL + searchPath

	response := &apiSearchResponse{}
	err = s.requestSearch(url, query, response)
	if err != nil {
		return
	}

	filter := service.SimilarNameFilter(name)
	found, err = s.filterResults(filter, response)
	return
}

// SearchBBox search for business profiles inside
// bounding box specified by sw (south wet) and
// ne (north east) coordinates
func (s *Service) SearchBBox(name string, sw *geo.Point, ne *geo.Point) (found []service.ProfileFound, err error) {
	panic("SearchBBox not implemented")
}

type apiSearchResult struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type apiSearchResponse struct {
	Total   int               `json:"total"`
	Results []apiSearchResult `json:"businesses"`
	Error   *apiErrorResult   `json:"error"`
}

type apiErrorResult struct {
	ID   string `json:"id"`
	Text string `json:"text"`
}

// ErrNotFound returned when requested profile does not exist
var ErrNotFound = errors.New("profile not found")

func (s *Service) request(uri string, params map[string]string, response interface{}) error {
	p := url.Values{}
	for k, v := range params {
		p.Set(k, v)
	}
	res, err := s.oauthClient.Get(s.client, s.oauthCredentials, uri, p)
	var body []byte
	if err != nil {
		return err
	}
	defer res.Body.Close()
	switch {
	case res.StatusCode == 400:
		return errors.New("bad request")
	case res.StatusCode == 404:
		return ErrNotFound
	case res.StatusCode != 200:
		return errors.New(res.Status)
	default:
		body, err = ioutil.ReadAll(res.Body)
		if err != nil {
			return err
		}
	}
	//fmt.Printf("%s\n", body)

	return json.Unmarshal(body, &response)
}

func (s *Service) requestProfile(url string, params map[string]string, response *apiProfileResult) error {
	err := s.request(url, params, response)
	if err != nil {
		return err
	}

	if response.Error != nil {
		return fmt.Errorf("%s: %s", response.Error.ID, response.Error.Text)
	}

	return nil
}

func (s *Service) requestSearch(url string, params map[string]string, response *apiSearchResponse) error {
	err := s.request(url, params, response)
	if err != nil {
		return err
	}

	if response.Error != nil && response.Error.ID != "UNAVAILABLE_FOR_LOCATION" {
		return fmt.Errorf("%s: %s", response.Error.ID, response.Error.Text)
	}

	return nil
}

// SearchAround searches for venues around the
// specified coordinates. radius for search is
// specified in meters
func (s *Service) SearchAround(name string, lat, lng float64, radius int) (found []service.ProfileFound, err error) {
	query := map[string]string{}
	query["limit"] = s.pageLimit
	query["term"] = name
	query["ll"] = fmt.Sprintf("%f,%f", lat, lng)
	query["radius_filter"] = fmt.Sprintf("%d", radius)
	url := baseURL + searchPath

	var response = &apiSearchResponse{}
	err = s.requestSearch(url, query, response)
	if err != nil {
		return found, err
	}

	filter := service.SimilarNameFilter(name)
	return s.filterResults(filter, response)
}

func (s *Service) filterResults(filter service.FilterFunc, response *apiSearchResponse) ([]service.ProfileFound, error) {
	var (
		found []service.ProfileFound
		err   error
	)

	for _, result := range response.Results {
		profile := service.ProfileFound{
			Service: "yelp",
			ID:      result.ID,
			Name:    result.Name,
		}
		var matched bool
		if matched, err = filter(profile); !matched {
			profile.Discarded = true
		}
		found = append(found, profile)
		if err != nil {
			return found, err
		}
	}

	return found, nil
}

// Profile extracts information for the profile with the specified id
func (s *Service) Profile(id string) (profile service.Profile, err error) {
	url := fmt.Sprintf(baseURL+placeDetailsPath, id)
	var response apiProfileResult
	err = s.requestProfile(url, nil, &response)
	if err != nil {
		return profile, err
	}

	return s.adapt(&response)
}

// CheckProfileStatus checks whether the passed profile has been merged or deleted and fills the appropriate field
func (s *Service) CheckProfileStatus(p *service.ProfileStatus) error {
	if p.ID == "" {
		return errors.New("no profile id")
	}
	if p.Service != s.Service() {
		return errors.New("profile from a different service")
	}

	id := unidecode.Unidecode(p.ID)
	url := fmt.Sprintf(baseURL+placeDetailsPath, id)
	var response apiProfileResult
	err := s.requestProfile(url, nil, &response)
	if err == ErrNotFound {
		p.Deleted = true
		return nil
	}

	if err != nil {
		return err
	}

	if p.ID != response.ID {
		p.BestID = response.ID
	}

	return nil
}
