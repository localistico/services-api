package yelp

import (
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/ernesto-jimenez/scraperboard"
	"github.com/localistico/services-api/helpers"
	"github.com/localistico/services-api/service"
)

type apiProfileResult struct {
	ID           string          `json:"id"`
	URL          string          `json:"url"`
	Name         string          `json:"name"`
	Phone        string          `json:"display_phone"`
	Rating       float32         `json:"rating"`
	TotalReviews int             `json:"review_count"`
	Claimed      bool            `json:"is_claimed"`
	Location     resultAddress   `json:"location"`
	Categories   [][]string      `json:"categories"`
	Error        *apiErrorResult `json:"error"`
}

type resultAddress struct {
	City        string   `json:"city"`
	PostalCode  string   `json:"postal_code"`
	CountryCode string   `json:"country_code"`
	Address     []string `json:"address"`
}

func (s *Service) scrape(profile *service.Profile) error {
	scraper, err := scraperboard.NewScraperFromString(scraperXML)
	if err != nil {
		return err
	}

	var response scraperResponse
	uri, err := url.Parse(profile.URL)
	if err != nil {
		return err
	}
	// Force yelp.com to get results in english
	uri.Host = "www.yelp.com"
	res, err := s.scraperClient.Get(uri.String())
	if err != nil {
		return err
	}
	defer res.Body.Close()
	err = scraper.ExtractFromResponse(res, &response)
	if err != nil {
		return err
	}

	profile.Website = response.Website
	profile.Summary = response.Summary
	profile.Lat, _ = strconv.ParseFloat(response.Latitude, 64)
	profile.Lng, _ = strconv.ParseFloat(response.Longitude, 64)
	if profile.Lat == 0 || profile.Lng == 0 {
		return fmt.Errorf("cannot extract coordinates")
	}
	//total, err := strconv.Atoi(response.TotalPhotos)
	//if total == 0 {
	//profile.TotalPhotos = len(response.TotalPhotos)
	//} else {
	//profile.TotalPhotos = total
	//}

	for _, hour := range response.Hours {
		times, err := parseTimes(hour.NormalizedTimes())
		if err != nil {
			return err
		}
		switch hour.Day {
		case "Mon":
			profile.Hours.Mon = times
		case "Tue":
			profile.Hours.Tue = times
		case "Wed":
			profile.Hours.Wed = times
		case "Thu":
			profile.Hours.Thu = times
		case "Fri":
			profile.Hours.Fri = times
		case "Sat":
			profile.Hours.Sat = times
		case "Sun":
			profile.Hours.Sun = times
		default:
			return fmt.Errorf("cannot recognize: %s", hour.Day)
		}
	}

	return nil
}

func parseTimes(times []string) ([]string, error) {
	var result []string
	for _, timeframe := range times {
		t := helpers.ParseTimes(timeframe)
		if len(t) != 2 {
			return result, fmt.Errorf("invalid timeframe: %s (returned times %s)", timeframe, t)
		}
		from := t[0].Format("15:04")
		to := t[1].Format("15:04")
		result = append(result, fmt.Sprintf("%s - %s", from, to))
	}
	return result, nil
}

type scraperResponse struct {
	Summary     string        `json:"summary"`
	Photos      []struct{}    `json:"photos"`
	TotalPhotos string        `json:"total_photos"`
	Website     string        `json:"website"`
	Hours       []scraperTime `json:"hours"`
	Latitude    string        `json:"latitude"`
	Longitude   string        `json:"longitude"`
}

type scraperTime struct {
	Day           string   `json:"day"`
	Times         []string `json:"times"`
	CompleteTimes string   `json:"complete_times"`
}

var timeframeRegex = regexp.MustCompile(`(\d{1,2}:\d{2}) *([ap]\.?m\.?)?[^\d]+(\d{1,2}:\d{2}) *([ap]\.?m\.?)?`)
var weekDays = map[string][]string{
	"Mon": []string{"Sun", "Tue"},
	"Tue": []string{"Mon", "Wed"},
	"Wed": []string{"Tue", "Thu"},
	"Thu": []string{"Wed", "Fri"},
	"Fri": []string{"Thu", "Sat"},
	"Sat": []string{"Fri", "Sun"},
	"Sun": []string{"Sat", "Mon"},
}

func (s *scraperTime) PreviousDay() string {
	d := weekDays[s.Day]
	if len(d) != 2 {
		return ""
	}
	return d[0]
}

func (s *scraperTime) NextDay() string {
	d := weekDays[s.Day]
	if len(d) != 2 {
		return ""
	}
	return d[1]
}

func (s scraperTime) NormalizedTimes() []string {
	var result []string
	totalTimes := len(s.Times)
	s.CompleteTimes = strings.ToLower(strings.TrimSpace(s.CompleteTimes))
	if totalTimes == 0 && s.CompleteTimes == "open 24 hours" {
		result = append(result, "12:00 am - 12:00 am")
	}
	for i := 0; i < totalTimes; i += 2 {
		from := s.Times[i]
		if from == s.PreviousDay() {
			from = "00:00"
		}
		to := s.Times[i+1]
		if to == s.NextDay() {
			to = "00:00"
		}
		times := fmt.Sprintf("%s - %s", from, to)
		result = append(result, times)
	}
	return result
}

func (s *Service) adapt(response *apiProfileResult) (service.Profile, error) {
	profile := service.Profile{}
	profile.Service = "yelp"
	profile.ID = response.ID
	profile.URL = response.URL
	profile.Name = response.Name
	profile.SetPhone(response.Phone)
	profile.Rating = &response.Rating
	maxRating := 5
	profile.MaxRating = &maxRating
	profile.TotalReviews = &response.TotalReviews
	profile.Verified = response.Claimed
	profile.Address.StreetAddress = strings.Join(response.Location.Address, ", ")
	profile.Address.Locality = response.Location.City
	profile.Address.Postcode = response.Location.PostalCode
	profile.Address.Country = response.Location.CountryCode
	for _, category := range response.Categories {
		profile.Categories = append(profile.Categories, category[0])
	}

	err := s.scrape(&profile)

	return profile, err
}

var scraperXML = `
<?xml version="1.0" encoding="UTF-8" ?>
<Scraper>
  <Property name="latitude" selector="[data-map-state]">
    <Filter type="attr" argument="data-map-state"/>
    <Filter type="regex" argument="latitude[^:]+: ([^},]+)[,}]"/>
  </Property>
  <Property name="longitude" selector="[data-map-state]">
    <Filter type="attr" argument="data-map-state"/>
    <Filter type="regex" argument="longitude[^:]+: ([^},]+)[,}]"/>
  </Property>
  <Property name="website" selector=".biz-website a">
    <Filter type="attr" argument="href"/>
    <Filter type="queryParameter" argument="url"/>
  </Property>
  <Property name="totalPhotos" selector=".showcase-photos .see-more">
    <Filter type="text"/>
    <Filter type="regex" argument="(\d+)"/>
    <!--FIXME: totalPhotos is null when photos > 0 && photos < 3-->
  </Property>
  <Each name="photos" selector=".showcase-photos .photo">
    <Property name="src" selector="img">
      <Filter type="attr" argument="src"/>
    </Property>
  </Each>
  <Property name="summary" selector=".from-biz-owner-content">
    <Filter type="markdown"/>
  </Property>
  <Each name="hours" selector=".hours-table tr">
    <Property name="day" selector="th">
        <Filter type="text"/>
    </Property>
    <ArrayProperty name="times" selector="td:not([class='extra']) span">
      <Filter type="text"/>
    </ArrayProperty>
    <Property name="completeTimes" selector="td:not([class='extra'])"/>
  </Each>
</Scraper>
`
