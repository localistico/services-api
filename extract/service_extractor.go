package extract

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"time"

	"github.com/beefsack/go-rate"
	"github.com/localistico/services-api/service"
	"github.com/stvp/rollbar"
)

// MaxAttempts is the amount of times a profile or search extraction has to fail before transitioning from failing to failed
const MaxAttempts = 3

// RPS is the max requests per second allowed
const RPS = 5

// ServiceExtractor wraps a service.ProfileServiceCh to rate limit requests to the profile service, backoff on errors and service several extractions
type ServiceExtractor struct {
	svc             service.ProfileService
	searches        chan searchRequest
	profiles        chan profileRequest
	profileStatuses chan profileStatusRequest
}

// NewServiceExtractor creates a new service extractor
func NewServiceExtractor(svc service.ProfileService) *ServiceExtractor {
	extractor := &ServiceExtractor{
		svc:             svc,
		searches:        make(chan searchRequest),
		profiles:        make(chan profileRequest),
		profileStatuses: make(chan profileStatusRequest),
	}
	go extractor.runExtractor()
	return extractor
}

func (s *ServiceExtractor) runExtractor() {
	var (
		searchRequests        []searchRequest
		profileRequests       []profileRequest
		profileStatusRequests []profileStatusRequest
		nextSearch            searchRequest
		nextProfile           profileRequest
		nextProfileStatus     profileStatusRequest
	)
	searches, profiles, profileStatuses := s.requestProcessor()
	for {
		var processSearch chan<- searchRequest
		var processProfile chan<- profileRequest
		var processStatus chan<- profileStatusRequest
		if len(profileRequests) > 0 {
			processProfile = profiles
			nextProfile = profileRequests[0]
		} else if len(searchRequests) > 0 {
			processSearch = searches
			nextSearch = searchRequests[0]
		} else if len(profileStatusRequests) > 0 {
			processStatus = profileStatuses
			nextProfileStatus = profileStatusRequests[0]
		}
		select {
		case search := <-s.searches:
			// New search request
			if search.Search.Status.Status == StatusUnknown {
				search.Search.Status.Status = StatusPending
			}
			log.Printf("%s search_request=received", search.Search.Log())
			searchRequests = append(searchRequests, search)
		case processSearch <- nextSearch:
			searchRequests = searchRequests[1:]
		case profile := <-s.profiles:
			if profile.Profile.Status.Status == StatusUnknown {
				profile.Profile.Status.Status = StatusPending
			}
			// New profile request
			log.Printf("%s profile_request=received", profile.Profile.Log())
			profileRequests = append(profileRequests, profile)
		case processProfile <- nextProfile:
			profileRequests = profileRequests[1:]
		case status := <-s.profileStatuses:
			if status.ProfileStatus.Status.Status == StatusUnknown {
				status.ProfileStatus.Status.Status = StatusPending
			}
			log.Printf("%s profile_status_request=received", status.ProfileStatus.Log())
			profileStatusRequests = append(profileStatusRequests, status)
		case processStatus <- nextProfileStatus:
			profileStatusRequests = profileStatusRequests[1:]
		}
	}
}

func (s *ServiceExtractor) requestProcessor() (chan<- searchRequest, chan<- profileRequest, chan<- profileStatusRequest) {
	searchRequests := make(chan searchRequest)
	profileRequests := make(chan profileRequest)
	profileStatusRequests := make(chan profileStatusRequest)
	go func() {
		svc := s.Service()
		errors := 0
		rl1 := rate.New(RPS, time.Second)
		rl2 := rate.New(1000, time.Hour)
		rl3 := rate.New(20000, time.Hour*24)
		for {
			wait := true
			errorWait := (time.Second / RPS) * time.Duration(rand.Int63n(int64(math.Pow(2, float64(errors)))))
			select {

			case request := <-searchRequests:
				request.Search.Attempts++
				log.Printf("%s search_request=processing", request.Search.Log())
				if request.Search.ExtractionFinished() {
					request.Results <- request.Search
					wait = false
					continue
				}
				var found []service.ProfileFound
				var err error
				if request.Search.Location != "" {
					found, err = s.svc.SearchLocation(request.Search.Name, request.Search.Location)
				} else {
					found, err = s.svc.SearchAround(request.Search.Name, request.Search.Lat, request.Search.Lng, request.Search.Radius)
				}
				if err == nil {
					filter := service.ExcludeNamesFilter(request.Search.IgnoredNames)
					err = service.DiscardProfiles(found, filter)
				}
				if err == nil {
					request.Search.Status.Status = StatusCompleted
					request.Search.Results = append(request.Search.Results, found...)
					request.Search.Finished()
					errors = errors / 2
				} else if request.Search.Attempts >= MaxAttempts {
					request.Search.Finished()
					rollbar.Error("error", fmt.Errorf("%s %s", request.Search.Service, err.Error()))
					errors++
				} else {
					request.Search.Failing(err)
					rollbar.Error("error", fmt.Errorf("%s %s", request.Search.Service, err.Error()))
					log.Printf("%s search_request=requesting_retry", request.Search.Log())
					s.searches <- request
					errors++
				}
				log.Printf("%s search_request=replying", request.Search.Log())
				request.Results <- request.Search

			case request := <-profileRequests:
				request.Profile.Attempts++
				log.Printf("%s profile_request=processing", request.Profile.Log())
				if request.Profile.ExtractionFinished() {
					request.Profiles <- request.Profile
					wait = false
					continue
				}
				profile, err := s.svc.Profile(request.Profile.RequestedID)
				request.Profile.Profile = &profile
				if err == nil {
					request.Profile.Status.Status = StatusCompleted
					request.Profile.Finished()
					errors = errors / 2
				} else if request.Profile.Attempts >= MaxAttempts {
					request.Profile.Finished()
					rollbar.Error("error", fmt.Errorf("%s %s", request.Profile.Service, err.Error()))
					errors++
				} else {
					switch err.(type) {
					case *service.NotBusinessProfileError:
						request.Profile.Cancel(err)
						log.Printf("extraction=profile-not-business %s", request.Profile.Log())
					default:
						request.Profile.Failing(err)
						rollbar.Error("error", fmt.Errorf("%s %s", request.Profile.Service, err.Error()))
						log.Printf("%s profile_request=requesting_retry", request.Profile.Log())
						s.profiles <- request
						errors++
					}
				}
				log.Printf("%s profile_request=replying", request.Profile.Log())
				request.Profiles <- request.Profile

			case request := <-profileStatusRequests:
				request.ProfileStatus.Attempts++
				log.Printf("%s profile_status_request=processing", request.ProfileStatus.Log())
				if request.ProfileStatus.ExtractionFinished() {
					request.ProfileStatuses <- request.ProfileStatus
					wait = false
					continue
				}
				err := s.svc.CheckProfileStatus(request.ProfileStatus.ProfileStatus)
				if err == nil {
					request.ProfileStatus.Status.Status = StatusCompleted
					request.ProfileStatus.Finished()
					errors = errors / 2
				} else if request.ProfileStatus.Attempts >= MaxAttempts {
					request.ProfileStatus.Finished()
					rollbar.Error("error", fmt.Errorf("%s %s", request.ProfileStatus.Service, err.Error()))
					errors++
				} else {
					request.ProfileStatus.Failing(err)
					rollbar.Error("error", fmt.Errorf("%s %s", request.ProfileStatus.Service, err.Error()))
					log.Printf("%s profile_status_request=requesting_retry", request.ProfileStatus.Log())
					s.profileStatuses <- request
					errors++
				}
				log.Printf("%s profile_request=replying", request.ProfileStatus.Log())
				request.ProfileStatuses <- request.ProfileStatus
			}
			if wait {
				if errorWait > 0 {
					waitAndLog(svc, "backoff", errorWait)
				}
				time.Sleep(errorWait)
				waitRateLimit(svc, "rps", rl1)
				waitRateLimit(svc, "rph", rl2)
				waitRateLimit(svc, "rpd", rl3)
			}
		}
	}()
	return searchRequests, profileRequests, profileStatusRequests
}

func waitRateLimit(service, limit string, rl *rate.RateLimiter) {
	if ok, remaining := rl.Try(); !ok {
		waitAndLog(service, limit, remaining)
	}
}

func waitAndLog(service, limit string, d time.Duration) {
	log.Printf("service=%s limit=%s wait=%q waitMillis=%d sleeping", service, limit, d.String(), d/time.Millisecond)
	time.Sleep(d)
	log.Printf("service=%s limit=%s done", service, limit)
}

// Service returns the service ID from the wrapped ProfileServiceCh
func (s *ServiceExtractor) Service() string {
	return s.svc.Service()
}

func (s *ServiceExtractor) searchesCh() chan<- searchRequest {
	return s.searches
}

func (s *ServiceExtractor) profilesCh() chan<- profileRequest {
	return s.profiles
}

func (s *ServiceExtractor) profilesStatusCh() chan<- profileStatusRequest {
	return s.profileStatuses
}
