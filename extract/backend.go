package extract

import (
	"errors"

	"github.com/jinzhu/copier"
)

// ExtractionsBackend defines the interafce to persist and fetch extractions
type ExtractionsBackend interface {
	FindOrCreateExtraction(*Extraction) error
	FindOrCreateSearch(*Extraction, *Search) error
	FindOrCreateProfile(*Extraction, *Profile) error
	FindOrCreateProfileStatus(*Extraction, *ProfileStatus) error
	FindExtraction(string) (*Extraction, error)
	FindPendingExtractions() ([]*Extraction, error)
	FindSearch(string) (*Search, error)
	FindProfile(string) (*Profile, error)
	FindProfileStatus(string) (*ProfileStatus, error)
	SaveExtraction(*Extraction) error
	SaveProfile(*Profile) error
	SaveProfileStatus(*ProfileStatus) error
	SaveSearch(*Search) error
	FindSearches(*Extraction) ([]*Search, error)
	FindProfiles(*Extraction) ([]*Profile, error)
	FindProfileStatuses(*Extraction) ([]*ProfileStatus, error)
}

// InMemoryBackend stores information in memory for tests
type InMemoryBackend struct {
	extractions               map[string]*Extraction
	extractionSearches        map[string][]string
	extractionProfiles        map[string][]string
	extractionProfileStatuses map[string][]string
	searches                  map[string]*Search
	profiles                  map[string]*Profile
	profileStatuses           map[string]*ProfileStatus
}

// NewInMemoryBackend initializes a backend that saves extractions in memory for testing
func NewInMemoryBackend() InMemoryBackend {
	backend := InMemoryBackend{
		extractions:               make(map[string]*Extraction),
		extractionSearches:        make(map[string][]string),
		extractionProfiles:        make(map[string][]string),
		extractionProfileStatuses: make(map[string][]string),
		searches:                  make(map[string]*Search),
		profiles:                  make(map[string]*Profile),
		profileStatuses:           make(map[string]*ProfileStatus),
	}
	return backend
}

// FindOrCreateExtraction takes an extraction and searches for it if it has an UUID or creates a new one if it doesn't
func (b *InMemoryBackend) FindOrCreateExtraction(extraction *Extraction) error {
	if extraction.Status == nil {
		extraction.Status = &Status{}
	}
	if extraction.UUID == "" {
		extraction.initStatus()
		b.extractionSearches[extraction.UUID] = []string{}
		b.extractionProfiles[extraction.UUID] = []string{}
		b.extractionProfileStatuses[extraction.UUID] = []string{}
		extraction.CorrelationUUID = extraction.UUID
		return b.SaveExtraction(extraction)
	}
	val, err := b.FindExtraction(extraction.UUID)
	if err != nil {
		return err
	}
	copier.Copy(extraction, val)
	return nil
}

// FindExtraction gets an extraction
func (b *InMemoryBackend) FindExtraction(uuid string) (*Extraction, error) {
	val, ok := b.extractions[uuid]
	if !ok {
		return nil, errors.New("extraction not found")
	}
	return val, nil
}

// FindPendingExtractions gets an extraction
func (b *InMemoryBackend) FindPendingExtractions() ([]*Extraction, error) {
	var extractions []*Extraction
	for _, extraction := range b.extractions {
		if !extraction.ExtractionFinished() {
			extractions = append(extractions, extraction)
		}
	}
	return extractions, nil
}

// FindOrCreateSearch find or create search
func (b *InMemoryBackend) FindOrCreateSearch(extraction *Extraction, search *Search) error {
	if search.Status == nil {
		search.Status = &Status{}
	}
	if search.UUID != "" {
		val, err := b.FindSearch(search.UUID)
		if err != nil {
			return err
		}
		copier.Copy(search, val)
	}
	key := extraction.UUID + ":" + search.Key()
	val, ok := b.searches[key]
	if ok {
		copier.Copy(search, val)
		return nil
	}
	search.initStatus()
	search.CorrelationUUID = extraction.CorrelationUUID
	b.searches[key] = search
	b.extractionSearches[extraction.UUID] = append(b.extractionSearches[extraction.UUID], search.UUID)
	if err := b.assignSearch(extraction, search); err != nil {
		return err
	}
	return b.SaveSearch(search)
}

func (b *InMemoryBackend) assignSearch(extraction *Extraction, search *Search) error {
	extraction.SearchIDs = append(extraction.SearchIDs, search.UUID)
	extraction.ExtractionUpdated()
	return b.SaveExtraction(extraction)
}

// FindOrCreateProfile find or create profile
func (b *InMemoryBackend) FindOrCreateProfile(extraction *Extraction, profile *Profile) error {
	if profile.Status == nil {
		profile.Status = &Status{}
	}
	if profile.UUID != "" {
		val, err := b.FindSearch(profile.UUID)
		if err != nil {
			return err
		}
		copier.Copy(profile, val)
	}
	key := extraction.UUID + ":" + profile.Key()
	val, ok := b.profiles[key]
	if ok {
		copier.Copy(profile, val)
		return nil
	}
	profile.initStatus()
	profile.CorrelationUUID = extraction.CorrelationUUID
	b.profiles[key] = profile
	b.extractionProfiles[extraction.UUID] = append(b.extractionProfiles[extraction.UUID], profile.UUID)
	if err := b.assignProfile(extraction, profile); err != nil {
		return err
	}
	return b.SaveProfile(profile)
}

// FindOrCreateProfileStatus find or create profile status
func (b *InMemoryBackend) FindOrCreateProfileStatus(extraction *Extraction, profile *ProfileStatus) error {
	if profile.Status == nil {
		profile.Status = &Status{}
	}
	if profile.UUID != "" {
		val, err := b.FindSearch(profile.UUID)
		if err != nil {
			return err
		}
		copier.Copy(profile, val)
	}
	key := extraction.UUID + ":" + profile.Key()
	val, ok := b.profileStatuses[key]
	if ok {
		copier.Copy(profile, val)
		return nil
	}
	profile.initStatus()
	profile.CorrelationUUID = extraction.CorrelationUUID
	b.profileStatuses[key] = profile
	b.extractionProfileStatuses[extraction.UUID] = append(b.extractionProfileStatuses[extraction.UUID], profile.UUID)
	if err := b.assignProfileStatus(extraction, profile); err != nil {
		return err
	}
	return b.SaveProfileStatus(profile)
}

func (b *InMemoryBackend) assignProfile(extraction *Extraction, profile *Profile) error {
	extraction.ProfileIDs = append(extraction.ProfileIDs, profile.UUID)
	extraction.ExtractionUpdated()
	return b.SaveExtraction(extraction)
}

func (b *InMemoryBackend) assignProfileStatus(extraction *Extraction, profile *ProfileStatus) error {
	extraction.ProfileStatusIDs = append(extraction.ProfileStatusIDs, profile.UUID)
	extraction.ExtractionUpdated()
	return b.SaveExtraction(extraction)
}

// FindSearches find searches from the given extraction
func (b *InMemoryBackend) FindSearches(extraction *Extraction) ([]*Search, error) {
	var searches []*Search
	for _, searchID := range b.extractionSearches[extraction.UUID] {
		search, err := b.FindSearch(searchID)
		if err != nil {
			return searches, err
		}
		searches = append(searches, search)
	}
	return searches, nil
}

// FindProfiles find profiles from the given extraction
func (b *InMemoryBackend) FindProfiles(extraction *Extraction) ([]*Profile, error) {
	var profiles []*Profile
	for _, profileID := range b.extractionProfiles[extraction.UUID] {
		profile, err := b.FindProfile(profileID)
		if err != nil {
			return profiles, err
		}
		profiles = append(profiles, profile)
	}
	return profiles, nil
}

// FindProfileStatuses find profile statuses from the given extraction
func (b *InMemoryBackend) FindProfileStatuses(extraction *Extraction) ([]*ProfileStatus, error) {
	var profiles []*ProfileStatus
	for _, profileID := range b.extractionProfileStatuses[extraction.UUID] {
		profile, err := b.FindProfileStatus(profileID)
		if err != nil {
			return profiles, err
		}
		profiles = append(profiles, profile)
	}
	return profiles, nil
}

// FindSearch find search
func (b *InMemoryBackend) FindSearch(id string) (*Search, error) {
	val, ok := b.searches[id]
	if !ok {
		return val, errors.New("not found")
	}
	return val, nil
}

// FindProfile find profile
func (b *InMemoryBackend) FindProfile(id string) (*Profile, error) {
	val, ok := b.profiles[id]
	if !ok {
		return val, errors.New("not found")
	}
	return val, nil
}

// FindProfileStatus find profile
func (b *InMemoryBackend) FindProfileStatus(id string) (*ProfileStatus, error) {
	val, ok := b.profileStatuses[id]
	if !ok {
		return val, errors.New("not found")
	}
	return val, nil
}

// SaveExtraction save extraction in memory
func (b *InMemoryBackend) SaveExtraction(extraction *Extraction) error {
	b.extractions[extraction.UUID] = extraction
	return nil
}

// SaveProfile save profile in memory
func (b *InMemoryBackend) SaveProfile(profile *Profile) error {
	b.profiles[profile.UUID] = profile
	return nil
}

// SaveProfileStatus save profile status in memory
func (b *InMemoryBackend) SaveProfileStatus(profile *ProfileStatus) error {
	b.profileStatuses[profile.UUID] = profile
	return nil
}

// SaveSearch save search in memory
func (b *InMemoryBackend) SaveSearch(search *Search) error {
	b.searches[search.UUID] = search
	return nil
}
