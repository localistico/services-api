package extract

import (
	"github.com/localistico/services-api/service"
	"github.com/jinzhu/copier"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

// MongoBackend stores extraction information in mongodb
type MongoBackend struct {
	db              *mgo.Database
	coll            *mgo.Collection
	extractions     *mgo.Collection
	searches        *mgo.Collection
	profiles        *mgo.Collection
	profileStatuses *mgo.Collection
}

// NewMongoBackend initializes a backend that saves extractions in mongodb
func NewMongoBackend(db *mgo.Database) MongoBackend {
	backend := MongoBackend{
		db:              db,
		extractions:     db.C("extractions"),
		searches:        db.C("searches"),
		profiles:        db.C("profiles"),
		profileStatuses: db.C("profile_statuses"),
	}
	return backend
}

// FindOrCreateExtraction takes an extraction and searches for it if it has an UUID or creates a new one if it doesn't
func (b *MongoBackend) FindOrCreateExtraction(extraction *Extraction) error {
	if extraction.Status == nil {
		extraction.Status = &Status{}
	}
	if extraction.UUID == "" {
		extraction.initStatus()
		extraction.CorrelationUUID = extraction.UUID
		return b.extractions.Insert(extraction)
	}
	val, err := b.FindExtraction(extraction.UUID)
	if err != nil {
		return err
	}
	copier.Copy(extraction, val)
	copier.Copy(extraction.Status, val.Status)
	return nil
}

// FindExtraction gets an extraction
func (b *MongoBackend) FindExtraction(uuid string) (*Extraction, error) {
	extraction := &Extraction{}
	err := b.extractions.Find(bson.M{"status.uuid": uuid}).One(extraction)
	return extraction, err
}

// FindPendingExtractions gets an extraction
func (b *MongoBackend) FindPendingExtractions() ([]*Extraction, error) {
	var extractions []*Extraction
	iter := b.extractions.Find(bson.M{"status.status": bson.M{"$in": []string{StatusPending, StatusUnknown, StatusFailing}}}).Iter()
	err := iter.All(&extractions)
	return extractions, err
}

// FindOrCreateSearch find or create search
func (b *MongoBackend) FindOrCreateSearch(extraction *Extraction, search *Search) error {
	if search.Status == nil {
		search.Status = &Status{}
	}
	if search.UUID != "" {
		val, err := b.FindSearch(search.UUID)
		if err != nil {
			return err
		}
		copier.Copy(search, val)
	}
	val := &Search{}
	err := b.searches.Find(bson.M{
		"status.correlationuuid": extraction.CorrelationUUID,
		"service":                search.Service,
		"name":                   search.Name,
		"lat":                    search.Lat,
		"lng":                    search.Lng,
		"radius":                 search.Radius,
		"location":               search.Location,
	}).One(val)
	if err == nil {
		copier.Copy(search, val)
		copier.Copy(search.Status, val.Status)
		return nil
	}
	search.initStatus()
	search.CorrelationUUID = extraction.CorrelationUUID
	if err := b.assignSearch(extraction, search); err != nil {
		return err
	}
	return b.searches.Insert(search)
}

func (b *MongoBackend) assignSearch(extraction *Extraction, search *Search) error {
	extraction.SearchIDs = append(extraction.SearchIDs, search.UUID)
	extraction.ExtractionUpdated()
	return b.SaveExtraction(extraction)
}

// FindOrCreateProfile find or create profile
func (b *MongoBackend) FindOrCreateProfile(extraction *Extraction, profile *Profile) error {
	if profile.Status == nil {
		profile.Status = &Status{}
	}
	if profile.UUID != "" {
		val, err := b.FindSearch(profile.UUID)
		if err != nil {
			return err
		}
		copier.Copy(profile, val)
		copier.Copy(profile.Status, val.Status)
	}
	val := &Profile{}
	err := b.profiles.Find(bson.M{
		"status.correlationuuid": extraction.CorrelationUUID,
		"service":                profile.Service,
		"requestedid":            profile.RequestedID,
	}).One(val)
	if err == nil {
		copier.Copy(profile, val)
		copier.Copy(profile.Status, val.Status)
		if profile.Profile == nil {
			profile.Profile = val.Profile
		} else {
			copier.Copy(profile.Profile, &val.Profile)
		}
		return nil
	}
	profile.initStatus()
	profile.CorrelationUUID = extraction.CorrelationUUID
	if err := b.assignProfile(extraction, profile); err != nil {
		return err
	}
	return b.profiles.Insert(profile)
}

// FindOrCreateProfileStatus find or create profile status
func (b *MongoBackend) FindOrCreateProfileStatus(extraction *Extraction, profile *ProfileStatus) error {
	if profile.Status == nil {
		profile.Status = &Status{}
	}
	if profile.UUID != "" {
		val, err := b.FindSearch(profile.UUID)
		if err != nil {
			return err
		}
		copier.Copy(profile, val)
		copier.Copy(profile.Status, val.Status)
	}
	val := &ProfileStatus{}
	err := b.profileStatuses.Find(bson.M{
		"status.correlationuuid": extraction.CorrelationUUID,
		"service":                profile.Service,
		"id":                     profile.ID,
	}).One(val)
	if err == nil {
		copier.Copy(profile, val)
		copier.Copy(profile.Status, val.Status)
		if profile.ProfileStatus == nil {
			profile.ProfileStatus = val.ProfileStatus
		} else {
			copier.Copy(profile.ProfileStatus, &val.ProfileStatus)
		}
		if profile.ProfileStatus == nil {
			profile.ProfileStatus = &service.ProfileStatus{
				ID:      profile.ID,
				Service: profile.Service,
			}
		}
		return nil
	}
	profile.initStatus()
	profile.CorrelationUUID = extraction.CorrelationUUID
	if err := b.assignProfileStatus(extraction, profile); err != nil {
		return err
	}
	if profile.ProfileStatus == nil {
		profile.ProfileStatus = &service.ProfileStatus{
			ID:      profile.ID,
			Service: profile.Service,
		}
	}
	return b.profileStatuses.Insert(profile)
}

func (b *MongoBackend) assignProfile(extraction *Extraction, profile *Profile) error {
	extraction.ProfileIDs = append(extraction.ProfileIDs, profile.UUID)
	extraction.ExtractionUpdated()
	return b.SaveExtraction(extraction)
}

func (b *MongoBackend) assignProfileStatus(extraction *Extraction, profile *ProfileStatus) error {
	extraction.ProfileStatusIDs = append(extraction.ProfileStatusIDs, profile.UUID)
	extraction.ExtractionUpdated()
	return b.SaveExtraction(extraction)
}

// FindSearches find searches from the given extraction
func (b *MongoBackend) FindSearches(extraction *Extraction) ([]*Search, error) {
	var searches []*Search
	err := b.searches.Find(
		bson.M{"status.uuid": bson.M{"$in": extraction.SearchIDs}},
	).Iter().All(&searches)
	return searches, err
}

// FindProfiles find profiles from the given extraction
func (b *MongoBackend) FindProfiles(extraction *Extraction) ([]*Profile, error) {
	var profiles []*Profile
	err := b.profiles.Find(
		bson.M{"status.uuid": bson.M{"$in": extraction.ProfileIDs}},
	).Iter().All(&profiles)
	return profiles, err
}

// FindProfileStatuses find profiles from the given extraction
func (b *MongoBackend) FindProfileStatuses(extraction *Extraction) ([]*ProfileStatus, error) {
	var profiles []*ProfileStatus
	err := b.profileStatuses.Find(
		bson.M{"status.uuid": bson.M{"$in": extraction.ProfileStatusIDs}},
	).Iter().All(&profiles)
	return profiles, err
}

// FindSearch find search
func (b *MongoBackend) FindSearch(uuid string) (*Search, error) {
	search := &Search{}
	err := b.searches.Find(bson.M{"status.uuid": uuid}).One(search)
	return search, err
}

// FindProfile find profile
func (b *MongoBackend) FindProfile(uuid string) (*Profile, error) {
	profile := &Profile{}
	err := b.profiles.Find(bson.M{"status.uuid": uuid}).One(profile)
	return profile, err
}

// FindProfileStatus find profile
func (b *MongoBackend) FindProfileStatus(uuid string) (*ProfileStatus, error) {
	profile := &ProfileStatus{}
	err := b.profileStatuses.Find(bson.M{"status.uuid": uuid}).One(profile)
	return profile, err
}

// SaveExtraction save extraction in memory
func (b *MongoBackend) SaveExtraction(extraction *Extraction) error {
	err := b.extractions.Update(bson.M{"status.uuid": extraction.UUID}, extraction)
	return err
}

// SaveProfile save profile in memory
func (b *MongoBackend) SaveProfile(profile *Profile) error {
	err := b.profiles.Update(bson.M{"status.uuid": profile.UUID}, profile)
	return err
}

// SaveProfileStatus save profile in memory
func (b *MongoBackend) SaveProfileStatus(profile *ProfileStatus) error {
	err := b.profileStatuses.Update(bson.M{"status.uuid": profile.UUID}, profile)
	return err
}

// SaveSearch save search in memory
func (b *MongoBackend) SaveSearch(search *Search) error {
	err := b.searches.Update(bson.M{"status.uuid": search.UUID}, search)
	return err
}

// EachPendingExtraction calls the callback with an extraction for each pending extraction
func (b *MongoBackend) EachPendingExtraction(fun func(Extraction)) error {
	iter := b.extractions.Find(bson.M{"status.status": bson.M{"$in": []string{StatusPending, StatusUnknown, StatusFailing}}}).Iter()
	extraction := Extraction{}
	for iter.Next(&extraction) {
		fun(extraction)
	}
	if err := iter.Close(); err != nil {
		return err
	}
	return nil
}
