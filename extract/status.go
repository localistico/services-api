package extract

import (
	"fmt"
	"github.com/lonelycode/go-uuid/uuid"
	"time"
)

const (
	// StatusUnknown Extraction is unkwnon
	StatusUnknown = "unknown"
	// StatusPending Extraction is pending or being extracted
	StatusPending = "pending"
	// StatusFailing Extraction is pending or being extracted and already had some error
	StatusFailing = "failing"
	// StatusCompleted Extraction has been succesfully completed
	StatusCompleted = "completed"
	// StatusFailed Extraction has failed
	StatusFailed = "failed"
	// StatusCancelled Extraction has been cancelled
	StatusCancelled = "cancelled"
)

// Status defines the properties of any extraction job
type Status struct {
	CorrelationUUID string    `json:"correlation_uuid,omitempty"`
	UUID            string    `json:"uuid"`
	Status          string    `json:"status"`
	Attempts        int       `json:"attempts,omitempty"`
	Error           string    `json:"error,omitempty"`
	LastModifiedAt  time.Time `json:"last_modified_at"`
	ExtractedAt     time.Time `json:"extracted_at"`
	RequestedAt     time.Time `json:"requested_at"`
}

// ExtractionUpdated sets LastModified to time.Now()
func (s *Status) ExtractionUpdated() {
	s.ExtractedAt = time.Now()
	s.LastModifiedAt = time.Now()
}

// Failed sets extraction status as failed with the given error and updates LastModified and ExtractedAt times
func (s *Status) Failed(err error) {
	s.Status = StatusFailed
	s.Error = err.Error()
	s.ExtractionUpdated()
}

// Failing sets extraction status as failing with the given error and updates LastModified and ExtractedAt times
func (s *Status) Failing(err error) {
	s.Status = StatusFailing
	s.Error = err.Error()
	s.ExtractionUpdated()
}

// Cancel cancels the extraction
func (s *Status) Cancel(err error) {
	if err != nil {
		s.Error = err.Error()
	}
	s.Status = StatusCancelled
	s.ExtractionUpdated()
}

// Finished transitions extraction from pending to completed or from failing to failed
func (s *Status) Finished() {
	if s.Status == StatusFailing || s.Status == StatusFailed {
		s.Status = StatusFailed
	} else if s.Status == StatusCancelled {
		s.Status = StatusCancelled
	} else {
		s.Error = ""
		s.Status = StatusCompleted
	}
	s.ExtractionUpdated()
}

func (s *Status) initStatus() {
	s.UUID = uuid.New()
	s.Status = StatusUnknown
	s.RequestedAt = time.Now()
	s.ExtractionUpdated()
}

// Log returns a logging string
func (s Status) log(kind string) string {
	msg := fmt.Sprintf("correlation=%s %s=%s status=%s", s.CorrelationUUID, kind, s.UUID, s.Status)
	if s.Error != "" {
		msg = fmt.Sprintf("%s attempts=%d error=%q", msg, s.Attempts, s.Error)
	}
	return msg
}

// ExtractionFinished returns true if the extraction is not pending
func (s *Status) ExtractionFinished() bool {
	switch s.Status {
	case StatusPending:
		return false
	case StatusFailing:
		return false
	case StatusUnknown:
		return false
	default:
		return true
	}
}
