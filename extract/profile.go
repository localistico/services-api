package extract

import (
	"fmt"

	"github.com/localistico/services-api/service"
)

// Profile contains the details about an extracted profile
type Profile struct {
	*Status
	*service.Profile
	Service     string `json:"service"`
	RequestedID string `json:"requested_id"`
}

// Key returns a string with the service and request_id
func (p *Profile) Key() string {
	return fmt.Sprintf("service=%q id=%q", p.Service, p.RequestedID)
}

// Log returns log information
func (p *Profile) Log() string {
	return fmt.Sprintf("%s service=%s profileID=%s", p.Status.log("profile"), p.Service, p.RequestedID)
}

// ProfileStatus contais the details about checking status from a profile
type ProfileStatus struct {
	*Status
	*service.ProfileStatus
	Service string `json:"string"`
	ID      string `json:"id"`
}

// Key returns a string with the service and request_id
func (p *ProfileStatus) Key() string {
	return fmt.Sprintf("service=%q id=%q", p.Service, p.ID)
}

// Log returns log information
func (p *ProfileStatus) Log() string {
	return fmt.Sprintf("%s service=%s profileID=%s", p.Status.log("profile_status"), p.Service, p.ID)
}

// Search contains the details about a search request
type Search struct {
	*Status
	Service      string                 `json:"service"`
	Name         string                 `json:"name"`
	IgnoredNames []string               `json:"ignored_names"`
	Location     string                 `json:"location,omitempty"`
	Lat          float64                `json:"lat,omitempty"`
	Lng          float64                `json:"lng,omitempty"`
	Radius       int                    `json:"radius,omitempty"`
	Results      []service.ProfileFound `json:"results"`
}

// NewSearchAround initialises a Search
func NewSearchAround(service, name string, lat, lng float64, radius int, ignoredNames []string) *Search {
	search := &Search{
		Status:       &Status{},
		Service:      service,
		Name:         name,
		Lat:          lat,
		Lng:          lng,
		Radius:       radius,
		IgnoredNames: ignoredNames,
	}
	return search
}

// NewSearchLocation initialises a Search
func NewSearchLocation(service, name, location string, ignoredNames []string) *Search {
	search := &Search{
		Status:       &Status{},
		Service:      service,
		Name:         name,
		Location:     location,
		IgnoredNames: ignoredNames,
	}
	return search
}

// Key returns a string with the key params for the search
func (s *Search) Key() string {
	return fmt.Sprintf("service=%q name=%q location=%q lat=%f lng=%f radius=%d", s.Service, s.Name, s.Location, s.Lat, s.Lng, s.Radius)
}

// Log returns log information
func (s *Search) Log() string {
	msg := s.Status.log("search")
	if s.Location == "" {
		msg = fmt.Sprintf("%s service=%s center=\"%f,%f\" radius=%d name=%q", msg, s.Service, s.Lat, s.Lng, s.Radius, s.Name)
	} else {
		msg = fmt.Sprintf("%s service=%s location=%q name=%q", msg, s.Service, s.Location, s.Name)
	}
	return msg
}
