package extract

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/kellydunn/golang-geo"
	"github.com/localistico/services-api/service"
)

// Extractor offers and API to take Extractions and fetch them
type Extractor struct {
	backend         ExtractionsBackend
	svcs            []*ServiceExtractor
	svcIDs          []string
	searchCh        map[string]chan<- searchRequest
	profileCh       map[string]chan<- profileRequest
	profileStatusCh map[string]chan<- profileStatusRequest
	extractions     map[string]chan struct{}

	client *http.Client
}

// NewExtractor initializes a new Extractor and runs Extractions in different go routines
func NewExtractor(backend ExtractionsBackend) *Extractor {
	return &Extractor{
		backend:         backend,
		searchCh:        make(map[string]chan<- searchRequest),
		profileCh:       make(map[string]chan<- profileRequest),
		profileStatusCh: make(map[string]chan<- profileStatusRequest),
		extractions:     make(map[string]chan struct{}),
		client:          &http.Client{},
	}
}

func (e *Extractor) httpClient() *http.Client {
	return e.client
}

// SetTransport sets the http.Transport for the webhook requests
func (e *Extractor) SetTransport(t http.RoundTripper) {
	e.client.Transport = t
}

// GetTransport gets the http.Transport for the webhook requests
func (e *Extractor) GetTransport() http.RoundTripper {
	t := e.client.Transport
	if t == nil {
		return http.DefaultTransport
	}
	return t
}

// MountService adds a new service to extract profiles and returns an error if another extractor has been mounted for that service
func (e *Extractor) MountService(svcExtractor service.ProfileService) error {
	svc := NewServiceExtractor(svcExtractor)
	svcID := svc.Service()
	if e.isServiceMounted(svcID) {
		return fmt.Errorf("service %s already mounted", svcID)
	}
	e.svcs = append(e.svcs, svc)
	e.svcIDs = append(e.svcIDs, svcID)
	e.searchCh[svcID] = svc.searchesCh()
	e.profileCh[svcID] = svc.profilesCh()
	e.profileStatusCh[svcID] = svc.profilesStatusCh()
	return nil
}

// MountedServices returns what services have been mounted
func (e *Extractor) MountedServices() []string {
	return e.svcIDs
}

func (e *Extractor) isServiceMounted(svcID string) bool {
	for _, id := range e.svcIDs {
		if svcID == id {
			return true
		}
	}
	return false
}

func (e *Extractor) checkExtractionServices(extraction *Extraction) error {
	unknownServices := []string{}
	for _, svcID := range extraction.Services {
		if !e.isServiceMounted(svcID) {
			unknownServices = append(unknownServices, svcID)
		}
	}
	if len(unknownServices) == 0 {
		return nil
	}
	return fmt.Errorf("unknown services %s", unknownServices)
}

func (e *Extractor) log(extraction *Extraction, key, status, msg string) {
	log.Printf("%s %s=%s %s", extraction.Log(), key, status, msg)
}

func (e *Extractor) logErr(extraction *Extraction, key string, err error) {
	e.log(extraction, key, "error", fmt.Sprintf("error=%q", err.Error()))
}

// Extract takes an extraction and starts running it unless it is invalid or it has already been extracted
func (e *Extractor) Extract(extraction *Extraction) (<-chan struct{}, error) {
	end := make(chan struct{})
	log := func(status string) {
		e.log(extraction, "extract", status, "")
	}
	logErr := func(err error) {
		e.logErr(extraction, "extract", err)
	}
	if extraction.ExtractionFinished() {
		log("finished")
		close(end)
		return end, nil
	}
	if extraction.UUID == "" {
		err := fmt.Errorf("missing extraction ID")
		e.fail(extraction, err)
		logErr(err)
		close(end)
		return end, err
	}
	if err := extraction.VerifyValid(); err != nil {
		e.fail(extraction, err)
		logErr(err)
		close(end)
		return end, err
	}
	if len(extraction.Services) == 0 {
		extraction.Services = e.svcIDs
	}
	if err := e.checkExtractionServices(extraction); err != nil {
		e.fail(extraction, err)
		logErr(err)
		close(end)
		return end, err
	}
	extraction.Status.Status = StatusPending
	if err := e.backend.SaveExtraction(extraction); err != nil {
		e.fail(extraction, err)
		logErr(err)
		close(end)
		return end, err
	}
	log("run")
	go e.runExtraction(extraction, end)
	return end, nil
}

type searchRequest struct {
	Search  *Search
	Results chan<- *Search
}

type profileRequest struct {
	Profile  *Profile
	Profiles chan<- *Profile
}

type profileStatusRequest struct {
	ProfileStatus   *ProfileStatus
	ProfileStatuses chan<- *ProfileStatus
}

// CancelExtraction cancels and stops an existing extraction
func (e *Extractor) CancelExtraction(extraction *Extraction) {
	if end, ok := e.extractions[extraction.UUID]; ok {
		end <- struct{}{}
	}
}

func (e *Extractor) runExtraction(extraction *Extraction, end chan struct{}) {
	defer close(end)
	e.extractions[extraction.UUID] = end
	defer delete(e.extractions, extraction.UUID)
	var (
		profiles                         = make(chan *Profile)
		profileStatuses                  = make(chan *ProfileStatus)
		results                          = make(chan *Search)
		pendingSearches, searchExpansion = e.defaultSearches(extraction)
		extractionResult                 = newExtractionResult(extraction)
		inFlight                         = 0
		totalProfiles                    = 0
		totalProfileStatuses             = 0
		finished                         bool
	)
	for _, check := range extraction.CheckProfiles {
		if _, ok := e.profileStatusCh[check.Service]; !ok {
			continue
		}
		requestedProfile := &ProfileStatus{
			Service: check.Service,
			ID:      check.ID,
		}
		e.backend.FindOrCreateProfileStatus(extraction, requestedProfile)
		log.Printf("%s extract=profile_status_requesting", requestedProfile.Log())
		e.profileStatusCh[check.Service] <- profileStatusRequest{
			ProfileStatus:   requestedProfile,
			ProfileStatuses: profileStatuses,
		}
		inFlight++
	}
	for i := 0; !finished; i++ {
		log.Printf("%s extract=iterate inflight=%d pending=%d totalProfiles=%d totalProfileStatuses=%d", extraction.Log(), inFlight, len(pendingSearches), totalProfiles, totalProfileStatuses)
		for _, search := range pendingSearches {
			log.Printf("%s extract=search_requesting", search.Log())
			e.searchCh[search.Service] <- searchRequest{
				Search:  search,
				Results: results,
			}
			inFlight++
		}
		pendingSearches = pendingSearches[0:0]
		select {
		case search := <-results:
			log.Printf("%s extract=search_result", search.Log())
			newProfiles := extractionResult.addSearchResults(search)
			for _, profile := range newProfiles {
				requestedProfile := &Profile{
					Service:     profile.Service,
					RequestedID: profile.ID,
				}
				e.backend.FindOrCreateProfile(extraction, requestedProfile)
				log.Printf("%s extract=profile_requesting", requestedProfile.Log())
				e.profileCh[profile.Service] <- profileRequest{
					Profile:  requestedProfile,
					Profiles: profiles,
				}
				inFlight++
			}
			if err := e.backend.SaveSearch(search); err != nil {
				extraction.Failed(err)
			}
			if search.Status.Status == StatusFailed {
				extraction.Failing(errors.New("some extractions failed"))
			}
			if search.ExtractionFinished() {
				inFlight--
			}
		case profile := <-profiles:
			log.Printf("%s extract=profile_result", profile.Log())
			totalProfiles++
			if err := e.backend.SaveProfile(profile); err != nil {
				extraction.Failed(err)
			}
			if extraction.ExpandSearch && searchExpansion.expands(profile.GeoCoordinates()) {
				expansion := e.searchCoordinates(extraction, profile.Lat, profile.Lng, extraction.Radius)
				log.Printf("%s extract=expand expanded_searches=%d", extraction.Log(), len(expansion))
				pendingSearches = append(pendingSearches, expansion...)
			}
			if profile.Status.Status == StatusFailed {
				extraction.Failing(errors.New("some extractions failed"))
			}
			if profile.ExtractionFinished() {
				inFlight--
			}
		case profile := <-profileStatuses:
			log.Printf("%s extract=profile_status_result", profile.Log())
			totalProfileStatuses++
			if err := e.backend.SaveProfileStatus(profile); err != nil {
				extraction.Failed(err)
			}
			if profile.Status.Status == StatusFailed {
				extraction.Failing(errors.New("some extractions failed"))
			}
			if profile.ExtractionFinished() {
				inFlight--
			}
		case <-end:
			extraction.Cancel(nil)
			e.finish(extraction)
			finished = true
		}
		if inFlight == 0 && len(pendingSearches) == 0 {
			e.finish(extraction)
			log.Printf("%s extract=finish", extraction.Log())
			finished = true
		} else if i == 100 {
			log.Printf("%s extract=notify", extraction.Log())
			e.progress(extraction)
			i = 0
		}
	}
}

type searchExpansion struct {
	extraction *Extraction
	coords     []*geo.Point
	radius     float64
}

func newSearchExpansion(extraction *Extraction) searchExpansion {
	if extraction.Radius == 0 {
		extraction.Radius = 5000
	}
	return searchExpansion{
		extraction: extraction,
		radius:     (float64(extraction.Radius) * 0.75) / 1000,
	}
}

func (ex *searchExpansion) contains(point *geo.Point) bool {
	for _, p := range ex.coords {
		distance := point.GreatCircleDistance(p)
		if distance <= ex.radius {
			return true
		}
	}
	return false
}

func (ex *searchExpansion) expands(point *geo.Point) bool {
	contained := ex.contains(point)
	if !contained {
		ex.coords = append(ex.coords, point)
	}
	return !contained
}

func (e *Extractor) defaultSearches(extraction *Extraction) ([]*Search, searchExpansion) {
	var searches []*Search
	searchExpansion := newSearchExpansion(extraction)
	for _, location := range extraction.Locations {
		searches = append(searches, e.searchLocation(extraction, location)...)
	}
	for _, coords := range extraction.Coordinates {
		if searchExpansion.expands(coords.GeoCoordinates()) {
			searches = append(searches, e.searchCoordinates(extraction, coords.Lat, coords.Lng, extraction.Radius)...)
		}
	}
	return searches, searchExpansion
}

func (e *Extractor) searchLocation(extraction *Extraction, location string) []*Search {
	var searches []*Search
	for _, service := range extraction.Services {
		for _, name := range extraction.NamesWithAlternatives() {
			search := NewSearchLocation(service, name, location, extraction.IgnoredNames)
			e.backend.FindOrCreateSearch(extraction, search)
			searches = append(searches, search)
		}
	}
	return searches
}

func (e *Extractor) searchCoordinates(extraction *Extraction, lat, lng float64, radius int) []*Search {
	var searches []*Search
	if lat == 0 && lng == 0 {
		return searches
	}
	for _, service := range extraction.Services {
		for _, name := range extraction.NamesWithAlternatives() {
			search := NewSearchAround(service, name, lat, lng, radius, extraction.IgnoredNames)
			e.backend.FindOrCreateSearch(extraction, search)
			searches = append(searches, search)
		}
	}
	return searches
}

func (e *Extractor) fail(extraction *Extraction, err error) {
	extraction.Failed(err)
	e.backend.SaveExtraction(extraction)
	e.NotifyProgress(extraction)
}

func (e *Extractor) progress(extraction *Extraction) {
	e.backend.SaveExtraction(extraction)
	e.NotifyProgress(extraction)
}

func (e *Extractor) finish(extraction *Extraction) {
	extraction.Finished()
	e.backend.SaveExtraction(extraction)
	e.NotifyProgress(extraction)
}

// NotifyProgress makes a POST request to the WebhookURL with the details about the Extraction
func (e *Extractor) NotifyProgress(extraction *Extraction) error {
	if extraction.Webhook == "" {
		return nil
	}
	body, err := json.Marshal(extraction)
	if err != nil {
		return err
	}
	url, err := extraction.webhookURL()
	if err != nil {
		return err
	}
	httpReq, err := http.NewRequest("POST", url.String(), bytes.NewBuffer(body))
	if err != nil {
		return err
	}
	httpReq.Header.Set("Content-Type", "application/json")
	resp, err := e.httpClient().Do(httpReq)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("webhook response status code was not 200 but %d", resp.StatusCode)
	}
	return nil
}
