package extract

import (
	"encoding/json"
	"errors"
	"io"
	"net/url"
	"strings"

	"github.com/localistico/services-api/helpers"
	"github.com/localistico/services-api/service"
	"github.com/kellydunn/golang-geo"
)

// Extraction contains the details to extract profiles
type Extraction struct {
	*Status
	// Webhook specifies a URL that will be notified about progress and completion
	Webhook string `json:"webhook,omitempty"`
	// List of names to search
	Names []string `json:"names"`
	// List of names to ignore
	IgnoredNames []string `json:"ignored_names"`
	// Locations lists the text locations to search the service around
	Locations []string `json:"locations"`
	// Coordinates lists all the coordinates to search profiles around
	Coordinates []Coordinates `json:"coordinates,omitempty"`
	// Radius specifies the radius in meters to search profiles around coordinates when seraching for Coordinates or expanding a search. Defaults to 1000 (1 Km)
	Radius int `json:"radius"`
	// Services lists the id of the services to extract profiles from. Empty services means all available services.
	Services []string `json:"services"`
	// ExpandSearch flag specifies whether to trigger new searches around found profiles to try to find as many profiles as possible
	ExpandSearch bool `json:"expand_search"`
	// CheckProfiles lists profiles whose status needs to be checked to see whether they have been merged
	CheckProfiles []CheckProfile `json:"check_profiles,omitempty"`

	ProfileIDs       []string `json:"profile_ids"`
	SearchIDs        []string `json:"search_ids"`
	ProfileStatusIDs []string `json:"profile_status_ids"`
}

// CheckProfile information about a profile that to be checked to see whether it has been merged or closed
type CheckProfile struct {
	Service string `json:"service"`
	ID      string `json:"id"`
}

// Coordinates represents a pair of coordinates for the extraction
type Coordinates struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

// GeoCoordinates returns a *geo.Point from the given coordinates
func (c Coordinates) GeoCoordinates() *geo.Point {
	return geo.NewPoint(c.Lat, c.Lng)
}

// Log returns a logging string
func (e Extraction) Log() string {
	return e.Status.log("extraction")
}

// NewExtractionRequest prepares a pending status request
func NewExtractionRequest() *Extraction {
	extraction := &Extraction{}
	extraction.Status = &Status{}
	return extraction
}

// NewExtractionRequestFromJSONReader takes a io.Reader containing an Extraction encoded in JSON
func NewExtractionRequestFromJSONReader(r io.Reader) (*Extraction, error) {
	e := NewExtractionRequest()
	err := json.NewDecoder(r).Decode(e)
	if err != nil {
		return nil, err
	}
	if len(e.ProfileIDs) != 0 {
		return nil, errors.New("profile_ids cannot be set in request")
	}
	if len(e.SearchIDs) != 0 {
		return nil, errors.New("search_ids cannot be set in request")
	}
	return e, e.VerifyValid()
}

// VerifyValid returns an error if the extraction is invalid
func (e *Extraction) VerifyValid() error {
	for i, l := range e.Locations {
		e.Locations[i] = strings.TrimSpace(l)
	}
	if len(e.Locations) == 0 && len(e.Coordinates) == 0 {
		return errors.New("location or coordinates required")
	}
	for i, n := range e.Names {
		e.Names[i] = strings.TrimSpace(n)
	}
	if len(e.Names) == 0 {
		return errors.New("at least one name is required")
	}
	if e.Webhook != "" {
		url, err := e.webhookURL()
		if err != nil {
			return err
		}
		if !url.IsAbs() {
			return errors.New("webhook must be an absolute URL")
		}
	}
	return nil
}

func (e *Extraction) webhookURL() (*url.URL, error) {
	return url.Parse(e.Webhook)
}

// NamesWithAlternatives returns Names and the alternative searches for those names
func (e *Extraction) NamesWithAlternatives() []string {
	result := []string{}
	for _, name := range e.Names {
		result = append(result, helpers.AltNames(name)...)
	}
	return result
}

// ExtractionResult contains the results from an extraction
type ExtractionResult struct {
	*Extraction
	SearchResults []service.ProfileFound `json:"search_results"`

	resultKeys map[string]bool
}

func newExtractionResult(extraction *Extraction) *ExtractionResult {
	return &ExtractionResult{
		Extraction: extraction,
		resultKeys: make(map[string]bool),
	}
}

func (e *ExtractionResult) addSearchResults(search *Search) []service.ProfileFound {
	var newProfiles []service.ProfileFound
	for _, result := range search.Results {
		var key = result.Key()
		if !result.Discarded && !e.resultKeys[key] {
			newProfiles = append(newProfiles, result)
			e.SearchResults = append(e.SearchResults, result)
			e.resultKeys[key] = true
		}
	}
	return newProfiles
}
