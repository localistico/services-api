package main

import (
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/ernesto-jimenez/httplogger"
	"github.com/localistico/services-api/helpers"
	"github.com/localistico/services-api/service"
	"github.com/gocraft/web"
)

// ServiceContext holds details on service
type ServiceContext struct {
	*Context
	service service.ProfileService
}

type profileSearchResult struct {
	Results    []service.ProfileFound `json:"results"`
	SearchedAt time.Time              `json:"searched_at"`
}

// Profile extracts details from a profile
func (c *ServiceContext) Profile(res web.ResponseWriter, req *web.Request) {
	id := req.PathParams["id"]
	profile, err := c.service.Profile(id)

	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "internal_error", err)
		return
	}
	c.renderJSON(res, http.StatusOK, profile)
}

// ProfileStatus extracts details from a profile
func (c *ServiceContext) ProfileStatus(res web.ResponseWriter, req *web.Request) {
	id := req.PathParams["id"]
	profile := service.ProfileStatus{ID: id}
	err := c.service.CheckProfileStatus(&profile)

	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "internal_error", err)
		return
	}
	c.renderJSON(res, http.StatusOK, profile)
}

// Search does a search
func (c *ServiceContext) Search(res web.ResponseWriter, req *web.Request) {
	query := req.URL.Query()
	name := query.Get("name")
	if name == "" {
		c.renderError(res, http.StatusBadRequest, "invalid_parameters", errors.New("you need to provide a name parameter"))
		return
	}
	var (
		result profileSearchResult
		err    error
	)
	if query.Get("center") != "" {
		result, err = c.searchAround(name, query)
	} else if query.Get("location") != "" {
		result, err = c.searchLocation(name, query)
	}
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "internal_error", err)
		return
	}
	c.renderJSON(res, http.StatusOK, result)
}

func (c *ServiceContext) searchAround(name string, query url.Values) (profileSearchResult, error) {
	center, err := helpers.PointFromStrCoords(query.Get("center"))
	if err != nil {
		c.Logger.Panic(err)
	}
	radius, err := strconv.Atoi(query.Get("radius"))
	if err != nil {
		radius = 1000
	}
	c.Logger.Printf("Search for name=%#v coords=%v radius=%d", name, *center, radius)
	found, err := c.service.SearchAround(
		name,
		center.Lat(),
		center.Lng(),
		radius,
	)
	return profileSearchResult{
		Results:    found,
		SearchedAt: time.Now(),
	}, err
}

func (c *ServiceContext) searchLocation(name string, query url.Values) (profileSearchResult, error) {
	location := query.Get("location")
	c.Logger.Printf("Search for name=%#v locatin=%#v", name, location)
	found, err := c.service.SearchLocation(
		name,
		location,
	)
	return profileSearchResult{
		Results:    found,
		SearchedAt: time.Now(),
	}, err
}

func serviceInitMiddleware(newService func() service.ProfileService) func(*ServiceContext, web.ResponseWriter, *web.Request, web.NextMiddlewareFunc) {
	return func(
		c *ServiceContext,
		rw web.ResponseWriter,
		req *web.Request,
		next web.NextMiddlewareFunc,
	) {
		c.service = newService()
		t := httplogger.NewLoggedTransport(c.service.GetTransport(), c.newHTTPLogger())
		c.service.SetTransport(t)
		next(rw, req)
	}
}

func routeServiceEndpoints(router *web.Router, path string, newService func() service.ProfileService) {
	subrouter := router.Subrouter(ServiceContext{}, path)
	subrouter.Middleware(serviceInitMiddleware(newService))
	subrouter.Get("/search", (*ServiceContext).Search)
	subrouter.Get("/profile/:id", (*ServiceContext).Profile)
	subrouter.Get("/profile/:id/status", (*ServiceContext).ProfileStatus)
}
