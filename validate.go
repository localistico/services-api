package main

import (
	"fmt"
	"log"
	"reflect"
	"strings"

	validator "github.com/astaxie/beego/validation"
)

// Validate receives a pointer to a struct with validationtags.
// If the struct is valid, it returns true. Otherwise it returns false and the error message
func Validate(u interface{}) (bool, error) {
	check := validator.Validation{}
	valid, err := check.Valid(u)
	if err != nil {
		log.Panic(err)
	}
	if valid {
		return valid, nil
	}
	errors := make([]string, len(check.Errors))
	value := reflect.ValueOf(u)
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}
	valueType := value.Type()
	for i, vErr := range check.Errors {
		// Use reflection to extract JSON name for errors
		field, found := valueType.FieldByName(vErr.Field)
		if !found {
			log.Panicf("No field %s in %#v", vErr.Field, u)
		}
		// Get key name (either from json tag or the field name)
		key := strings.Split(field.Tag.Get("json"), ",")[0]
		if key == "" {
			key = vErr.Field
		}
		errors[i] = fmt.Sprintf("%s: %s", key, vErr.Message)
	}
	return valid, fmt.Errorf(strings.Join(errors, ", "))
}
