package main

import (
	"crypto/subtle"
	"encoding/base64"
	"errors"
	"net/http"

	"github.com/gocraft/web"
)

func apiAuthMiddleware(apiKey *string) func(*Context, web.ResponseWriter, *web.Request, web.NextMiddlewareFunc) {
	return func(
		c *Context,
		rw web.ResponseWriter,
		req *web.Request,
		next web.NextMiddlewareFunc,
	) {
		siteAuth := base64.StdEncoding.EncodeToString([]byte(":" + *apiKey))
		if !authorized(req, siteAuth) {
			rw.Header().Set("WWW-Authenticate", "Basic realm=\"Authorization Required\"")
			c.renderError(rw, http.StatusUnauthorized, "unauthorized", errors.New("you cannot access the API with the provided API Key"))
		} else {
			next(rw, req)
		}
	}
}

func authorized(req *web.Request, expected string) bool {
	auth := req.Header.Get("Authorization")
	return secureCompare(auth, "Basic "+expected)
}

func secureCompare(given string, actual string) bool {
	if subtle.ConstantTimeEq(int32(len(given)), int32(len(actual))) == 1 {
		return subtle.ConstantTimeCompare([]byte(given), []byte(actual)) == 1
	}
	/* Securely compare actual to itself to keep constant time, but always return false */
	return subtle.ConstantTimeCompare([]byte(actual), []byte(actual)) == 1 && false
}
