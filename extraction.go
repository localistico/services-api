package main

import (
	"net/http"

	"github.com/localistico/services-api/extract"
	"github.com/gocraft/web"
	"github.com/jinzhu/copier"
)

// ExtractionContext contains the extractor and backend
type ExtractionContext struct {
	*Context
	Extractor *extract.Extractor
	Backend   extract.ExtractionsBackend
}

// CreateExtraction cerates a new extraction
func (c *ExtractionContext) CreateExtraction(res web.ResponseWriter, req *web.Request) {
	defer req.Body.Close()
	extraction, err := extract.NewExtractionRequestFromJSONReader(req.Body)
	if err != nil {
		c.renderError(res, http.StatusBadRequest, "invalid_params", err)
		return
	}
	err = c.Backend.FindOrCreateExtraction(extraction)
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	_, err = c.Extractor.Extract(extraction)
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	c.Logger.Printf("extractionAPI=create %s", extraction.Log())
	c.renderJSON(res, http.StatusOK, map[string]interface{}{"extraction": extraction})
}

// ShowExtractions get extraction details
func (c *ExtractionContext) ShowExtractions(res web.ResponseWriter, req *web.Request) {
	extractions, err := c.Backend.FindPendingExtractions()
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	for _, e := range extractions {
		e.Coordinates = nil
		e.CheckProfiles = nil
	}
	c.renderJSON(res, http.StatusOK, map[string]interface{}{"extractions": extractions})
}

// ShowExtraction get extraction details
func (c *ExtractionContext) ShowExtraction(res web.ResponseWriter, req *web.Request) {
	extraction, err := c.Backend.FindExtraction(req.PathParams["id"])
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	extraction.Coordinates = nil
	extraction.CheckProfiles = nil
	c.renderJSON(res, http.StatusOK, map[string]interface{}{"extraction": extraction})
}

// NotifyExtraction get extraction details and push to webhook
func (c *ExtractionContext) NotifyExtraction(res web.ResponseWriter, req *web.Request) {
	extraction, err := c.Backend.FindExtraction(req.PathParams["id"])
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	c.Extractor.NotifyProgress(extraction)
	http.Redirect(res, req.Request, "/extractions/"+extraction.UUID, http.StatusFound)
}

// CancelExtraction cancels a running extraction
func (c *ExtractionContext) CancelExtraction(res web.ResponseWriter, req *web.Request) {
	extraction, err := c.Backend.FindExtraction(req.PathParams["id"])
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	if !extraction.ExtractionFinished() {
		c.Extractor.CancelExtraction(extraction)
	}
	http.Redirect(res, req.Request, "/extractions/"+extraction.UUID, http.StatusFound)
}

type extractionSearch struct {
	extract.Status
	Service  string  `json:"service"`
	Name     string  `json:"name"`
	Lat      float64 `json:"lat,omitempty"`
	Lng      float64 `json:"lng,omitempty"`
	Radius   int     `json:"radius,omitempty"`
	Location string  `json:"location,omitempty"`
}

// ShowExtractionSearches get extraction details
func (c *ExtractionContext) ShowExtractionSearches(res web.ResponseWriter, req *web.Request) {
	filterStatus := req.URL.Query().Get("status")
	includeResults := req.URL.Query().Get("include") == "results"
	extraction, err := c.Backend.FindExtraction(req.PathParams["id"])
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	searches, err := c.Backend.FindSearches(extraction)
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	var results []interface{}
	for _, search := range searches {
		if filterStatus != "" && search.Status.Status != filterStatus {
			continue
		}
		if includeResults {
			results = append(results, search)
			continue
		}
		result := extractionSearch{}
		copier.Copy(&result, search)
		copier.Copy(&result.Status, search.Status)
		results = append(results, result)
	}
	c.renderJSON(res, http.StatusOK, map[string]interface{}{"searches": results})
}

type extractionProfile struct {
	extract.Status
	Service     string `json:"service"`
	RequestedID string `json:"requested_id"`
	ID          string `json:"id,omitempty"`
}

// ShowExtractionProfiles get extraction details
func (c *ExtractionContext) ShowExtractionProfiles(res web.ResponseWriter, req *web.Request) {
	filterStatus := req.URL.Query().Get("status")
	includeProfile := req.URL.Query().Get("include") == "profile"
	extraction, err := c.Backend.FindExtraction(req.PathParams["id"])
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	profiles, err := c.Backend.FindProfiles(extraction)
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	var results []interface{}
	for _, profile := range profiles {
		if filterStatus != "" && profile.Status.Status != filterStatus {
			continue
		}
		if includeProfile {
			results = append(results, profile)
			continue
		}
		result := extractionProfile{}
		copier.Copy(&result, profile)
		copier.Copy(&result.Status, profile.Status)
		if profile.Profile != nil {
			result.ID = profile.ID
		}
		results = append(results, result)
	}
	c.renderJSON(res, http.StatusOK, map[string]interface{}{"profiles": results})
}

// ShowExtractionProfileStatuses get extraction details
func (c *ExtractionContext) ShowExtractionProfileStatuses(res web.ResponseWriter, req *web.Request) {
	filterStatus := req.URL.Query().Get("status")
	extraction, err := c.Backend.FindExtraction(req.PathParams["id"])
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	profiles, err := c.Backend.FindProfileStatuses(extraction)
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	var results []interface{}
	for _, profile := range profiles {
		if filterStatus != "" && profile.Status.Status != filterStatus {
			continue
		}
		results = append(results, profile)
	}
	c.renderJSON(res, http.StatusOK, map[string]interface{}{"profile_statuses": results})
}

// ShowSearch get search details
func (c *ExtractionContext) ShowSearch(res web.ResponseWriter, req *web.Request) {
	search, err := c.Backend.FindSearch(req.PathParams["id"])
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	c.renderJSON(res, http.StatusOK, map[string]interface{}{"search": search})
}

// ShowProfile get profile details
func (c *ExtractionContext) ShowProfile(res web.ResponseWriter, req *web.Request) {
	profile, err := c.Backend.FindProfile(req.PathParams["id"])
	if err != nil {
		c.renderError(res, http.StatusInternalServerError, "invalid_params", err)
		return
	}
	c.renderJSON(res, http.StatusOK, map[string]interface{}{"profile": profile})
}

func extractionEndpoint(router *web.Router, extractor *extract.Extractor, backend extract.ExtractionsBackend) {
	subrouter := router.Subrouter(ExtractionContext{}, "")
	subrouter.Middleware(extractionInitMiddleware(extractor, backend))
	subrouter.Post("/extractions", (*ExtractionContext).CreateExtraction)
	subrouter.Get("/extractions", (*ExtractionContext).ShowExtractions)
	subrouter.Get("/extractions/:id", (*ExtractionContext).ShowExtraction)
	subrouter.Get("/extractions/:id/notify", (*ExtractionContext).NotifyExtraction)
	subrouter.Get("/extractions/:id/cancel", (*ExtractionContext).CancelExtraction)
	subrouter.Get("/extractions/:id/searches", (*ExtractionContext).ShowExtractionSearches)
	subrouter.Get("/searches/:id", (*ExtractionContext).ShowSearch)
	subrouter.Get("/extractions/:id/profiles", (*ExtractionContext).ShowExtractionProfiles)
	subrouter.Get("/extractions/:id/profile_statuses", (*ExtractionContext).ShowExtractionProfileStatuses)
	subrouter.Get("/profiles/:id", (*ExtractionContext).ShowProfile)
}

func extractionInitMiddleware(extractor *extract.Extractor, backend extract.ExtractionsBackend) func(*ExtractionContext, web.ResponseWriter, *web.Request, web.NextMiddlewareFunc) {
	return func(
		c *ExtractionContext,
		rw web.ResponseWriter,
		req *web.Request,
		next web.NextMiddlewareFunc,
	) {
		c.Extractor = extractor
		c.Backend = backend
		next(rw, req)
	}
}
