package helpers

import (
	"log"
	"regexp"
	"strings"
	"time"
)

var (
	timeRegexp  = regexp.MustCompile(`\d{1,2}:\d{2}( *[ap]\.?m\.?)?`)
	timeFormats = []string{"3:04pm", "15:04"}
)

// ParseTimes extracts times from a strings such as "7:30 am - 7:30 pm"
func ParseTimes(source string) []time.Time {
	source = strings.ToLower(source)
	matches := timeRegexp.FindAllString(source, -1)
	times := []time.Time{}
	for _, src := range matches {
		str := Stripchars(src, " .")
		var err error
		for _, format := range timeFormats {
			t, err := time.Parse(format, str)
			if err == nil {
				times = append(times, t)
				break
			}
		}
		if err != nil {
			log.Panicf("%s - %#v", err, str)
		}
	}
	return times
}
