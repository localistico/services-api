package helpers

import (
	"reflect"
	"testing"
	"time"
)

type test struct {
	name     string
	str      string
	expected []time.Time
}

var tests = []test{
	test{
		name: "am/pm hours with nonascii char",
		str:  "7:30 am – 7:30 pm",
		expected: []time.Time{
			time.Date(0, 1, 1, 7, 30, 0, 0, time.UTC),
			time.Date(0, 1, 1, 19, 30, 0, 0, time.UTC),
		},
	},
	test{
		name: "a.m./p.m. hours with nonascii char",
		str:  "7:30 a.m. – 7:30 p.m.",
		expected: []time.Time{
			time.Date(0, 1, 1, 7, 30, 0, 0, time.UTC),
			time.Date(0, 1, 1, 19, 30, 0, 0, time.UTC),
		},
	},
	test{
		name: "AM/PM hours with caps (foursquare style)",
		str:  "7:30 AM–8:00 PM",
		expected: []time.Time{
			time.Date(0, 1, 1, 7, 30, 0, 0, time.UTC),
			time.Date(0, 1, 1, 20, 00, 0, 0, time.UTC),
		},
	},
	test{
		name: "24 hours format with nonascii char",
		str:  "7:30 – 19:30 ",
		expected: []time.Time{
			time.Date(0, 1, 1, 7, 30, 0, 0, time.UTC),
			time.Date(0, 1, 1, 19, 30, 0, 0, time.UTC),
		},
	},
	test{
		name: "24 hours timeframe",
		str:  "00:00 - 00:00",
		expected: []time.Time{
			time.Date(0, 1, 1, 0, 0, 0, 0, time.UTC),
			time.Date(0, 1, 1, 0, 0, 0, 0, time.UTC),
		},
	},
	test{
		name: "mixed times",
		str:  "7:30 am – 7:30 p.m. - 23:15!",
		expected: []time.Time{
			time.Date(0, 1, 1, 7, 30, 0, 0, time.UTC),
			time.Date(0, 1, 1, 19, 30, 0, 0, time.UTC),
			time.Date(0, 1, 1, 23, 15, 0, 0, time.UTC),
		},
	},
}

func TestParseTimes(t *testing.T) {
	for _, test := range tests {
		actual := ParseTimes(test.str)
		if !reflect.DeepEqual(actual, test.expected) {
			t.Errorf("Failed %s\nExpected: %s\nReceived: %s", test.name, test.expected, actual)
		}
	}
}
