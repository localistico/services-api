package helpers

import (
	"strconv"
	"strings"

	"github.com/kellydunn/golang-geo"
)

// PointFromStrCoords takes a string with latitude and longitude separeted by a
// comma and returns a geo.Point for those coordinates
func PointFromStrCoords(coords string) (*geo.Point, error) {
	pair := strings.Split(coords, ",")
	lat, err := strconv.ParseFloat(pair[0], 64)
	if err != nil {
		return nil, err
	}
	lng, err := strconv.ParseFloat(pair[1], 64)
	if err != nil {
		return nil, err
	}
	point := geo.NewPoint(lat, lng)
	return point, nil
}
