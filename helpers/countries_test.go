package helpers

import (
	"testing"
)

var testsCountryCode = map[string]string{
	"Spain":                           "ES",
	"vietnam":                         "VN",
	"colombia":                        "CO",
	"bolivia":                         "BO",
	"Plurinational State of bolivia":  "BO",
	"Bolivia, Plurinational State of": "BO",
}

func TestCountryCodeFromName(t *testing.T) {
	for country, expectedCode := range testsCountryCode {
		actualCode, err := CountryCodeFromName(country)
		if err != nil {
			t.Error(err)
			continue
		}
		if actualCode != expectedCode {
			t.Errorf("Country for %#v. Expected: %#v Got: %#v", country, expectedCode, actualCode)
		}
	}
}
