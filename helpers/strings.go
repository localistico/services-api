package helpers

import (
	"regexp"
	"strings"

	"gopkgs.com/unidecode.v1"
)

// NormalizeString takes a string, transliterates non-ascii characters, downcases it and replaces non-alphanumeric characters with spaces
func NormalizeString(str string) string {
	str = unidecode.Unidecode(str)
	str = strings.ToLower(str)
	str = ampRegexp.ReplaceAllString(str, "$1 and $2")
	str = apostrpheRegexp.ReplaceAllString(str, "s")
	str = normalizeRegexp.ReplaceAllString(str, " ")
	return str
}

var (
	normalizeRegexp = regexp.MustCompile("[^a-z0-9]+")
	ampRegexp       = regexp.MustCompile("([^ ]?)&([^ ]?)")
	apostrpheRegexp = regexp.MustCompile("'s")
)

// AltNames takes a name string and returns different alternative names to search
func AltNames(str string) (list []string) {
	var names = make(map[string]bool)
	names[str] = true
	names[unidecode.Unidecode(str)] = true
	names[NormalizeString(str)] = true

	for name := range names {
		list = append(list, name)
	}
	return
}

// Stripchars returns a new string removing all runes appearing in chr from str
func Stripchars(str, chr string) string {
	return strings.Map(func(r rune) rune {
		if strings.IndexRune(chr, r) < 0 {
			return r
		}
		return -1
	}, str)
}
