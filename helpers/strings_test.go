package helpers

import (
	"testing"
)

func TestStripchars(t *testing.T) {
	str := "12 this removed all chars!?!"
	actual := Stripchars(str, "!as23")
	expected := "1 thi removed ll chr?"
	if actual != expected {
		t.Errorf("\nExpected: %#v\nReceived: %#v", expected, actual)
	}
}

func TestNormalize(t *testing.T) {
	str := "Whatever this & that did to this&those - nando's 14"
	actual := NormalizeString(str)
	expected := "whatever this and that did to this and those nandos 14"

	if actual != expected {
		t.Errorf("\nExpected: %#v\nReceived: %#v", expected, actual)
	}
}
