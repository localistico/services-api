# DEPRECATED

The project is deprecated and we are not using. Here its env configuration just in case:

```
ADMIN_PASSWORD:           m0nmouth
AIRBRAKE_KEY:             ***
API_KEY:                  ***
BUILDPACK_URL:            https://github.com/heroku/heroku-buildpack-go.git
ENV:                      production
FB_ACCESS_TOKEN:          ***
FOURSQUARE_CLIENT_ID:     ***
FOURSQUARE_CLIENT_SECRET: ***
GOOGLE_API_KEY:           ***
LIBRATO_PASSWORD:         ***
LIBRATO_TOKEN:            ***
LIBRATO_USER:             app37102833@heroku.com
MONGODB_DATABASE:         localistico-extractor-production
MONGO_URL:                ***
MOUNT_SERVICES:           facebook,foursquare
RACK_ENV:                 production
REDIS_URL:                ***
ROLLBAR_TOKEN:            ***
SCRAPER_PROXY_URL:        ***
SEARCH_AROUND:            false
SLACK_REPORTS_TOKEN:      ***
SLACK_TEAM:               espresso
SLACK_TOKEN:              ***
YELP_CONSUMER_KEY:        ***
YELP_CONSUMER_SECRET:     ***
YELP_TOKEN_KEY:           ***
YELP_TOKEN_SECRET:        ***
```

# Extractor

This repo contains the service exposing the APIs to search profiles in
the different platforms.

It is consumed by [localistico-backend][https://github.com/espresso-team/localistico-backend].

## Details

The extractor is mostly platform-independent. Platform-specific
details are encapsulated in their own packages containing the logic to
search for profiles, extract details about profiles and check the
profiles' status.

Platforms are mounted so they are available in extractions and have
their own APIs.

Read the [API documentation](doc/API.md).

## Development

 * [Set up Go](http://golang.org/doc/install).
 * [Set up Godep](https://github.com/tools/godep).
 * [Set up gin](https://github.com/codegangsta/gin).
 * Create an `.env` file with the needed environment variables.
 * Launch gin with `gin -p 3000 -g -i run` so the code will be recompiled after your changes.
 * If you change the package dependencies, run `godep save` to update them.

## Testing

Codeship will run the tests on each deploy to `master`, but you can test locally running `go test`.

## Deployment

The extractor is currently deployed to Heroku via Codeship. To deploy it, just
`git push` to the branch:

|**branch**|**environment**|**heroku app**|**endpoint**|
|----------|---------------|--------------|------------|
|master|staging|localistico-extractor-pre|http://services-api-staging.localistico.com|
|production|production|localistico-extractor-pro|http://services-api.localistico.com|

Data is stored in mongodb in [compose.io](http://compose.io)

`google` package caches ID-reference mappings in redis, inside the app
server.

## Server arguments

|**argument**|**ENV variable**|Description|
|------------|----------------|-----------|
|`rollbar-token`|`ROLLBAR_TOKEN`|Access token for Rollbar error notifications|
|`api-key`|`API_KEY`|API Key for http basic auth|
|`fb-access-token`|`FB_ACCESS_TOKEN`|Access Token to access the Facebook API|
|`foursquare-client-id`|`FOURSQUARE_CLIENT_ID`|Client ID for the foursquare API|
|`foursquare-client-secret`|`FOURSQUARE_CLIENT_SECRET`|Client Secret for the foursquare API|
|`google-api-key`|`GOOGLE_API_KEY`|API Key for Google Places API|
|`mount-services`|`MOUNT_SERVICES`|csv list of services to mount|
|`port`|`PORT`|Server port|
|`proxyURL`|`SCRAPER_PROXY_URL`|URL for the scraper's proxy|
|`redis-url`|`REDIS_URL`|URL for the redis database|
|`slack-token`|`SLACK_TOKEN`|Token to publish messages to Slack|
|`yelp-consumer-key`|`YELP_CONSUMER_KEY`|Yelp's Consumer Key|
|`yelp-consumer-secret`|`YELP_CONSUMER_SECRET`|Yelp's Consumer Secret|
|`yelp-token-key`|`YELP_TOKEN_KEY`|Yelp's Token Key|
|`yelp-token-secret`|`YELP_TOKEN_SECRET`|Yelp's Token Secret|
