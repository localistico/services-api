package service

import (
	"testing"
)

func TestExcludeNamesFilter(t *testing.T) {
	filter := ExcludeNamesFilter([]string{"New", "Old"})
	res, _ := filter(ProfileFound{
		Name: "new york",
	})
	if res != false {
		t.Error("new york should be excluded")
	}

	res, _ = filter(ProfileFound{
		Name: "pimlico fresh",
	})
	if res != true {
		t.Error("pimlico fresh should be kept")
	}
}

func TestDiscardProfiles(t *testing.T) {
	filter := ExcludeNamesFilter([]string{"New", "Old"})
	profiles := []ProfileFound{
		ProfileFound{Name: "foo", Discarded: true},
		ProfileFound{Name: "new york", Discarded: false},
		ProfileFound{Name: "bar", Discarded: false},
	}

	err := DiscardProfiles(profiles, filter)
	if err != nil {
		t.Fatal("unexpected error DiscardingProfiles")
	}
	if profiles[0].Discarded != true {
		t.Error("previously discarded should stay discarded")
	}
	if profiles[1].Discarded != true {
		t.Error("name should be discarded")
	}
	if profiles[2].Discarded != false {
		t.Error("unexcluded should stay undiscarded")
	}
}
