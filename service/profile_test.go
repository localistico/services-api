package service

import (
	"testing"
)

type testSimilarName struct {
	name        string
	profileName string
	expected    bool
}

var testSimilarNames = []testSimilarName{
	testSimilarName{
		name:        "Home burguer",
		profileName: "Home Burguer Bar",
		expected:    true,
	},
	testSimilarName{
		name:        "HOME BURGUER",
		profileName: "Home Burguer Bar",
		expected:    true,
	},
	testSimilarName{
		name:        "Casa Maria",
		profileName: "Casa María",
		expected:    true,
	},
	testSimilarName{
		name:        "Casa Maria",
		profileName: "CASA MARÍA",
		expected:    true,
	},
}

func TestSimilarNameFilter(t *testing.T) {
	for _, test := range testSimilarNames {
		name := test.name
		profileName := test.profileName
		expected := test.expected
		profile := ProfileFound{Name: profileName}
		filter := SimilarNameFilter(name)
		actual, err := filter(profile)
		if err != nil {
			t.Fatalf("%s error: %s", name, err)
		}
		if actual != expected {
			if actual {
				t.Errorf("%#v did not match %#v", name, profileName)
			} else {
				t.Errorf("%#v did match %#v", name, profileName)
			}
		}
	}
}
