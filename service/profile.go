package service

import (
	"fmt"
	"net/http"

	"github.com/kellydunn/golang-geo"
)

// Profile contains the details from a venue in a from a service
type Profile struct {
	Service      string       `json:"service"`       // where the profile is (e.g: google, foursquare)
	ID           string       `json:"id"`            // identifier of the profile in the service
	URL          string       `json:"url"`           // link to the profile page
	Name         string       `json:"name"`          // name of the venue/business
	Verified     bool         `json:"verified"`      // whether the profile is Verified/Claimed or not
	Lat          float64      `json:"lat"`           // latitude
	Lng          float64      `json:"lng"`           // longitude
	Address      Address      `json:"address"`       // full venue address
	Phone        string       `json:"phone"`         // international phone number of the venue
	Categories   []string     `json:"categories"`    // list of profile categories
	Website      string       `json:"website"`       // uri to the business or venue website
	Rating       *float32     `json:"rating"`        // business/venue rating (nil if uknown)
	MaxRating    *int         `json:"max_rating"`    // max rating in the service (nil if service has no profile ratings)
	TotalReviews *int         `json:"total_reviews"` // Total number of reviews in the profile (nil if unknown)
	Summary      string       `json:"summary"`       // summary about the venue or business
	Hours        ProfileHours `json:"hours"`         // venue opening hours
	Closed       bool         `json:"closed"`        // wether profile has been marked as closed
}

// ProfileStatus model
type ProfileStatus struct {
	Service string `json:"service"`
	ID      string `json:"id"`
	Deleted bool   `json:"deleted"`
	Closed  bool   `json:"closed"`
	BestID  string `json:"best_id,omitempty"`
}

// SetPhone sets the Phone property after normalizing the phone number
func (p *Profile) SetPhone(phone string) error {
	p.Phone = phone
	return nil
}

// GeoCoordinates returns a *geo.Point from the profile lat lng
func (p Profile) GeoCoordinates() *geo.Point {
	return geo.NewPoint(p.Lat, p.Lng)
}

// ProfileHours includes all timeframes
type ProfileHours struct {
	Mon []string `json:"mon"`
	Tue []string `json:"tue"`
	Wed []string `json:"wed"`
	Thu []string `json:"thu"`
	Fri []string `json:"fri"`
	Sat []string `json:"sat"`
	Sun []string `json:"sun"`
}

// ProfileFound contains ba
type ProfileFound struct {
	Service     string     `json:"service,omitempty"`
	ID          string     `json:"id"`
	Name        string     `json:"name"`
	Coordinates *geo.Point `json:"coordinates,omitempty"`
	Discarded   bool       `json:"discarded"`
}

// Key is a "Service:ID" key
func (p ProfileFound) Key() string {
	return p.Service + ":" + p.ID
}

// Address contains the structured details from an address
type Address struct {
	StreetAddress string `json:"street"`   // Street name and number
	Postcode      string `json:"postcode"` // Postcode or ZIP code
	Locality      string `json:"locality"` // Name of the city or locality
	Region        string `json:"region"`   // Name of the region
	Country       string `json:"country"`  // 2-chars ISO 3166-1 country code
}

type profileServiceBase interface {
	GetTransport() http.RoundTripper
	SetTransport(http.RoundTripper)
	Service() string
	Profile(id string) (profile Profile, err error)
	CheckProfileStatus(*ProfileStatus) error
}

// ProfileService interface defines methods to find and extract profiles
type ProfileService interface {
	profileServiceBase

	SearchLocation(name string, loc string) (found []ProfileFound, err error)
	//SearchBBox(name string, sw *geo.Point, ne *geo.Point) (found []ProfileFound, err error)
	SearchAround(name string, lat, lng float64, radius int) (found []ProfileFound, err error)
}

// SplitSearch divides returned results in accepted and discarded
func SplitSearch(profiles []ProfileFound, err error) (accepted, discarded []ProfileFound, returnedErr error) {
	returnedErr = err
	for _, profile := range profiles {
		if profile.Discarded {
			discarded = append(discarded, profile)
		} else {
			accepted = append(accepted, profile)
		}
	}
	return
}

// NewInt takes an integer and returns a pointer to such integer
func NewInt(num int) *int {
	return &num
}

// NewFloat32 takes a float32 and returns a pointer to such integer
func NewFloat32(num float32) *float32 {
	return &num
}

// NewFloat64 takes a float64 and returns a pointer to such integer
func NewFloat64(num float64) *float64 {
	return &num
}

// NotBusinessProfileError is returned by ProfileService.Profile when actual profile is not from a business (e.g.: a profile from a bus stop)
type NotBusinessProfileError struct {
	ID   string
	Name string
	URL  string
}

// Error returns details about the profile errro
func (err *NotBusinessProfileError) Error() string {
	return fmt.Sprintf("profile is not a business name=%#v id=%s url=%s", err.Name, err.ID, err.URL)
}
