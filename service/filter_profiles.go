package service

import (
	"strings"

	"github.com/localistico/services-api/helpers"
	"github.com/kellydunn/golang-geo"
)

// SimilarNameFilter returns a FilterFunc to match profiles with a similar name
func SimilarNameFilter(name string) FilterFunc {
	name = helpers.NormalizeString(name)
	return func(profile ProfileFound) (bool, error) {
		profileName := helpers.NormalizeString(profile.Name)
		if strings.Contains(profileName, name) {
			return true, nil
		}
		return false, nil
	}
}

// ExcludeNamesFilter returns a FilterFunc to filter out profiles with certain names
func ExcludeNamesFilter(names []string) FilterFunc {
	excluded := make([]string, len(names))
	for i, n := range names {
		excluded[i] = helpers.NormalizeString(n)
	}
	return func(profile ProfileFound) (bool, error) {
		profileName := helpers.NormalizeString(profile.Name)
		for _, n := range excluded {
			if strings.Contains(profileName, n) {
				return false, nil
			}
		}
		return true, nil
	}
}

// WithinBoundsFilter returns a FilterFunc that checks whether a profile is within the given bounds
func WithinBoundsFilter(bounds *geo.Polygon) FilterFunc {
	return func(profile ProfileFound) (bool, error) {
		contained := bounds.Contains(profile.Coordinates)
		return contained, nil
	}
}

// MatchesAllFilter takes a list of filter and returns a FilterFunc that returns true if all filters return true and false otherwise
func MatchesAllFilter(filters ...FilterFunc) FilterFunc {
	return func(profile ProfileFound) (matched bool, err error) {
		for _, filter := range filters {
			matched, err = filter(profile)
			if err != nil {
				return false, err
			}
			if !matched {
				return
			}
		}
		return
	}
}

// FilterFunc functions are used to match found profiles. It returns true when the profile matches filter and false when it's rejected
type FilterFunc func(ProfileFound) (bool, error)

// FilterProfiles takes a list of profiles and divides the list in those matching the filter function and those rejected
func FilterProfiles(profiles []ProfileFound, filter FilterFunc) (match []ProfileFound, reject []ProfileFound, err error) {
	for _, profile := range profiles {
		matched, err := filter(profile)
		if err != nil {
			return match, reject, err
		}
		if matched {
			match = append(match, profile)
		} else {
			reject = append(reject, profile)
		}
	}
	return match, reject, nil
}

// DiscardProfiles takes a list of profiles and further discards profiles
func DiscardProfiles(profiles []ProfileFound, filter FilterFunc) error {
	for i, profile := range profiles {
		if profile.Discarded {
			continue
		}
		matched, err := filter(profile)
		if err != nil {
			return err
		}
		if !matched {
			profiles[i].Discarded = true
		}
	}
	return nil
}
