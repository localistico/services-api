package service

import (
	"time"
)

// Review contains the details from a profile review
type Review struct {
	User      string    `json:"user"`       // Who wrote the review
	Body      string    `json:"body"`       // The review contents
	CreatedAt time.Time `json:"created_at"` // When the review was written
	URL       string    `json:"url"`        // Link to the review
}

// ReviewService defines methods to extract reviews
type ReviewService interface {
	Reviews(id string) (reviews []Review, err error)
	ReviewsSince(id string, since time.Time) (reviews []Review, err error)
}
