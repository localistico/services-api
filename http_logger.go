package main

import (
	"github.com/ernesto-jimenez/httplogger"
	"labix.org/v2/mgo"
	"log"
	"net/http"
	"time"
)

func (c *Context) newHTTPLogger() httplogger.HTTPLogger {
	return &httpLogger{
		requestID: c.RequestID,
		coll:      c.DB.C("http_log"),
		log:       c.Logger,
	}
}

type httpLogger struct {
	requestID string
	log       *log.Logger
	coll      *mgo.Collection
}

func (l *httpLogger) LogRequest(req *http.Request) {
	l.coll.Insert(map[string]interface{}{
		"request": map[string]interface{}{
			"method":         req.Method,
			"url":            req.URL,
			"host":           req.Host,
			"headers":        req.Header,
			"content_length": req.ContentLength,
		},
		"request_id": l.requestID,
		"logged_at":  time.Now(),
	})
}

func (l *httpLogger) LogResponse(req *http.Request, res *http.Response, err error, duration time.Duration) {
	l.coll.Insert(map[string]interface{}{
		"response": map[string]interface{}{
			"status":         res.Status,
			"status_code":    res.StatusCode,
			"headers":        res.Header,
			"content_length": res.ContentLength,
		},
		"error":      err,
		"request_id": l.requestID,
		"logged_at":  time.Now(),
	})
	duration /= time.Millisecond
	l.log.Printf("HTTP Request host=%s method=%s path=%s status=%d durationMs=%d", req.Host, req.Method, req.URL.Path, res.StatusCode, duration)
}

type httpLoggerExtraction struct {
	coll *mgo.Collection
}

func (l *httpLoggerExtraction) LogRequest(req *http.Request) {
	l.coll.Insert(map[string]interface{}{
		"request": map[string]interface{}{
			"method":         req.Method,
			"url":            req.URL,
			"host":           req.Host,
			"path":           req.URL.Path,
			"headers":        req.Header,
			"content_length": req.ContentLength,
		},
		"logged_at": time.Now(),
	})
}

func (l *httpLoggerExtraction) LogResponse(req *http.Request, res *http.Response, err error, duration time.Duration) {
	row := map[string]interface{}{
		"request": map[string]interface{}{
			"method":         req.Method,
			"url":            req.URL,
			"host":           req.Host,
			"path":           req.URL.Path,
			"headers":        req.Header,
			"content_length": req.ContentLength,
		},
		"error":     err,
		"logged_at": time.Now(),
	}
	if res != nil {
		row["response"] = map[string]interface{}{
			"status":         res.Status,
			"status_code":    res.StatusCode,
			"headers":        res.Header,
			"content_length": res.ContentLength,
		}
	}
	l.coll.Insert(row)
	duration /= time.Millisecond
	if res != nil {
		log.Printf("HTTP Request host=%s method=%s path=%s status=%d durationMs=%d", req.Host, req.Method, req.URL.Path, res.StatusCode, duration)
	} else {
		log.Printf("HTTP Request host=%s method=%s path=%s error=%q durationMs=%d", req.Host, req.Method, req.URL.Path, err.Error(), duration)
	}
}
