package main

import (
	"errors"
)

// ErrNotFound is returned by FindID when record is not found
var ErrNotFound = errors.New("resource not found")

// ResourceStore interface defines
type ResourceStore interface {
	All(interface{}) error // Fill interface with content from the store
	// coll().Find(nil).All(&users)
	Insert(interface{}) error         // Store resource item
	FindID(string, interface{}) error // Find element and fill details
	// c.coll().Find(bson.M{"id": req.PathParams["id"]}).One(&user)
	UpdateID(string, interface{}) error // Update element
	// c.coll().Update(query, user)
	RemoveID(string) error
	// c.coll().Remove(query)
}
