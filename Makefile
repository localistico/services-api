SRC=$(wildcard *.go)

.PHONY: all build test

all: test build

test: $(SRC)
	goimports -w *.go **/*.go
	go vet ./...
	go test ./...

build: $(SRC)
	go build ./...

